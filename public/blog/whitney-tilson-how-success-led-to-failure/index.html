<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="map[]" />
    <meta name="description" content="A Log Journal">
    <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico">
    <title>Whitney Tilson - How my success led to my fall</title>
    <meta name="generator" content="Hugo 0.54.0" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/css/main.css" />
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,400,200bold,400old" />
    
    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

    
<script type="application/javascript">
var doNotTrack = false;
if (!doNotTrack) {
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-116300175-1', 'auto');
	
	ga('send', 'pageview');
}
</script>

  </head>

  <body>
    <div id="wrap">

      
      <nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><i class="fa fa-home"></i></a>
    </div>
    <div id="navbar">
      <ul class="nav navbar-nav navbar-right">
      
        
        <li><a href="/blog/">BLOG</a></li>
        
        <li><a href="/projects/">PROJECTS</a></li>
        
        <li><a href="https://www.linkedin.com/in/yarapavan/">RESUME</a></li>
        
      
      </ul>
    </div>
  </div>
</nav>

      
      <div class="container">
        <div class="blog-post">
          <h3>
            <strong><a href="/blog/whitney-tilson-how-success-led-to-failure/">Whitney Tilson - How my success led to my fall</a></strong>
          </h3>
        </div>
        <div class="blog-title">
          <h4>
          May 25, 2018
            &nbsp;&nbsp;
            
            <span class="label label-success">lessons</span>
            
          </h4>
        </div>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="blogpost">
              

<p>Whitney Tilson founded and for nearly two decades managed hedge fund Kase Capital. He is now teaching the next generation of investors via his new business, Kase Learning.</p>

<p>In my new career teaching investing and investment fund entrepreneurship at Kase Learning, one of the most important things I teach is my story and the many, many lessons that can be learned from it.</p>

<p>I launched a tiny hedge fund with only $1 million under management in January 1999, running the entire business myself from my laptop on a rickety Ikea desk in a corner of my bedroom for the first five years. But I learned fast, worked around the clock, made some good decisions and, frankly, got lucky over the first dozen years, during which I nearly tripled my investors’ money in a flat market and grew to three hedge funds and two mutual funds with $200 million under management.</p>

<p>And then I screwed it up over the next seven years, seizing defeat from the jaws of victory. My funds chronically underperformed, which led to assets shrinking to $50 million and, eventually, the decision to close my funds last September.</p>

<p>You might think the single more important and valuable thing I teach during my Lessons from the Trenches investing bootcamp is how I achieved such success in my first dozen years.</p>

<p>But you’d be wrong. While there are of course many important lessons I teach about this early period in my investing career that will benefit those who seek to achieve similar success, I believe the most valuable thing I teach is what happened afterward.</p>

<h2 id="studying-mistakes-and-failures">Studying mistakes and failures</h2>

<p>“Huh?” you might ask. “Why should I study mistakes and failures?”</p>

<p>Here’s why: If you want to learn anything difficult (investing, surgery, flying a plane, etc.), don’t just study success: Spend an equal amount of time studying mistakes and failures. As Charlie Munger says, “Invert, always invert” and “All I want to know is where I’m going to die so I never go there.” (I discussed this way of thinking in my commencement address two years ago at my alma mater, Eaglebrook; video <a href="https://www.youtube.com/watch?v=383qfAdYkbc&amp;feature=youtu.be">here</a>, text with appendices<a href="http://www.tilsonfunds.com/TilsonEaglebrookaddress.pdf">here</a>.)</p>

<p>During my two years at Harvard Business School, I read ~300 case studies and pretty much every one featured a heroic protagonist, facing a difficult issue, but almost every time reaching the right decision and achieving great success. I can’t recall a single one in which the protagonist made wrong decisions and screwed it all up. It’s absolute negligence on the part of HBS (and, to be fair, pretty much every other business school I suspect) to teach success 99% of the time. That’s not how the world works — everyone makes mistakes and suffers setbacks. In addition, if you want to make good decisions, it’s critical to be able to rule out bad ones, which means you need to study them!</p>

<h2 id="how-to-avoid-a-similar-fate">How to avoid a similar fate</h2>

<p>After I finished teaching my personal tale of woe during the recent bootcamp, one of my students asked a wonderful, simple question: “What steps can I take to prevent this from happening to me?”</p>

<p>My quick answer was: “Don’t become successful.”</p>

<p>After the laughter subsided, I added: “And don’t go to Harvard … and graduate with high honors …twice!” (More laughter.)</p>

<p>But I was actually serious, as I explained:</p>

<p>I think being super successful educationally, and then being super successful in the first part of my investing career led to my downfall in many ways. Here are two:</p>

<p>First, an overabundance of hubris (going to Harvard tends to instill that) led me to launch my fund in January 1999 with almost no relevant experience — I had a good general business and entrepreneurial background, but hadn’t worked a day in the finance or investment industries.</p>

<p>I fancied myself a true value investor, following the principles of Warren Buffett and Munger, but I now see that I was little more than a late-’90s bull market genius. For example, I made six times my money in AOL stock in 1998, which led me to believe that I was God’s gift to investing. (I can’t tell you how many young investors today remind me exactly of myself back then!)</p>

<p>I shouldn’t have been managing my own money, much less anyone else’s, much less launching a hedge fund until I’d gotten some real experience in this apprenticeship-based business. But instead I took a shortcut.</p>

<p>While I was able to overcome my lack of experience for a number of years, I made some big mistakes, both as an investor and entrepreneur, which eventually caught up with me.</p>

<p>Second, nailing the internet bubble and then the housing bubble (which culminated in being featured in a “60 Minutes” segment in December 2008 that won an Emmy and CNBC calling me “The Prophet” filled me with hubris (which had become even worse after more than a decade of great success), which blinded me to risks and led to many bad decisions.</p>

<p>Perhaps the worst is that I thought I was a good macro prognosticator and market timer. Thus, instead of just focusing on finding a dozen or so cheap stocks, which is all I did in my early days, I instead formed the opinion that the market was ahead of itself and might plunge at any moment, as it had in 2008-09. Worse yet, I acted on this view, positioning my portfolio defensively, with lots of cash and a big short book, waiting for the market meltdown that never came. Needless to say, this was exactly the wrong positioning during this long, complacent bull market.</p>

<h2 id="to-succeed-you-have-to-be-better-than-ever-before">To succeed, you have to be better than ever before</h2>

<p>The reason I call Kase Learning’s core program Lessons from the Trenches is that it’s a battle. This long bull market combined with the rise of indexing and the increasing sophistication of supercomputers has made the job of investors and fund managers much harder. So, to succeed, you have to be better than ever before: do even more in-depth research, do better analysis, be more patient and disciplined, etc.</p>

<p>Most importantly, you have to constantly be moving rapidly up the experience curve, studying both successes and failures. There are only two ways to get experience: learning from veterans (like me) or stumbling around on your own, making mistakes and getting scars on your back. Which do you prefer?</p>

<p>Original Published at <a href="https://finance.yahoo.com/news/success-led-fall-174512177.html">Yahoo Finance</a></p>

              <hr>
              <div class="related-posts">
                <h5>Related Posts</h5>
                
                  <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <h6 style="text-align: right">
                        March 11, 2019
                      </h6>
                    </div>
                    <div class="col-sm-8 col-md-8 col-lg-8">
                      <h6 style="text-align: left">
                        <strong><a href="/blog/it-takes-a-while/">It’s normal to take a while</a></strong>
                      </h6>
                    </div>
                  </div>
                
                  <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <h6 style="text-align: right">
                        September 17, 2018
                      </h6>
                    </div>
                    <div class="col-sm-8 col-md-8 col-lg-8">
                      <h6 style="text-align: left">
                        <strong><a href="/blog/notes-investor-field-guide-podcasts/">2nd Anniversary Lessons from Investor Field Guide Podcasts</a></strong>
                      </h6>
                    </div>
                  </div>
                
              </div>
            </div>
          </div>
          <hr>
        </div>
      </div>
      
    </div>

    
    <footer>
  <div id="footer">
    <div class="container">
      <p class="text-muted">&copy; All rights reserved. Powered by Hugo and
      sustain theme with ♥</p>
    </div>
  </div>
</footer>
<div class="footer"></div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="/js/docs.min.js"></script>
<script src="/js/main.js"></script>

<script src="/js/ie10-viewport-bug-workaround.js"></script>


    
  </body>
</html>
