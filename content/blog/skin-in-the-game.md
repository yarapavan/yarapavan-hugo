+++
title = "Skin In the Game"
description = "Quotes from Taleb's book - Skin In The Game: Hidden Asymmetries in Daily Life"
tags = []
categories = []
date= 2018-03-12T22:41:58+05:30

+++

From Taleb's new book - Skin In The Game: Hidden Asymmetries in Daily Life

What should real life be about?

```
[...] Life is sacrifice and risk taking, and nothing that doesn’t entail some moderate amount of the former, under the constraint of satisfying the latter, is close to what we can call life. If you do not undertake a risk of real harm, reparable or even potentially irreparable, from an adventure, it is not an adventure.

...take some Fat Tony wisdom: always do more than you talk. And precede talk with action. For it will always remain that action without talk supersedes talk without action.
```

Why skin in the game should matter to you?

```
[...]learning through thrills and pleasure. People have two brains, one when there is skin in the game, one when there is none. Skin in the game can make boring things less boring. When you have skin in the game, dull things like checking the safety of the aircraft because you may be forced to be a passenger in it cease to be boring. If you are an investor in a company, doing ultra-boring things like reading the footnotes of a financial statement (where the real information is to be found) becomes, well, almost not boring.
...
When there was risk on the line, suddenly a second brain in me manifested itself, and the probabilities of intricate sequences became suddenly effortless to analyze and map. When there is fire, you will run faster than in any competition. When you ski downhill some movements become effortless. Then I became dumb again when there was no real action.
...
But if you muster the strength to weight-lift a car to save a child, above your current abilities, the strength gained will stay after things calm down. So, unlike the drug addict who loses his resourcefulness, what you learn from the intensity and the focus you had when under the influence of risk stays with you. You may lose the sharpness, but nobody can take away what you’ve learned.

```

What you should do if you want to "help mankind"?

```
Finally, when young people who “want to help mankind” come to me asking, “What should I do? I want to reduce poverty, save the world,” and similar noble aspirations at the macro-level, my suggestion is:

1. Never engage in virtue signaling;
2. Never engage in rent-seeking;
3. You must start a business. Put yourself on the line, start a business.
Yes, take risk, and if you get rich (which is optional), spend your money generously on others. We need people to take (bounded) risks. The entire idea is to move the descendants of Homo sapiens away from the macro, away from abstract universal aims, away from the kind of social engineering that brings tail risks to society. Doing business will always help (because it brings about economic activity without large-scale risky changes in the economy); institutions (like the aid industry) may help, but they are equally likely to harm (I am being optimistic; I am certain that except for a few most do end up harming).

Courage (risk taking) is the highest virtue. We need entrepreneurs.

```

On ethics

> Start by being nice to every person you meet. But if someone tries to exercise power over you, exercise power over him.

> Avoid taking advice from someone who gives advice for a living, unless there is a penalty for their advice.

> Laws come and go; ethics stay.

> The ethical is always more robust than the legal. Over time, it is the legal that should converge to the ethical, never the reverse.

> Beware of the person who gives advice, telling you that a certain action on your part is “good for you” while it is also good for him, while the harm to you doesn’t directly affect him.

> No person in a transaction should have certainty about the outcome while the other one has uncertainty.

On why you shouldn't trust academics

```
Academia has a tendency, when unchecked (from lack of skin in the game), to evolve into a ritualistic self-referential publishing game.

Now, while academia has turned into an athletic contest, Wittgenstein held the exact opposite viewpoint: if anything, knowledge is the reverse of an athletic contest. In philosophy, the winner is the one who finishes last, he said.
...
In some areas, such as gender studies or psychology, the ritualistic publishing game gradually maps less and less to real research, by the very nature of the agency problem, to reach a Mafia-like divergence of interest: researchers have their own agenda, at variance with what their clients, that is, society and the students, are paying them for. The opacity of the subject to outsiders helps them control the gates. Knowing “economics” doesn’t mean knowing anything about economics in the sense of the real activity, but rather the theories, most of which are bullshit, produced by economists. And courses in universities, for which hard-working parents need to save over decades, easily degenerate into fashion. You work hard and save for your children to be taught a post-colonial study-oriented critique of quantum mechanics.
...
The deprostitutionalization of research will eventually be done as follows. Force people who want to do “research” to do it on their own time, that is, to derive their income from other sources. Sacrifice is necessary. It may seem absurd to brainwashed contemporaries, but Antifragile documents the outsized historical contributions of the nonprofessional, or, rather, the non-meretricious. For their research to be genuine, they should first have a real-world day job, or at least spend ten years as: lens maker, patent clerk, Mafia operator, professional gambler, postman, prison guard, medical doctor, limo driver, militia member, social security agent, trial lawyer, farmer, restaurant chef, high-volume waiter, firefighter (my favorite), lighthouse keeper, etc., while they are building their original ideas.

```

"Burn old logs. Drink old wine. Read old books. Keep old friends."


```

You can define a free person precisely as someone whose fate is not centrally or directly dependent on peer assessment.

Effectively Lindy answers the age-old meta-questions: Who will judge the expert? Who will guard the guard? (Quis custodiet ipsos custodes?) Who will judge the judges? Well, survival will.

For time operates through skin in the game. Things that have survived are hinting to us ex post that they have some robustness—conditional on their being exposed to harm. For without skin in the game, via exposure to reality, the mechanism of fragility is disrupted: things may survive for no reason for a while, at some scale, then ultimately collapse, causing a lot of collateral harm.

That which is “Lindy” is what ages in reverse, i.e., its life expectancy lengthens with time, conditional on survival.


```

On loss aversion

```
What matters isn’t what a person has or doesn’t have; it is what he or she is afraid of losing.
...
The more you have to lose, the more fragile you are.

```

On the proper source of knowledge

```
[...] When I don’t have skin in the game, I am usually dumb. My knowledge of technical matters, such as risk and probability, did not initially come from books. It did not come from lofty philosophizing and scientific hunger. It did not even come from curiosity. It came from the thrills and hormonal flush one gets while taking risks in the markets.

```
To have friends, you'll need to hide your money and erudition


```
If anything, being rich you need to hide your money if you want to have what I call friends. This may be known; what is less obvious is that you may also need to hide your erudition and learning. People can only be social friends if they don’t try to upstage or outsmart one another. Indeed, the classical art of conversation is to avoid any imbalance, as in Baldassare Castiglione’s Book of the Courtier: people need to be equal, at least for the purpose of the conversation, otherwise it fails. It has to be hierarchy-free and equal in contribution. You’d rather have dinner with your friends than with your professor, unless of course your professor understands “the art” of conversation.


```
