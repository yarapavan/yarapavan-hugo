+++
date = "2017-03-21T20:26:40+05:30"
title = "Three buckets of Knowledge"
categories = []
tags = []
description = "What Can the Three Buckets of Knowledge Teach Us About History?"

+++

> Every statistician knows that a large, relevant sample size is their best friend. What are the three largest, most relevant sample sizes for identifying universal principles? Bucket number one is inorganic systems, which are 13.7 billion years in size. It's all the laws of math and physics, the entire physical universe. Bucket number two is organic systems, 3.5 billion years of biology on Earth. And bucket number three is human history, you can pick your own number, I picked 20,000 years of recorded human behavior. Those are the three largest sample sizes we can access and the most relevant. — Peter Kaufman


