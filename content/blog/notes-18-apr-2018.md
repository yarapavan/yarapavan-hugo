+++
title = "Notes - 18th April 2018"
description = ""
tags = ['links']
categories = ['links']
date= 2018-04-18T23:13:48+05:30

+++

* [Flipkart SlashN 2018 Vidoes](https://www.youtube.com/playlist?list=PLNrODmzoJwwrVrcKsqcENLsV4S7aCp9KJ). I missed noticing and thus, attending this annual event at Flipkart. Adding to my #watchlist 
* [AWS Serverless Log- Q1 2018](https://aws.amazon.com/blogs/compute/icymi-serverless-q1-2018/). A compendium of things happened in AWS Serverless world in the 1st Quarter, 2018. #readinglist

#### George Orwell's six rules of writing:
 - Never use a metaphor, simile or other figure of speech which you are used to seeing in print.
 - Never use a long word where a short one will do.
 - If it is possible to cut a word out, always cut it out.
 - Never use the passive where you can use the active.
 - Never use a foreign phrase, a scientific word or a jargon word if you can think of an everyday English equivalent.
 - Break any of these rules sooner than say anything outright barbarous.
