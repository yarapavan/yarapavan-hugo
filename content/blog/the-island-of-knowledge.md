+++
title = "The Island of Knowledge"
description = "Marcelo Glesier's book - The Island of Knowledge"
tags = ['notes']
categories = ['notes']
date= 2019-01-02T19:51:58+05:30

+++

From Marcelo Glesier's book - [The Island of Knowledge](https://www.amazon.in/gp/product/0465049648/):

> Consider, then, the sum total of our accumulated knowledge as constituting an island, which I call the Island of Knowledge. A vast ocean surrounds the Island of Knowledge, the unexplored ocean of the unknown, hiding countless tantalizing mysteries. As the Island of Knowledge grows, so do the shores of our ignorance—the boundary between the known and unknown. Learning more about the world doesn’t lead to a point closer to a final destination — whose existence is nothing but a hopeful assumption anyways — but to more questions and mysteries. The more we know, the more exposed we are to our ignorance, and the more we know to ask.
