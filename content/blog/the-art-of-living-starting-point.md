+++
title = "The Starting Point"
description = "Notes on vipassana meditation book by William Hart - The Art of Living"
tags = ["mind"]
categories = []
date= 2018-03-25T00:03:40+05:30

+++

> We operate on the unthinking assumption that the person who existed ten years ago is essentially the same person who exists will exist ten years from now, perhaps who will still exist in a future life after death. No matter what philosophies or theories or belief hold as true, actually we each live our lives with the deep-rooted conviction, "*I was, I am, I shall be.*"

> The Buddha challenged this instinctive assertion of identity. By doing so he was not expounding one more speculative view to combat the theories of others: he repeatedly emphasized that he was not putting forth an opinion, but simply describing the truth that he had experienced and that any ordinary person can experience. "The enlightened one has cast aside all theories," he said, "for he has seen the reality of matter, sensation, perception, reaction, and consciousness, and their arising and passing away." Despite appearances, he had found that __each human being is in fact a series of separate but related events. Each event is the result of the preceding one and follows it without any interval. The unbroken progression of closely connected events gives the appearance of continuity__, of identity, but this is only an apparent reality, not the ultimate truth.



> We may give a river a name but actually it is a flow of water never pausing in its course. We may think of the light of a candle as something constant, but if we look closely, we see that it is really a flame arising from a wick which burns for a moment, to be replaced at once by a new flame, moment after moment. We talk of the light of an electric lamp, never pausing to think that in reality it is, like the river, a constant flow, in this case a flow of energy caused by very high frequency oscillations taking place within the filament.  Every moment something new arises as a product of the past, to be replaced by something new in the following moment. The succession of events is so rapid and continuous that it is difficult to discern. At a particular point in the process one cannot say that what occurs now is the same as what preceded it, nor can one say that it is not the same. Nevertheless, the process occurs.

> In the same way, the Buddha realized, a person is not a finished, unchanging entity but a process flowing from moment to moment. There is no real "being", merely an ongoing flow, a continuous process of becoming.
