+++
title = "On Wealth"
description = ""
tags = ['tweetstorm']
categories = []
date= 2018-06-20T16:23:58+05:30

+++

Via [Mike Karnjanaprakorn](twitter.com/mikekarn) [thread](https://mobile.twitter.com/mikekarnj/status/1004814247954698247):

1. Over time, I've changed my views on wealth. When I was in my early 20s, I wrote it off as something I would figure out later. When I started a company at 27, I figured that my stock as founder would be good enough. I was wrong in both. Here's what I've learned…

2. “The three most harmful addictions are heroin, carbohydrates, and a monthly salary.” - Nassim Nicholas Taleb

3. FIRE (Financial Independence, Retire Early) changed my view on money. When your net worth is 25x your annual expenses, or when your passive income to expenses ratio is 5:1, you’re considered financially independent. [The Basics of FIRE](https://twocents.lifehacker.com/the-basics-of-fire-financial-independence-and-early-re-1820129768).

4. Why wait for Universal Basic Income (UBI) when you can create it for yourself? What would you do with the freedom and flexibility it offered you?

5. “If you don’t find a way to make money while you sleep, you will work until you die.” — Warren Buffet.

6. You can reach FIRE by minimizing your expenses and/or maximizing your income. Do both but focus your energy on the latter. It’s better to get paid for your

7. When taking risks, limit your downside while having considerable upside. I like to go “all in” when the downside is limited/close to zero, which allows me to swing for the fences.

8. While the riskiest route, the biggest upside is by starting your own company. Most entrepreneurs have a 15-year career span before they get burned out. Take as many calculated swings as you can within that window. Opportunity cost is too high to work on things that go nowhere.

9. A less risky route is to create passive income aka the "side hustle". @jwmares does a great job of highlighting the four different types of side businesses here: [the 4 kinds of side businesses you can start](http://justinmares.com/the-4-kinds-of-side-businesses-you-can-start/)

10. Take concentrated bets, instead of diversifying across hundreds. “I learned that if I could have 10 or 15 uncorrelated bets, and they're all about the same return, that I could cut my risk by 75% or 85%" - @RayDalio

11. Buying a house is a consumption expense, not an investment. After calculating taxes, upkeep, etc, you will get similar returns by investing in the S&P 500 with 99% less work [Nontraditional approach to wealth management - Fabrice Grinda](https://fabricegrinda.com/nontraditional-approach-to-wealth-management/)

12. You don't have to be a billionaire to take advantage of the same strategies. For example, it’s possible to pay $150 in taxes on an income of $150K: [$150,000 Income, $150 Income Tax - Root of Good](https://rootofgood.com/make-six-figure-income-pay-no-tax/).

13. Success is not how much time you spend doing what you love, but how little time you spend doing what you hate.

14. The biggest regret of those on their deathbed? Courage to live a true life to yourself, not working so hard, courage to express feelings, staying in touch with friends, and letting yourself be happier. [The 5 Things People Regret Most On Their Deathbed](http://www.businessinsider.com/5-things-people-regret-on-their-deathbed-2013-12?IR=T).

15. The wealthiest people in the world value experiences, relationships, happiness, freedom, flexibility, and having a sense of purpose. This is the "new wealth" that we should accumulate in our lifetime.
