+++
title = "Low floor, wide walls, high ceiling "
description = ""
tags = ['paradigms','mental-models', 'learning']
categories = ['mental-models']
date= 2018-09-03T10:02:49+05:30

+++

"Low floor, wide walls" is the mantra that guides the design of MIT's [Scratch](https://scratch.mit.edu/) language.

> When discussing technologies to support learning and education, my mentor Seymour Papert often emphasized the importance of “low floors” and “high ceilings.” For a technology to be effective, he said, it should provide easy ways for novices to get started (low floor) but also ways for them to work on increasingly sophisticated projects over time (high ceiling).


> For a more complete picture, we need to add an extra dimension: wide walls. It’s not enough to provide a single path from low floor to high ceiling; we need to provide wide walls so that kids can explore multiple pathways from floor to ceiling.

> — Mitchel Resnick

This gives us a lens through which evaluate tools:

* Low floor: how easy is it to learn?
* Wide walls: how inclusive is it? How many different use-cases does it serve?
* High ceiling: does it scale with growth?

Most programming tools focus exclusively on creating a high ceiling. By choosing the other two neglected dimensions, Scratch has carved out a durable niche for itself. It is widely used in intro-to-programming courses, beginner robotics, after-school classes... any place a simple but open-ended programming model is needed.

<br/>
Product development has a natural bias toward pushing the ceiling higher — focusing on the needs of the most valuable customer, where the highest amount of value may be extracted. This is one of the driving forces of disruption in the Innovator's Dilemma.

How can your product focus on:

Lowering the floor — becoming simpler, cheaper, "good enough"
Widening the walls — serving an ignored audience or market

Source: http://gordonbrander.com/pattern/low-floor-wide-walls-high-ceiling/
