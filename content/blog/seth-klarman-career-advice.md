+++
title = "Seth Klarman's Career Advice"
description = ""
tags = ['advice']
categories = ['career']
date= 2018-11-30T23:07:50+05:30

+++
via [Intelligent Fanatics post](https://community.intelligentfanatics.com/t/seth-klarmans-career-advice/1468):

> I also thought I’d be good at certain things. Competing a 100 hours a week won’t be one of them, because there’ll always be someone else that can do a 110 hours. But being recognized for what the things I can bring to the table are, in terms a certain investment perspective, a certain discipline, a certain risk aversion that maybe other people didn’t have would be a good thing. All I would say is try to hold out. If you have to work for a couple of years to get the next offer or to get int business school, fine. But remember, your goal’s not to be working a 100 hours a week, your goal is to get into a situation that recognizes you for who you are. If you ever are going to work that hard, try and have it be on your nickel, try to be an entrepreneur, and at least you’re doing it for you, not for somebody else who … You can hold you a better accountable than you can hold a boss accountable. And if you do work that hard at least the rewards will also go to you, so if you can, be an entrepreneur, especially if you’re gonna work that hard.


