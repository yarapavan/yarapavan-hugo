+++
title = "Heuristics for deciding on meetings"
description = ""
tags = ['lifehacks']
categories = ['productivity']
date= 2018-07-18T18:50:12+05:30

+++
Source: https://twitter.com/farnamstreet/status/1019601416804020225

(Thread) Heuristics for deciding on meetings. Please feel free to add yours / challenge mine.

1. If you wouldn’t do it right now, say no.

2. Say no to everything outside of work hours you wouldn’t call your partner and bail on dinner for.

3. Say no to all recurring meetings with the exception of ones from your boss if you have one. Tell people you’ll opt-in on a per-agenda basis.

4. Cut all meetings in 1/2

5. If you’re in a meeting and it’s clearly pointless, politely excuse yourself.

6. Keep an internal calculation of what your time is worth. Raise this every year.

7. If you're going for the drink, don't go.

8. If you're the smartest person in the room, you're in the wrong meeting.

9. Even if you can't get out of a meeting, rather than sit there an be bored or make a grocery list, become a detective and see what you can learn from/about everyone else in the meeting.

10. If everyone is on their phone or laptop at the meeting, leave. The decision has already been made.

11. If you're talking to signal how smart you are just shut up.

12. If you're going to "add value" to someone else's idea, just skip the meeting. That type of stuff is best-done one-on-one and not in a group setting where you reduce the motivation of the other person. (See signally how smart you are (11) and shut up.)
