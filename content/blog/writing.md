+++
title = "Writing"
description = "Manuel Blum on Writing"
tags = []
date= 2018-02-18T16:27:57+05:30

+++

> You are all computer scientists.
> You know what FINITE AUTOMATA can do.
> You know what TURING MACHINES can do.
> For example, Finite Automata can add but not multiply.
> Turing Machines can compute any computable function.
> Turing machines are incredibly more powerful than Finite Automata.
> Yet the only difference between a FA and a TM is that
> the TM, unlike the FA, has paper and pencil.
> Think about it.
> It tells you something about the power of writing.
> Without writing, you are reduced to a finite automaton.
> With writing you have the extraordinary power of a Turing machine.
 
~ *Manuel Blum*, Advice to a Beginning Graduate Student
