+++
title = "No Sunk Costs"
description = ""
tags = ['wisdom']
categories = []
date= 2018-07-12T10:12:45+05:30

+++

via @JasonZweig's edge.org [essay](http://jasonzweig.com/what-i-learned-from-daniel-kahneman/):

While I worked with Danny on a project, many things amazed me about this man whom I had believed I already knew well: his inexhaustible mental energy, his complete comfort in saying "I don't know," his ability to wield a softly spoken "Why?" like the swipe of a giant halberd that could cleave overconfidence with a single blow.

But nothing amazed me more about Danny than his ability to detonate what we had just done.

Anyone who has ever collaborated with him tells a version of this story: You go to sleep feeling that Danny and you had done important and incontestably good work that day. You wake up at a normal human hour, grab breakfast, and open your email. To your consternation, you see a string of emails from Danny, beginning around 2:30 a.m. The subject lines commence in worry, turn darker, and end around 5 a.m. expressing complete doubt about the previous day's work. 

You send an email asking when he can talk; you assume Danny must be asleep after staying up all night trashing the chapter. Your cellphone rings a few seconds later. "I think I figured out the problem," says Danny, sounding remarkably chipper. "What do you think of this approach instead?"

The next thing you know, he sends a version so utterly transformed that it is unrecognizable: It begins differently, it ends differently, it incorporates anecdotes and evidence you never would have thought of, it draws on research that you've never heard of. If the earlier version was close to gold, this one is hewn out of something like diamond: The raw materials have all changed, but the same ideas are somehow illuminated with a sharper shift of brilliance.

The first time this happened, I was thunderstruck. How did he do that? How could anybody do that? When I asked Danny how he could start again as if we had never written an earlier draft, he said the words I've never forgotten: "I have no sunk costs."

To most people, rewriting is an act of cosmetology: You nip, you tuck, you slather on lipstick. To Danny, rewriting is an act of war: If something needs to be rewritten then it needs to be destroyed. The enemy in that war is yourself.

After decades of trying, I still hadn't learned how to be a writer until I worked with Danny.

I no longer try to fix what I've just written if it doesn't work. I try to destroy it instead— and start all over as if I had never written a word.

__Danny taught me that you can never create something worth reading unless you are committed to the total destruction of everything that isn't. He taught me to have no sunk costs.__
