+++
title = "Summary - The Distracted Mind: Ancient Brains in a High-Tech World"
description = ""
tags = ['books']
categories = []
date= 2018-03-25T21:32:43+05:30

+++

From the book: The Distracted Mind: Ancient Brains in a High-Tech World

* Your brain is an incredible information-processing system and the most complex structure known to humankind. The brain has allowed us to perform extraordinary feats from discovering general relativity to painting the Sistine Chapel, from building airplanes to composing symphonies. And yet, we still forget to pick up milk on the way home. How can this be?

* We have come to believe that the human brain is a master navigator of the river of information that rages steadily all around us. And yet we often feel challenged when trying to fulfill even fairly simple goals. This is the result of interference—both distractions from irrelevant information and interruptions by our attempts to simultaneously pursue multiple goals. 

* Our cognitive control is really quite limited: we have a restricted ability to distribute, divide, and sustain attention; actively hold detailed information in mind; and concurrently manage or even rapidly switch between competing goals. We can only speculate that if the neural processes of goal enactment evolved to a comparable degree as our goal-setting abilities, we would not be so encumbered by goal interference. If we could hold more information in mind and with higher fidelity, if we could cast a broader attentional net on the world around us and with greater sustainability, if we could simultaneously engage in multiple demanding tasks and transition more efficiently between them, we would not be so easily distracted and interrupted. In many ways, we are ancient brains in a high-tech world.

* Cognitive Control Limitations
	* Attention
    		* __Selectivity__ is limited by susceptibility to bottom-up influences.
    		* __Distribution__ of attention results in diminished performance compared to focused attention.
		* __Sustainability__ of attention over time is limited, especially in extended, boring situations.
		* __Processing speed__ limitations affect both the efficiency of allocation and withdrawal of attention.
 	* Working Memory
		* __Capacity__, or the number of items that can be actively held in mind, is severely limited.
		* __Fidelity__, the quality of information maintained in working memory, decays over time and as a result of interference.
 	* Goal Management
		* __Multitasking__ is limited by our inability to effectively parallel process two attention-demanding tasks.
		* __Task switching__ results in costs to accuracy and speed performance.

* High-Tech World. The “three “game changers” in our rapidly changing high-tech world: the Internet, smartphones, and social media. Each features, at its core, a commodity that is precious us—information. These technologies have the remarkable ability to grab our attention at any instant with an alert, notification, buzz, beep, or even simply in response to a thought that it must be time to check in and make sure that we have not missed out on anything important in the past few minutes. 

* Tackling the Distracted Mind: There are two approaches by which we can diminish the negative impact of interference on our lives: changing our brains and changing our behavior.

* There is quite a wide range in the strength of evidence that has been generated for the many approaches we have considered to aid the Distracted Mind. In our opinion, only physical exercise has reached the highest level of being a truly prescriptive approach. Not far behind this are cognitive exercises, video game training, and meditation, where we see a strong signal in the accumulated scientific literature. There is a weaker signal, but a signal nonetheless, for other approaches that need more evidence to become prescriptive—drugs, brain stimulation, and neurofeedback. 

This is [how to increase your attention span](https://www.bakadesuyo.com/2018/03/attention-span/):

* __Stop multitasking__: You wouldn’t try to lift 5000 pounds. Your body can’t do that. Don’t try to do your best work while checking email, texting, and posting to Instagram. Your brain can’t do that.
* __Exercise__: You know it’s good for your body. Guess what? Your brain’s part of your body. (Shocking, I know.)
Meditate: Simply put, meditation is attention training.
* __Call your mother nature__: Looking at a picture of a tree is like a deep tissue massage for your brain.
* __Reduce interference__: Remove anything from your environment that might distract you. Batch email and social media. Extend the time between breaks to build your attention muscles.
* __Having your phone out doesn’t just distract you from work — it also reduces empathy and harms your relationships__.
