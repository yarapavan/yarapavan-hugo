+++
title = "The Anti Portfolio"
description = ""
tags = ['mistakes']
categories = []
date= 2018-05-20T22:42:35+05:30

+++

Bessemer Venture Partners is perhaps the US's oldest venture capital firm, and it's anti portfolio page is something I haven't seen any where. It documents some of the missed billion dollar opportunities including airbnb, tesla, apple, atlassian, facebook, google, ebay, paypal, snapchat, fedex, intel, intuit, kayak and others.

Website: https://www.bvp.com/portfolio/anti-portfolio


