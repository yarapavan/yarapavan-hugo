+++
title = "The 40-70 Rule"
description = ""
tags = ['mental-models']
categories = []
date= 2018-04-01T10:25:21+05:30

+++

__The 40-70 Rule__. When you’re making a decision, you should collect at least 40% of the information available, but never more than 70%. 40% makes sure you have enough baseline to make a good choice, but over 70% and you get into the tail end optimizations where you waste time and give in to infomania.

> Colin Powell has a rule of thumb about making tough decisions that I feel is helpful when facing such situations. He says that every time you face a tough decision you should have no less than forty percent and no more than seventy percent of the information you need to make the decision. If you make a decision with less than forty percent of the information you need you are shooting from the hip and you will make too many mistakes.

> The second part of the decision making rule is what surprises many leaders. They often think that they need more than seventy percent of the information before they can make a decision. But, I explain to them, if you get more than seventy percent of the information you need to make the decision then the opportunity has usually passed and someone else has beaten you to the punch.

