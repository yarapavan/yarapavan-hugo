+++
title = "Work and Play"
description = ""
tags = ['quotes']
categories = []
date= 2018-07-03T22:28:29+05:30

+++

> A master in the art of living draws no sharp distinction between his work and his play; his labor and his leisure; his mind and his body; his education and his recreation. He hardly knows which is which. He simply pursues his vision of excellence through whatever he is doing, and leaves others to determine whether he is working or playing. To himself, he always appears to be doing both.
>  Lawrence Pearsall Jacks in Education through Recreation, 1932
