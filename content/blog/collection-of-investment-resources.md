+++
title = "Collection of Investing Resources (Valuewalk)"
description = ""
tags = ['links']
categories = ['links']
date= 2018-07-11T10:16:09+05:30

+++

Edit: Fixed some typos and links. Originally conceived to be part of my personal collection, I see that there are financial twitterati following this article. I'm glad you found this collection useful. Please share your feedback at @pavanyara_ (or pavanyara at gmail dot com)

Original Source: https://www.valuewalk.com/2018/02/collection-investment-resources/ 


## Collection of investment resources

______________

Thanks to the Internet I have managed to put together a collection of investment resources. Below are various articles, books, speeches, videos to anybody interested in becoming a better investor. Some are all-time classics, and others are just articles that I enjoyed reading. The collection is not complete and I will update it when times permit. Why am I doing this? First, I wanted to gather some of the most important investment material under one roof. Second, somehow stuff gets lost on the Internet.

The resources are not simply about investing. It is about becoming a better human being. Learning from the success and failure of others is the fastest way to get smarter and wiser without a lot of pain.

__The best thing a human being can do is help another human being know more.__ 

– Charlie Munger, Berkshire Hathaway Annual Meeting, 2010

The resources are classified by alphabetical order.

**Allan Mecham (Arlington Value Management)**

* [Manual of Ideas – Allan Mecham Interview][13] (pdf)
* [The 400% Man][14] (Marketwatch)
* [Is This The Next Warren Buffett?][15] (Forbes)
* [The Oracle of Salt Lake City][16] (ComplianceX)
* [Arlington Capital Presentation – Avoid the Deadly Sins][17] (pdf)
* [Arlington Value 2013 Letter][18] (pdf)
* [Arlington Value 2014 Letter][19] (Valuewalk)
* [Arlington Value 2015 Letter][20] (pdf)

**Benjamin Graham (Father of Value Investing)**

* ["Securities in an Insecure World" San Francisco speech transcript in 1963][21] (PDF) ([Thanks to Jason Zweig for transcript/copy and comments)][22]
* [ Should Rich But Losing Corporations Be Liquidated? ][23](Forbes)
* Books:
  * [Security Analysis 6th edition][24]
  * [The Intelligent Investor: The Definitive Book on Value Investing][25]

**Bill Ackman (Pershing Square)**

* [Graham & Doddsville Winter 2015][26] (PDF)
* [What's Eating Bill Ackman?][27] (Institutional Investor)

**Bill Gates (Microsoft/Cascade Investment)**

* [1998 Warren Buffett & Bill Gates Discuss Innovation, Business and Success][28] (Youtube)
* [2009 Warren Buffett & Bill Gates on Capitalism, Financial Crisis and America][29] (Youtube)
* [This Man's Job: Make Bill Gates Richer][30] (WSJ)

**Bill Gross (PIMCO/Janus)**

* [Bill Gross June Investment Outlook: How to Make Money][31] (Janus Henderson)

**Bill Nygren (Oakmark Fund)**

* [Bill Nygren: "Value Investing Principles and Approach"][32] (Talks at Google)
* [11 Questions With Oakmark's Bill Nygren And Win Murray][33] (Forbes)

**Bruce Berkowitz (Fairhome Funds)**

* [Why Bruce Berkowitz Still Likes Stocks Others Hate][34] (Video, Bloomberg)
* [After the Apocalypse][35] (Barron's)
* [Betting Big, Winning Big: Interview With Bruce Berkowitz, CEO of Fairholme Capital Management][36] (Barron's)
* [The Manager of the Decade award][37] (Morningstar)

 
**Carl Icahn (Icahn Enterprises)**

* [Carl Icahn's Dealbook Conference Interview][38] (Youtube/NYT)

**Charlie Munger (Berkshire Hathaway)**

* [The Psychology of Human Misjudgement][39] speech (pdf)
* [Boom and Bust Is Normal][40] (10 min) (BBC/Youtube)
* [A Conversation with Charlie Munger and Michigan Ross – 2017][41]  ([Script][42]) (Youtube/The Ross School of Business)
* Books:  
 - [Poor Charlie's Almanack: The Wit and Wisdom of Charles T. Munger][43] by Peter D. Kaufman  
[Charlie Munger: The Complete Investor][44] by Tren Griffin  
 - [Damn Right: Behind the Scenes with Berkshire Hathaway Billionaire Charlie Munger][45] by Janet Lowe

**Daniel Kahneman**

* [Don't Blink! The Hazards of Confidence][46] (NYT)
* [A Conversation With Daniel Kahneman; On Profit, Loss and the Mysteries of the Mind ][47](NYT)

**David Einhorn (Greenlight Capital)**

* Book: [Fooling Some of the People All of the Time, A Long Short][48] by David Einhorn

**David F. Swenson (Chief Investment Officer, Yale University)**

* [The Money Management Gospel of Yale's Endowment Guru][49] (NYT)
* [In College Endowment Returns, Davids Beat the Goliaths][50] (NYT)
* [A Conversation with David Swensen][51] (CFR)
* Books:
  - [Pioneering Portfolio Management: An Unconventional Approach to Institutional Investment][52]
  - [Unconventional Success: A Fundamental Approach to Personal Investment][53]

**Elon Musk (Tesla Motors, SpaceX)**

* [Elon Musk: The Architect of Tomorrow][54] (Rolling Stones Magazine)
* Book: [Elon Musk: Tesla, SpaceX, and the Quest for a Fantastic Future][55] by Ashlee Vance
* [Elon's Email to SpaceX Employees Regarding Taking The Company Public][56]

**Ed Thorp (Princeton/Newport Partners)**

* [Ed Thorp: the man who beat the casinos, then the markets][57] (FT)
* [Podcast: Ed Thorp talks with Barry Ritholtz on the Masters in Business][58]
* [Ed Thorp Interview at Context Summits Miami 2017][59] (Youtube)
* [The House Always Wins. Then He Came Along][60] (NPR)
* [Edward Thorp's 20% Annual Return For 30 Years][61] (Forbes)
* Book: [A Man for All Markets: From Las Vegas to Wall Street, How I Beat the Dealer and the Market][62]

**Francois Rochon (Giverny Capital)**

* [The Art of Investing: Analyzing Numbers and Going Beyond][63] – (Talks at Google)

**Henry Singleton (Teledyne Inc.)**

* [Henry Singleton Forbes 1979 article][64]
* [Teledyne and Dr. Henry Singleton, Case Study in Capital Allocation][65] (pdf)
* [The Manual of Ideas on Business Leader Henry Singleton][66] (Youtube)
* [Henry E. Singleton, a Founder Of Teledyne, Is Dead at 82][67] (NYT)
* [Emulate Henry Singleton][68] (Grant's Interest Rate Observer)
* [The Brain Behind Teledyne, A Great American Capitalist][69] (NY Observer)
* [Corporations: Teledyne's Takeoff][70] (Time)
* [A Case Study In Financial Brilliance by Leon G. Cooperman][71] (Valuewalk)
* [A Conglomerate That Works ][72](NYT)

**Howard Marks (Oaktree Capital)**

* Howard Marks' letters at [OaktreeCapital.com][73]
* [Howard Marks: "The Most Important Thing – Origins and Inspirations"][74] (Talks at Google)
* [Podcast: Interview with Howard Marks: Masters in Business][75]
* [Oaktree's Howard Marks on Market Warning, Risks, FAANGs][76] (Youtube/Bloomberg)
* [Oaktree's Howard Marks: Keep Calm and Invest On][77] (Youtube/Bloomberg)
* Book: [The Most Important Thing][78]

**George Soros**

* [George Soros – I Would Rather Call Myself An Insecurity Analyst][79] (The Acquirer's Multiple)

**Guy Spier (Aquamarine Fund)**

* [An Interview with Famed Value Investor Guy Spier][80] (DoughRoller)
* [My $650,100 Lunch with Warren Buffett][81] (Time)

**Indexing**

* [The Silent Road to Serfdom: Why Passive Investing is Worse Than Marxism][82] (Sanford C. Bernstein & Co./Bloomberg)
* [Are Index Funds Communist?][83] (Bloomberg)

**Irving Kahn (Kahn Brothers & Company)**

* [Irving Kahn, Oldest Active Wall Street Investor, Dies at 109][84] (NYT)
* [A Dozen Things I've Learned from Irving Kahn about Value Investing and Business ][85](25iq)
* [How to Play the Market: Irving Kahn][86] (Bloomberg)
* [108-year-old investor: 'I doubled my money in 1929 crash][87] – and I'm still winning' (Telegraph)
* [The 100-Year-Old on Wall Street][88] (NPR)
* [106-year-old stockbroker talks shop][89] (CNN)

**Jamie Dimon (JPMorgan Chase)**

* [Lessons from the CEO Warren Buffett turns to for guidance][90] (CNBC)
* [Jamie Dimon talk][91] (Stanford Graduate School of Business)

**Jeff Bezos (Amazon)**

* [Jeff Bezos and brother Mark give a rare interview about growing up and secrets to success][92] (Summit/Youtube)
* [Jeff Bezos' guide to life][93] (TechCrunch)

**Jeffrey Gundlach (DoubleLine Capital)**

* [Jeffrey Gundlach: The Man Behind The Millions][94] (Buffalo News)

**Jeremy Grantham (Grantham, Mayo, Van Otterloo & Co. LLC)**

* Jeremy Grantham's letters at [GMO.com][95]

**Jim Chanos (Kynikos Associates)**

* [Chanos: Is a big change underway in global capitalism?][96]
* [Transcript of Jim Chanos' appearance on CNBC][97] (CNBC)

**Jim Simons (Renaissance Technologies Corp.)**

* [Jim Simons, the Numbers King][98] (The New-Yorker)
* [Billionaire Mathematician James Simons Flopped the First Time He Invested][99] (Bloomberg)
* [The mathematician who cracked Wall Street][100] (TED Talk)
* ["World's Smartest Billionaire:" James Simons is Cal Alumnus of the Year for 2016][101] (California Magazine)

**Joel Greenblatt (Gotham Asset Management)**

* [On Value Investing: A Conversation with Joel Greenblatt with Howard Marks][102] (Youtube/Wharton)
* [Joel Greenblatt's Market Secrets (Intelligent Investing With Steve Forbes)][103] (Forbes)

**Joel Tillinghast (Fidelity)**

* [Joel Tillinghast Finds Big Returns in Small Stocks][104] (Barron's)

**John Malone (TCI & Liberty Media)**  
Articles & Letters

* [John Malone: Flying Solo][105] (The New Yorker – February 7, 1994)
* [AT&T Completes the Acquisition of TCI][106] (NYT, March 10, 1999)
* [Cable's Darth Vader Is Back][107] (Forbes)
* [National Western Stock Show Citizen of the West John Malone known for loyalty, can-do Western spirit][108] (Denver Post)
* [Liberty Media Company History][109] (Liberty Media)
* [Largest Land Owner][110] (Land Report 100)
* [CNBC's full interview with Liberty Media's John Malone][111] (CNBC)

**Jorge Paulo Lemann (3G Capital)**

* [What I Learned at Harvard][112] (PDF, Santangel's Review)
* [Jim Collins Interviews Jorge Paulo Lemann][113] (Youtube/Havard Business School 2016)
* [Nitin Nohria Interviews Jorge Paulo Lemann and Warren Buffett][114] (Youtube/Havard Business School 2017)
* Book: [Dream Big][115] by Cristiane Correa

**Lei Zhang (Hillhouse Capital)**

* [Lei Zhang's Lecture at Columbia Business School ][116]([Link 1][116], [link 2][117])
* [The Chinese Billionaire Zhang Lei Spins Research Into Investment Gold][118] (NYT)
* [Zhang Lei has Lunch with the FT][119] (Financial Times)
* [Hillhouse Capital Zhang Lei: the best investment does not need to quit][120] ([Netease][121], Translated from Chinese)

**Li Lu (Himalaya Capital)**

* [Li Lu —Know What You Don't Know][122] (Graham & Doddsville)

**Lou Simpson (Former CIO for Geico and current Chairman of SQ Advisors)**

* [A Q&A with renowned investor Lou Simpson][123] (Kellogg Insight)
* [A Dozen Things I've Learned from Lou Simpson About Investing and Business][124] (25iq)
* [Some insight on Lou Simpson and how he works][125] (Value Investing World)
* [Lou Simpson and portfolio construction][126] (Value Investing World)
* [The Next Warren Buffett][127] (Forbes)
* [A Maestro of Investments in the Style of Buffett][128] (NYT)
* [Lou Simpson WSJ Interview][129]

**Mark Cuban (Entrepeneur, Owner of the Dallas Mavericks, Shark Tank)**

* [Mark Cuban In Conversation With Kyle Bass][130] (Real Vision/Youtube)

**Mark Sellers**

* [Mark Sellers Speech to Sellers Capital Investors, June 9, 2007][131] (Pdf)
* [So You Want To Be The Next Warren Buffett? How ís Your Writing?][132] (Pdf via Manual of Ideas)

**Monish Padrai (Padrai Funds/Dhandho Funds)**

* [Mohnish Pabrai Lecture at Boston College Nov 2017][133]  (Youtube/Carroll School of Mgmt)
* [Why Mohnish Pabrai Likes GM, Fiat, and Southwest Air][134] (Barron's)
* [How A Chinese Economic Policy Could Save Club Med Countries][135] (Forbes)
* [More blog, articles and books][136] by Monish (Chai with Padrai)

**Morningstar**

* [Losing My Religion][137] (Excellent article)

**Nassim Nicholas Taleb (Author of Fooled By Randomness and The Black Swan)**

* [Nassim Nicholas Taleb][138] (Talks at Google)
* [Nassim Taleb and Daniel Kahneman discusses Antifragility at NYPL][139] (Youtube/NYPL)

**Paul Singer (Elliott Management)**

* [The World's Most Feared Investor][140] (Bloomberg)
* [The David Rubenstein Show: Paul Singer][141] (Youtube/Bloomberg)
* [Inside Elliott Management: How Paul Singer's Hedge Fund Always Wins][142] (Fortune)

**Peter L. Bernstein**

* [A (Long) Chat With Peter L. Bernstein by Jason Zweig ][143]
* [Our Interview with Peter L. Bernstein][144] (Advisor Perspectives)
* [Obituary: Peter L. Bernstein; Influential Economic Historian][145] (WP)
* [Peter L. Bernstein on Risk][146] (McKinsey & Company)
* [The Big Interview: Chuck Jaffe talks to Peter L. Bernstein, author of "Capital Ideas Evolving"][147] (Marketwatch)
* [Interview With Peter Bernstein | Betting On The Market][148] (PBS)
* Book: [The Intelligent Asset Allocator: How to Build Your Portfolio to Maximize Returns and Minimize Risk][149]
* Book: [The Four Pillars of Investing: Lessons for Building a Winning Portfolio][150]

**Peter Lynch (Fidelity Investments' Magellan Fund)**

* [Peter Lynch Interview – Picking Stocks & Investing][151] (Youtube)
* [A Wall Street legend explains why it 'disturbs' him when people tell him they 'play the market'][152] (Business Insider)
* [Interview With Peter Lynch | Betting On The Market][153] (PBS)
* [Peter Lynch — Charlie Rose][154]

**Ray Dalio (Bridgewater Associates)**

* [How Hegemons Fall][155] (Morningstar)
* [Populism: The Phenomenon][156](Pdf) (Daily Observations)
* [How to build a company where the best ideas win][157] (TED Talk)
* [A Conversation with Ray Dalio and Michael Milken][158] (MilkenInstitute/Youtube)
* [How The Economic Machine Works][159] (Bridgewater/Youtube)
* [Tony Robbins interviews billionaire Ray Dalio][160] (Tony Robbins/Youtube)
* [Ray Dalio: There is Nothing That is Very Attractive][161]
* [Hedge Fund Trader Ray Dalio Best Trading Techniques][162]
* [Dalio discussing his investing principles at the Bloomberg Markets 50 Summit with Erik Schatzker][163] (Bloomberg/Youtube)

**Seth Klarman (Baupost Group)**

* [Seth Klarman – The Value of Not Being Sure][164] (PDF)
* [The Collected Wisdom of Seth Klarman][165] (Santangel's Review)
* [Legendary Investor Is More Worried Than Ever][166] (WSJ)
* [Seth Klarman OID Interview March 2009][167] (PDF)
* [Harvard Business School Interview with Seth Klarman][168]
* [The Oracle of Boston][169] (The Economist)
* [1991 Barron's Interview][170]

**Stanley Druckenmiller**

* [CNBC's full interview with Stanley Druckenmiller][171] (CNBC)

**Ted Weschler (Berkshire Hathaway)**

* [Interview with Warren Buffett's investment manager Ted Weschler][172] (Business Insider)
* [Warren Buffett's money managers, Todd Combs and Ted Weschler, speak][173] (Yahoo)
* [Buffett's three T's][174] (CNBC)

**Todd Combs (Berkshire Hathaway)**

* [10 Questions: Todd Combs][175] (FSU/Vires Magazine)
* [What it's like working for Warren Buffett][176] (CNBC)
* [Warren Buffett's money managers, Todd Combs and Ted Weschler, speak][173] (Yahoo)
* [Buffett's three T's][174] (CNBC)

**Tom Russo (Gardner Russo & Gardner)**

* [Learning from Superinvestors: The Wisdom of Tom Russo][177] (Latticework)

**Vanguard**

* [How risk, reward & time are related][178]

**Warren Buffett (Berkshire Hathaway)**

* [The Superinvestors of Graham-and-Doddsville][179] (pdf) (If you've never read this essay before, please read it. It may be the most important essay ever written in the world of investing. )
* [Buffett Partnership Letters -1957-70][180] (PDF)
* [Warren Buffett's Letters to Berkshire Shareholders (BRK home website)][181]
* [Warren Buffett's first tax return][182] (PBS)
* [Buffett Standford Seminar 1978][183] (PDF)
* [Warren Buffett's 1950s Articles in the Commercial and Financial Chronicle][184]
* [Warren Buffett's 1962 Letter to Barron's][185]
* [1972 Buffett Letter to Sees Candies][186] (PDF)
* [1999 Fortune Magazine Interview. Stock expectations and the Internet][187] (Fortune by Carol Loomis)
* Warren Buffett on PBS NewsHour [Part 1][188], [Part 2][189] (PBS)
* [Warren Buffett – HBO Documentary][190]
* [Warren Buffett brilliantly explains how bubbles are formed][191] (Business Insider)
* [Interview: John Templeton Warren Buffett Robert Wilson][192] (Youtube)

**Walter Schloss (Walter & Edwin Schloss Associates)**

* [Sixty-Five Years on Wall Street][193] (Pdf, Grant's Interest Rate Observer via Grahamanddoddsville.net)
* [Walter & Edwin Schloss Associates][194] interview (Outstanding Investor Digest via CSinvesting)
* [Benjamin Grahamand Security Analysis: A Reminiscence][195] (Pdf va CSinvesting)

**Will Danoff (Fidelity Investments)**

* The $108 Billion Man Who Has Beaten the Market [Link 1, ][196][Link 2][197] (WSJ)

**William N. Thorndike**

* [William Thorndike: "The Outsiders" ][198](Talks at Google)
* Book: [The Outsiders][199]



[1]: https://www.valuewalk.com/category/top-stories/
[2]: https://www.valuewalk.com/category/business/
[3]: https://www.valuewalk.com/category/technology/
[4]: https://finbox.io/integrations/excel?utm_medium=ValueWalk&utm_campaign=ValueWalkSpreadsheetAddon&utm_source=ValueWalk&utm_content=SpreadsheetAddon
[5]: https://www.valuewalk.com/sign-email/
[6]: https://www.valuewalk.com/category/politics/
[7]: https://valuewalkpremium.com/
[8]: https://mk0valuewalkgcar7lmc.kinstacdn.com/wp-content/uploads/2018/07/valuewalk-logo-white.jpg
[9]: https://mk0valuewalkgcar7lmc.kinstacdn.com/wp-content/themes/coolist/assets/img/loading-bubbles.svg
[10]: https://www.valuewalk.com/category/value-investing/ "View all posts in "
[11]: https://www.valuewalk.com/2018/02/collection-investment-resources/
[12]: https://www.valuewalk.com/2018/02/collection-investment-resources/#respond "No Comment"
[13]: https://brianlangis.files.wordpress.com/2017/01/allan-mecham-interview.pdf
[14]: http://www.marketwatch.com/story/the-400-man-1328818316857
[15]: http://www.forbes.com/sites/brettarends/2014/06/22/is-this-the-next-warren-buffett/#4963215b82f6
[16]: http://compliancex.com/the-oracle-of-salt-lake-city-allan-mechams-success-confounds-experts/
[17]: https://brianlangis.files.wordpress.com/2017/01/arlington-value-capital-presentation-12-31-14i1.pdf
[18]: https://brianlangis.files.wordpress.com/2017/01/arlington-value_s-2013-letter.pdf
[19]: https://www.valuewalk.com/2015/02/allan-mecham-2014-letter/
[20]: https://brianlangis.files.wordpress.com/2017/01/arlington-value-capital-2015-annual-letter.pdf
[21]: http://jasonzweig.com/wp-content/uploads/2015/03/BG-speech-SF-1963.pdf
[22]: http://www.jasonzweig.com/a-rediscovered-masterpiece-by-benjamin-graham/bg-speech-sf-1963-2/
[23]: https://www.forbes.com/forbes/1999/1227/6415410a.html
[24]: http://amzn.to/2sZbcnD
[25]: http://amzn.to/2tTg21V
[26]: http://www8.gsb.columbia.edu/valueinvesting/sites/valueinvesting/files/Graham%20%20Doddsville_Issue%2023_Final.pdf
[27]: https://www.institutionalinvestor.com/article/b15ywsstynx8fm/whats-eating-bill-ackman
[28]: https://youtu.be/r92NoD8vALg
[29]: https://www.youtube.com/watch?v=og4tto-Lg_Q
[30]: https://www.wsj.com/articles/this-mans-job-make-bill-gates-richer-1411093811
[31]: https://en-us.janushenderson.com/institutional/make-money/
[32]: https://www.youtube.com/watch?v=GhMZ2c4SSlw
[33]: https://www.forbes.com/sites/gurufocus/2017/12/07/11-questions-with-oakmarks-bill-nygren-and-win-murray/#7fae3763438e
[34]: https://www.bloomberg.com/news/videos/2017-06-19/why-bruce-berkowitz-still-likes-stocks-others-hate-video
[35]: http://www.barrons.com/articles/SB126964400912268153
[36]: http://www.barrons.com/articles/SB120553459025537887
[37]: http://news.morningstar.com/articlenet/article.aspx?id=321713
[38]: https://www.youtube.com/watch?v=GsN0WVLjpcs
[39]: https://cogly.org/wp-content/uploads/2017/01/psychology-misjudgement-munger.pdf
[40]: https://youtu.be/3XlBrohrIUc
[41]: https://www.youtube.com/watch?v=S9HgIGzOENA
[42]: https://microcapclub.com/wp-content/uploads/2017/12/Charlie-Munger-Transcript-University-of-Michigan-11.30.2017.pdf
[43]: http://amzn.to/2iI4ydd
[44]: http://amzn.to/2iFnPOH
[45]: http://amzn.to/2hVi6jz
[46]: http://www.nytimes.com/2011/10/23/magazine/dont-blink-the-hazards-of-confidence.html?pagewanted=all&_r=0
[47]: http://www.nytimes.com/2002/11/05/health/a-conversation-with-daniel-kahneman-on-profit-loss-and-the-mysteries-of-the-mind.html
[48]: http://amzn.to/2uKBnv4
[49]: https://www.nytimes.com/2016/11/06/business/the-money-management-gospel-of-yales-endowment-guru.html
[50]: https://www.nytimes.com/2016/02/26/business/in-college-endowment-returns-davids-beat-the-goliaths.html
[51]: https://www.cfr.org/event/conversation-david-swensen
[52]: https://www.amazon.com/Pioneering-Portfolio-Management-Unconventional-Institutional/dp/1416544690/ref=sr_1_1?ie=UTF8&qid=1499277736&sr=8-1&keywords=david+f.+swensen
[53]: http://amzn.to/2sqzwL6
[54]: http://www.rollingstone.com/culture/features/elon-musk-inventors-plans-for-outer-space-cars-finding-love-w511747
[55]: http://amzn.to/2DEtyf8
[56]: https://brianlangis.wordpress.com/2018/01/26/elons-email-to-spacex-employees-regarding-taking-the-company-public/
[57]: https://www.ft.com/content/e7898528-e897-11e6-967b-c88452263daf?mhq5j=e1
[58]: http://ritholtz.com/2017/07/mib-ed-thorp-beating-vegas-wall-street/
[59]: https://www.youtube.com/watch?v=U5Kj594UAAA
[60]: https://www.npr.org/sections/money/2017/01/13/509624455/the-house-always-wins-then-he-came-along
[61]: https://www.forbes.com/sites/prestonpysh/2017/03/13/edward-thorp-blackjack-beat-the-dealer/#77ded45a13de
[62]: http://amzn.to/2uEsrLE
[63]: https://www.youtube.com/watch?v=ejmn_pxJwtI
[64]: https://brianlangis.wordpress.com/?attachment_id=3888
[65]: https://brianlangis.wordpress.com/2017/07/19/teledyne-and-dr-henry-singleton-case-study-in-capital-allocation/
[66]: https://www.youtube.com/watch?v=3BeqIrpnmT8
[67]: http://www.nytimes.com/1999/09/03/business/henry-e-singleton-a-founder-of-teledyne-is-dead-at-82.html
[68]: https://brianlangis.files.wordpress.com/2017/07/grants-article-1148.pdf
[69]: http://observer.com/2003/04/the-brain-behind-teledyne-a-great-american-capitalist/
[70]: https://www.manualofideas.com/files/content/20090901singleton1.pdf
[71]: https://www.valuewalk.com/wp-content/uploads/2014/11/leon-cooperman-value-investing-congress-henry-singleton.pdf
[72]: http://www.nytimes.com/1981/02/01/business/a-conglomorate-that-works.html
[73]: https://www.oaktreecapital.com/insights/howard-marks-memos
[74]: https://www.youtube.com/watch?v=6WroiiaVhGo
[75]: https://www.bloomberg.com/news/audio/2017-02-17/interview-with-howard-marks-masters-in-business-audio
[76]: https://www.youtube.com/watch?v=BqiQjspBMqU
[77]: https://www.youtube.com/watch?v=Fm0tcmhWdkw
[78]: http://amzn.to/2uIsfLE
[79]: https://acquirersmultiple.com/2017/11/george-soros-i-would-rather-call-myself-an-insecurity-analyst/
[80]: http://www.doughroller.net/investing/interview-famed-value-investor-guy-spier/
[81]: http://content.time.com/time/business/article/0,8599,1819293,00.html
[82]: https://www.bloomberg.com/news/articles/2016-08-23/bernstein-passive-investing-is-worse-for-society-than-marxism
[83]: https://www.bloomberg.com/view/articles/2016-08-24/are-index-funds-communist
[84]: https://www.nytimes.com/2015/02/27/business/irving-kahn-oldest-active-wall-street-investor-dies-at-109.html
[85]: https://25iq.com/2015/05/02/a-dozen-things-ive-learned-from-irving-kahn-about-value-investing-and-business/
[86]: https://www.bloomberg.com/news/articles/2012-04-12/how-to-play-the-market-irving-kahn
[87]: http://www.telegraph.co.uk/finance/personalfinance/investing/11048689/108-year-old-investor-I-doubled-my-money-in-1929-crash-and-Im-still-winning.html
[88]: http://www.npr.org/templates/transcript/transcript.php?storyId=5180970
[89]: http://money.cnn.com/video/news/2011/12/19/n_106_year_old_stockbroker_2.cnnmoney/
[90]: https://www.cnbc.com/2017/03/08/the-ceo-warren-buffett-turns-to-for-wisdom-jpmorgans-jamie-dimon.html
[91]: https://www.youtube.com/watch?v=IyEadGANbgM
[92]: https://www.youtube.com/watch?v=Hq89wYzOjfs&feature=youtu.be
[93]: https://techcrunch.com/2017/11/05/jeff-bezos-guide-to-life/
[94]: http://buffalonews.com/2017/06/30/jeffrey-gundlach-man-behind-millions/
[95]: http://www.gmo.com
[96]: https://www.ineteconomics.org/perspectives/blog/chanos-is-a-big-change-in-global-capitalism-underway
[97]: https://www.cnbc.com/2017/12/14/cnbc-exclusive-cnbc-transcript-kynikos-associates-founder-and-president-jim-chanos-speaks-with-cnbcs-kelly-evans.html
[98]: https://www.newyorker.com/magazine/2017/12/18/jim-simons-the-numbers-king
[99]: https://www.bloomberg.com/news/articles/2017-10-27/billionaire-mathematician-james-simons-flopped-the-first-time-he-invested
[100]: https://www.ted.com/talks/jim_simons_a_rare_interview_with_the_mathematician_who_cracked_wall_street
[101]: https://alumni.berkeley.edu/california-magazine/spring-2016-war-stories/world-s-smartest-billionaire-james-simons-cal-alumnus
[102]: https://www.youtube.com/watch?v=N-azmBU0yII
[103]: https://www.youtube.com/watch?v=3PShSES5nBc
[104]: http://www.barrons.com/amp/articles/joel-tillinghast-finds-big-returns-in-small-stocks-1502511285?mg=prod/accounts-barrons
[105]: http://www.kenauletta.com/johnmaloneflyingsolo.html
[106]: http://www.nytimes.com/1999/03/10/business/at-t-completes-the-acquisition-of-tci.html
[107]: http://www.forbes.com/2001/07/11/0711malone.html
[108]: http://www.denverpost.com/2017/01/08/national-western-stock-show-citizen-of-the-west-john-malone/
[109]: http://www.libertymedia.com/overview/company-history.html
[110]: http://www.landreport.com/americas-100-largest-landowners/
[111]: https://www.cnbc.com/video/2017/11/16/watch-cnbcs-full-interview-with-liberty-medias-john-malone.html
[112]: https://www.scribd.com/doc/169640518/Jorge-Paulo-Lemann-What-I-Learned-at-Harvard
[113]: https://www.youtube.com/watch?v=54LX6SgRLhs
[114]: https://www.youtube.com/watch?v=Co3GFCqQInw
[115]: http://amzn.to/2FDP2KD
[116]: http://www.marketfolly.com/2015/07/lei-zhangs-lecture-at-columbia-business.html
[117]: https://strategicintuition.co/2015/07/28/value-investing-with-legends-lei-zhangs-hillhouse-capital-lecture-at-columbia-business-school/
[118]: https://www.nytimes.com/2015/04/03/business/the-chinese-billionaire-zhang-lei-spins-research-into-investment-gold.html
[119]: https://www.ft.com/content/6f160a50-f5fe-11e3-a038-00144feabdc0?mhq5j=e1
[120]: http://www.hi3p.com/2015/12/01/hillhouse-capital-zhang-lei-the-best-investment-does-not-need-to-quit-33474.html
[121]: http://tech.163.com/15/1201/08/B9O1S199000915BF.html
[122]: https://www8.gsb.columbia.edu/valueinvesting/sites/valueinvesting/files/files/Graham%20%26%20Doddsville%20-%20Issue%2018%20-%20Spring%202013_0.pdf
[123]: https://insight.kellogg.northwestern.edu/article/investment-great-lou-simpson-explains-portfolio-strategy
[124]: https://25iq.com/2015/04/04/a-dozen-things-ive-learned-from-lou-simpson-about-investing-and-business/
[125]: http://www.valueinvestingworld.com/2016/05/some-insight-on-lou-simpson-and-how-he.html
[126]: http://www.valueinvestingworld.com/2016/05/lou-simpson-and-portfolio-construction.html
[127]: https://www.forbes.com/forbes/2000/1030/6612378a.html#77b0bd97bca6
[128]: http://www.nytimes.com/2007/04/23/business/23simpson.html
[129]: http://summit-demo.squarespace.com/summit-financial-advisors/2006/4/29/lou-simpson-wsj-interview.html
[130]: https://www.youtube.com/watch?v=PAcZPUjLdf4
[131]: https://brianlangis.files.wordpress.com/2017/06/mark-seller-speech.pdf
[132]: https://www.manualofideas.com/files/sellers.pdf
[133]: https://youtu.be/_1aGen3q2_g
[134]: http://webreprints.djreprints.com/4007210238780.html
[135]: https://www.forbes.com/sites/mohnishpabrai/2013/01/03/how-a-chinese-economic-policy-could-save-club-med-countries/#74182e577da4
[136]: http://www.chaiwithpabrai.com/articles.html
[137]: https://brianlangis.wordpress.com/2017/06/26/losing-my-religion/
[138]: https://www.youtube.com/watch?v=S3REdLZ8Xis
[139]: https://www.youtube.com/watch?v=MMBclvY_EMA
[140]: https://www.bloomberg.com/graphics/2017-elliott-management/
[141]: https://www.youtube.com/watch?v=jvaCkHhklgc
[142]: http://fortune.com/2017/12/07/elliott-management-hedge-fund-paul-singer/
[143]: http://wp.me/p2vQ8m-1aw
[144]: https://www.google.ca/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&cad=rja&uact=8&ved=0ahUKEwjQuoi13bHXAhWO14MKHb3KDOEQFgg7MAM&url=https%3A%2F%2Fwww.advisorperspectives.com%2Fpdfs%2Fnewsltr08-2-5-1.pdf&usg=AOvVaw0RkHsnNOHfQ0kpnQhE7PZF
[145]: http://www.washingtonpost.com/wp-dyn/content/article/2009/06/08/AR2009060803915.html
[146]: https://www.mckinsey.com/business-functions/risk/our-insights/peter-l-bernstein-on-risk
[147]: https://www.marketwatch.com/story/the-big-interview-chuck-jaffe-talks-to-peter-l-bernstein-author-of-capital-ideas-evolving
[148]: http://www.pbs.org/wgbh/pages/frontline/shows/betting/pros/bernstein.html
[149]: http://amzn.to/2tqaY69
[150]: http://amzn.to/2sqoXYC
[151]: https://www.youtube.com/watch?v=gQP9vpFyULg
[152]: http://www.businessinsider.com/peter-lynch-on-playing-the-market-2017-5
[153]: http://www.pbs.org/wgbh/pages/frontline/shows/betting/pros/lynch.html
[154]: https://charlierose.com/videos/17771
[155]: https://finance.yahoo.com/news/hegemons-fall-120000087.html
[156]: http://www.obela.org/system/files/Populism.pdf
[157]: https://www.ted.com/talks/ray_dalio_how_to_build_a_company_where_the_best_ideas_win
[158]: https://www.youtube.com/watch?v=oCSDfpeTkcU
[159]: https://www.youtube.com/watch?v=PHe0bXAIuk0
[160]: https://www.youtube.com/watch?v=uN7MNHoE6bM&t=2289s
[161]: https://www.youtube.com/watch?v=qztY4A70pCA
[162]: https://www.youtube.com/watch?v=2jKEFe0fPbI
[163]: https://www.youtube.com/watch?v=uU9fmO0_oLI
[164]: https://brianlangis.files.wordpress.com/2017/07/seth-klarman-the-value-of-not-being-sure.pdf "Seth Klarman The-Value-of-Not-Being-Sure"
[165]: https://www.scribd.com/doc/283949360/The-Collected-Wisdom-of-Seth-Klarman
[166]: https://www.wsj.com/articles/SB10001424052748704167704575258442772338282
[167]: https://brianlangis.files.wordpress.com/2017/11/seth-klarman-oid-interview-march-2009.pdf
[168]: https://www.alumni.hbs.edu/stories/Pages/story-bulletin.aspx?num=713
[169]: http://www.economist.com/node/21558274
[170]: https://brianlangis.wordpress.com/2016/03/04/seth-klarman-1991-barrons-interview/
[171]: https://www.cnbc.com/video/2017/12/13/watch-cnbcs-full-interview-with-legendary-investor-stanley-druckenmiller.html
[172]: http://www.businessinsider.de/warren-buffetts-investment-manager-ted-weschler-the-recipe-for-financial-success-2016-6?IR=T
[173]: https://finance.yahoo.com/news/warren-buffetts-money-managers-todd-combs-ted-weschler-speak-142643892.html
[174]: https://www.cnbc.com/video/2014/03/03/buffetts-three-ts.html
[175]: http://alumni.fsu.edu/vires-magazine/10-questions-todd-combs-bs-93
[176]: https://www.cnbc.com/2017/04/28/what-its-like-working-for-warren-buffett-its-literally-just-reading-about-12-hours-a-day.html
[177]: http://latticework.com/learning-from-superinvestors-the-wisdom-of-tom-russo/
[178]: https://investor.vanguard.com/investing/how-to-invest/risk-reward-compounding
[179]: https://brianlangis.files.wordpress.com/2017/01/the-superinvestors-of-graham-and-doddsville-by-warren-buffett.pdf
[180]: https://brianlangis.files.wordpress.com/2017/01/warren-buffett-partnership-letters-1957-70.pdf
[181]: http://www.berkshirehathaway.com/letters/letters.html
[182]: http://www.pbs.org/newshour/updates/warren-buffetts-first-tax-return-filed-age-14/
[183]: https://brianlangis.files.wordpress.com/2017/07/buffett-standford-seminar-1978.pdf
[184]: https://brianlangis.wordpress.com/2018/01/31/warren-buffetts-1950s-articles/
[185]: https://brianlangis.wordpress.com/2018/01/29/warren-buffetts-1962-letter-to-barrons/
[186]: https://brianlangis.files.wordpress.com/2018/02/1972-buffett-letter-to-sees-candies.pdf "1972-Buffett-Letter-to-Sees-Candies"
[187]: http://archive.fortune.com/magazines/fortune/fortune_archive/1999/11/22/269071/index.htm
[188]: http://www.pbs.org/newshour/bb/america-stand-just-wealth-says-warren-buffett/
[189]: http://www.pbs.org/newshour/bb/part-2-billionaire-warren-buffett-says-gop-health-reform-bills-relief-rich/
[190]: https://www.youtube.com/watch?v=ridk6OvkgGM
[191]: http://www.businessinsider.com/warren-buffett-explains-how-bubbles-are-formed-2016-3
[192]: https://www.youtube.com/watch?time_continue=1002&v=LFWj0ps9DqA
[193]: http://www.grahamanddoddsville.net/wordpress/Files/Gurus/Walter%20Schloss/Schloss-Sixty-Five-Years.pdf
[194]: http://csinvesting.org/wp-content/uploads/2014/10/Walter-Schloss-OID-Interview.pdf
[195]: http://csinvesting.org/wp-content/uploads/2014/10/graham_reminiscence.pdf
[196]: http://webreprints.djreprints.com/52914E.html
[197]: https://blogs.wsj.com/moneybeat/2016/10/21/the-108-billion-man-who-has-beaten-the-market/
[198]: https://www.youtube.com/watch?v=D6h5bvxnBKk
[199]: http://amzn.to/2ua7RS5
[200]: https://www.facebook.com/sharer.php?u=https%3A%2F%2Fwww.valuewalk.com%2F2018%2F02%2Fcollection-investment-resources%2F
[201]: https://twitter.com/intent/tweet?text=Collection%20of%20investment%20resources&url=https://www.valuewalk.com/2018/02/collection-investment-resources/&via=valuewalk
[202]: http://www.reddit.com/submit?url=https%3A%2F%2Fwww.valuewalk.com%2F2018%2F02%2Fcollection-investment-resources%2F&title=Collection+of+investment+resources
[203]: https://www.valuewalk.com/author/brianlangis/
[204]: https://mk0valuewalkgcar7lmc.kinstacdn.com/wp-content/uploads/2018/07/Prime-Day-150x150.png
[205]: https://www.valuewalk.com/2018/07/amazon-prime-day-deals-2018-list/
[206]: https://mk0valuewalkgcar7lmc.kinstacdn.com/wp-content/uploads/2017/12/Mark-twain-150x150.jpg
[207]: https://www.valuewalk.com/2018/07/famous-investor-book/
[208]: https://mk0valuewalkgcar7lmc.kinstacdn.com/wp-content/uploads/2017/07/Mario-Gabelli-GAMCO-150x150.jpg
[209]: https://www.valuewalk.com/2018/07/marios-millennial-mondays-compounding/
[210]: https://mk0valuewalkgcar7lmc.kinstacdn.com/wp-content/uploads/2018/07/iRobot-Roomba-671-Robot-Vacuum-150x150.jpg
[211]: https://www.valuewalk.com/2018/07/irobot-roomba-671-robot-vacuum-prime/
[212]: https://mk0valuewalkgcar7lmc.kinstacdn.com/wp-content/uploads/2018/07/Bill-Browder-150x150.jpg
[213]: https://www.valuewalk.com/2018/07/bill-browder-putin/
[214]: https://mk0valuewalkgcar7lmc.kinstacdn.com/wp-content/uploads/2018/07/Lauren-Bonner-Point72-150x150.jpg
[215]: https://www.valuewalk.com/2018/07/lauren-bonner-suing-point72-steve-cohen/
[216]: https://mk0valuewalkgcar7lmc.kinstacdn.com/wp-content/uploads/2018/07/1-Banner-150x150.jpg
[217]: https://www.valuewalk.com/2018/07/student-loans-generation-debt/
[218]: https://www.valuewalk.com
[219]: https://www.paved.com/sites/valuewalk-daily-newsletter?ref=jacobwolinsky
[220]: https://www.valuewalk.com/legal-disclaimer-valuewalk/
[221]: https://www.valuewalk.com/dmca/
[222]: https://c.statcounter.com/5794736/0/b41137cb/1/

  
