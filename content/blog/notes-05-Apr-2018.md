+++
title = "Notes - April 5, 2018"
description = ""
tags = ['links']
categories = []
date= 2018-04-05T12:09:42+05:30

+++
* [HTTP, HTTPS, WebSocket debugging proxy](https://avwo.github.io/whistle/). whistle is a cross-platform web debugging tool based on Node.js.

* [Turn a MacBook into a Touchscreen with $1 of Hardware](https://www.anishathalye.com/2018/04/03/macbook-touchscreen/). The basic principle behind Sistine is simple. Surfaces viewed from an angle tend to look shiny, and you can tell if a finger is touching the surface by checking if it’s touching its own reflection.
* [Make JSON Greppable](https://github.com/tomnomnom/gron). gron transforms JSON into discrete assignments to make it easier to grep for what you want and see the absolute 'path' to it. It eases the exploration of APIs that return large blobs of JSON but have terrible documentation.



