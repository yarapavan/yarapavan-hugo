+++
title = "Speed of Information"
description = ""
tags = ['quotes']
categories = ['mungerisms']
date= 2018-05-16T14:31:49+05:30

+++
> "The speed of information really doesn’t make any difference to us. It’s the processing and finally coming to some judgment that actually has some utility...it’s a judgment about the price of a business or a part of a business, a security, versus what it’s essentially worth. And none of that involves anything to do, really, with quick information. It involves getting good information.... We’re not looking for needles in haystacks or anything of the sort...We like haystacks, not needles, basically. And we want it to shout at us." 
> --Warren Buffett [(1994](https://buffett.cnbc.com/video/1994/04/25/morning-session---1994-berkshire-hathaway-annual-meeting.html)

