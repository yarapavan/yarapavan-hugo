+++
title = "The Time Keeper"
description = "Quote by Mitch Albom "
tags = ['wisdom']
categories = ['quotes']
date= 2019-02-28T19:56:53+05:30

+++

 “Try to imagine a life without timekeeping.<br> 
 You probably can’t.<br> 
 You know the month, the year, the day of the week. <br> 
 There is a clock on your wall or the dashboard of your car. <br> 
 You have a schedule, a calendar, a time for dinner or a movie. <br> 
 Yet all around you, timekeeping is ignored. <br> 
 Birds are not late. <br> 
 A dog does not check its watch. <br> 
 Deer do not fret over passing birthdays. <br> 
__Man alone measures time. <br> 
 Man alone chimes the hour. <br> 
 And, because of this, man alone suffers a paralyzing fear that no other creature endures. <br> 
 A fear of time running out__.” <br> 

 ―[Mitch Albom, The Time Keeper](https://en.wikipedia.org/wiki/The_Time_Keeper)
