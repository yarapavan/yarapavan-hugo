+++
title = "Notes - 27th March 2018"
description = ""
tags = ['things-learnt']
categories = []
date= 2018-03-27T01:04:27+05:30

+++

### AWS Edition
* [Tools for converting Python code to AWS Step Function json](https://github.com/bennorth/pyawssfn)
* [HAL: AWS s3-sns based single-slack-command bot to handle your VPC](https://made2591.github.io/posts/hal-s3-sns)

### Podcasts

#### Startups
* [This Week in Startups](https://medium.com/@TWiStartups)
* [Acquired](http://www.acquired.fm/)
* [Masters of Scale](https://mastersofscale.com/)
* [Exponent](https://soundcloud.com/exponentfm)

#### VCs
* [Venture Studio](https://soundcloud.com/venture-studio)
* [Angel](https://soundcloud.com/twistartups/angel-podcast-ep-1-cyan-banister-v3)
* [Syndicate](http://thesyndicate.vc/itunes)
* [Balderton Capital Podcast](https://soundcloud.com/balderton-capital)
* [20 Minute VC](http://www.thetwentyminutevc.com/category/podcast/)
* [Ventured](https://soundcloud.com/venturedpodcast)
* [a16z](https://soundcloud.com/a16z)
* [Greymatter](https://soundcloud.com/greylock-partners)

#### Google Searching
* [Google Advanced Search Tips from HubSpot](https://blog.hubspot.com/marketing/google-advanced-search-tips)
