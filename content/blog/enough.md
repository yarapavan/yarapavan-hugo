+++
title = "Enough"
description = "The Knowledge that I've got enough"
tags = ['quotes']
categories = ['advice']
date= 2018-12-11T15:14:30+05:30

+++
via __The Hqppiness Equation book__

Joseph Heller, a funny writer now dead, and his friend were at a party given by a billionaire on Shelter Island.

The friend asked, “Joe, how does it make you feel to know that our host only yesterday may have made more money than your novel ‘Catch-22’ has earned in its entire history?”

And Joe said, “I’ve got something he can never have.” 

And his friend asked, “What on earth could that be, Joe?” 

And Joe said, “The knowledge that I’ve got enough.”
