+++
title = "This Much I know - Daniel Kahneman"
description = ""
tags = ['things-learnt']
categories = ['advice']
date= 2018-03-29T22:24:20+05:30

+++

Notes:

* Human beings cannot comprehend very large or very small numbers. It would be useful for us to acknowledge that fact.

* Happiness is complicated. There are two components. One is strongly genetic; the second is a question of how you feel at any moment. I am pretty content, but I had a very pessimistic mother, and I've always been known as a pessimist.

* There is a powerful idea that we should want to be richer. I went to a financial advisor in the States and said: "I don't really want to get richer, but I would like to continue to live like I do." She said: "I can't work with you."

* Collaboration is not only more creative, it is more fun. Amos Tversky, my research partner, and I were better together than on our own. We sort of knew that. Mostly it was extremely pleasant not trying to work everything out yourself.

* Many people now say they knew a financial crisis was coming, but they didn't really. After a crisis we tell ourselves we understand why it happened and maintain the illusion that the world is understandable. In fact, we should accept the world is incomprehensible much of the time.

* Motives are rarely straightforward. When the war started my father was chief of research for a company that was part of L'Oréal in Paris. The owner of L'Oréal was also a main funder of the fascist party in France, and antisemitic. But he protected my [Jewish] father during the war when he was taken by the Nazis.

* Despite 45 years of work in the field, I am still inclined to make over-confident predictions.

* System 1 and System 2.

Fast thinking is, I think, most of the way that we think. It's what your memory delivers to you. You start talking, and you talk. You don't have to deliberate about one word and then the other. You walk. You don't deliberate and decide to put one foot in front of the other.

Most of what we do sort of comes automatically. Most of what we do is highly skilled and emotional -- some of it is emotional, much of it is highly skilled -- and all of that is automatic. There's just an awful lot of automatic stuff that goes on.

Then there is System 2.

If I say, "2 plus 2," a number came to your mind. That's System 1. If I say, "The relationship between China and Japan," now it's not one word that came to your mind, but a whole set of words, a whole set of ideas. I mentioned that, you were thinking islands, you were thinking war, you were thinking navies.

You might have been thinking about the history of China and Japan. A lot happened that you were not... it happened at once. Those are not explicit thoughts, but you are ready for a whole topic, as soon as I mention something. That's System 1.

I mention the word "mother," "your mother" -- you are having an emotion. That's System 1.

There's an awful lot that System 1 does. System 1 has judgments and opinions and attitudes and impressions that are generated -- like when I said, "Think of China and Japan" -- that a whole lot happened at once. You were not conscious of it all at once, but your mind was ready. It was getting ready with it.

That's the idea, that there is that thing going on in our minds, silently. Then you have System 2.

System 2 is the effortful one. It depends on the allocation of attention. It's what we are paying attention to, mostly. It's involved in computations. It's involved in difficult decisions. It's involved in controlling yourself and not telling somebody to go to hell. That demands System 2. It's all part of effortful system.

What's the relationship between the two of them? That's the interesting part.


