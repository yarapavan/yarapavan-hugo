+++
title = "Notes - May 3, 2018"
description = ""
tags = ['links']
categories = ['links']
date= 2018-05-03T10:23:02+05:30

+++

* [Soundcloud Music Downloader](https://github.com/flyingrub/scdl). Just do a ```scdl -l <link>``` of your favorite soundcloud track.
* [Chinese Internet Archtiectures](https://github.com/davideuler/architecture.of.internet-product).A collection of tech architectures for FANG + other big chinese services including WeChat, TenCent, Alibaba.
* [Virtual Xposed](http://vxposed.com/). Use Xposed with a simple APP, without needing to root, unlock the bootloader, or flash a system image.

