+++
title = "Decision Journal"
description = "Daniel Kahneman advice on a decision journal"
date = 2017-11-21T19:50:22+05:30

+++

From Kahneman's book:

> …go down to a local drugstore and buy a very cheap notebook and start keeping track of your decisions. And the specific idea is whenever you’re making a consequential decision, something going in or out of the portfolio, just take a moment to think, write down what you expect to happen, why you expect it to happen and then actually, and this is optional, but probably a great idea, is write down how you feel about the situation, both physically and even emotionally. Just, how do you feel? I feel tired. I feel good, or this stock is really draining me. Whatever you think.

> When you’ve got a decision-making journal, it gives you accurate and honest feedback of what you were thinking at that time. And so there can be situations, by the way, you buy a stock and it goes up, but it goes up for reasons very different than what you thought was going to happen. And having that feedback in a way to almost check yourself periodically is extremely valuable. So that’s, I think, a very inexpensive; it’s actually not super time consuming, but a very, very valuable way of giving yourself essential feedback because our minds won’t do it normally.
