+++
title = "Your Intuition is Wrong"
description = "Notes from Daniel Kahneman's 2018 WEF talk"
tags = ['advice']
categories = ['intuition', 'human nature']
date= 2018-11-27T23:16:02+05:30

+++


“In general, confidence is a very poor cue to accuracy. Because intuitions come to your mind with considerable confidence and there is no guarantee they’re right.”

There are certain times when intuition can be correct. For instance, Kahneman explained, chess players and married couples generally have accurate intuition.

“Intuitions of master chess players when they look at the board [and make a move], they’re accurate,” he said. “Everybody who’s been married could guess their wife’s or their husband’s mood by one word on the telephone. That’s an intuition and it’s generally very good, and very accurate.”

According to Kahneman, who’s studied when one can trust intuition and when one cannot, there are three conditions that need to be met in order to trust one’s intuition.

__The first is that there has to be some regularity in the world that someone can pick up and learn.__

“So, chess players certainly have it. Married people certainly have it,” Kahnemen explained.

However, he added, people who pick stocks in the stock market do not have it.

“Because, the stock market is not sufficiently regular to support developing that kind of expert intuition,” he explained.

__The second condition for accurate intuition is “a lot of practice,” according to Kahneman.__

And the __third condition is immediate feedback. Kahneman said that “you have to know almost immediately whether you got it right or got it wrong.”__

When those three kinds of conditions are satisfied, people develop expert intuition.

“But unless those three conditions are satisfied, the mere fact that you have an idea and nothing else comes to mind and you feel a great deal of confidence — absolutely does not guarantee accuracy,” he added.


