+++
title = "Notes - April 10, 2018"
description = ""
tags = ['links']
categories = []
date= 2018-04-10T19:18:48+05:30

+++
* [A list of awesome repos as a web page](https://awesomerepos.com/)
* [DatabaseFlow - Query your database using a modern web interface for SQL and GraphQL](https://databaseflow.com/). An open source self-hosted SQL client, GraphQL server, and charting application that works with your database.  Can support visualizing schemas, query plans, charts, and results.
* [AWS Meta Documentation](https://alestic.com/2018/03/aws-documentation-jennine/)
* [A Good Intro to AWS basics](https://www.inqdo.com/aws-explained-the-basics/?lang=en). A good primer.

#### How to delete large s3 buckets
 - set expiration policy to 1 day on the target s3 bucket. 
 - do not choose any prefix. 
