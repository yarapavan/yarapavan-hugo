+++
title = "The Headwinds/Tailwinds Asymmetry"
description = ""
tags = ['biases']
categories = []
date= 2018-07-31T18:37:09+05:30

+++

### The Headwinds/Tailwinds Asymmetry

Most of us asymmetrically asses the "__benefits we receive and the obstacles we must overcome in many areas of life__". We feel we face more headwinds and obstacles than every one else - which breeds resentment. We also undervalue the tailwinds that help us - which leaves us ungrateful and unhappy. 

Not only do we tend to over-emphasize the obstacles, but we also tend to think that the obstacles “I” face are more difficult than those faced by “others”.  We manufacture inaccurate ideas to support the notion that other groups have it easier than we do, that their headwinds are less or their tailwinds greater, even when there is no objective evidence that this is the case.
