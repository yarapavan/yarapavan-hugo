+++
title = "Notes - May 25, 2018"
description = ""
tags = ['links']
categories = ['links']
date= 2018-05-25T20:21:09+05:30

+++

* [A note on CLI text processing](https://github.com/learnbyexample/Command-line-text-processing). ⚡️ From finding text to search and replace, from sorting to beautifying text and more 🎨
* A HN list of tech talks. [part1-2018ed](https://news.ycombinator.com/item?id=16838460), [part2-2016ed](https://news.ycombinator.com/item?id=12637239)
* Quote for the day - The secret to great investing is having an edge.

> “Julian Roberts had said it best, ‘What is your edge?’
> When having a bear and a bull discuss a stock at his shop he would constantly say, 

> ‘What do you know that the market doesn’t?’ 

> 'What is in your process that gives you an edge, trading-wise or research-wise?'

> 'What makes you see things differently?'

> 'Where you see the reality rather than the perception of reality?r'

