+++
title = "You Are Alone"
description = "Naval Ravikant's quote"
tags = ['wisdom']
categories = ['wisdom']
date= 2019-01-03T20:21:19+05:30

+++

Ravikant: Socially, we’re told, “Go work out. Go look good.” That’s a multi-player competitive game. Other people can see if I’m doing a good job or not.

We’re told, “Go make money. Go buy a big house.” Again, external multi-player competitive game.

When it comes to learn to be happy, train yourself to be happy, completely internal, no external progress, no external validation, 100% you’re competing against yourself, single-player game.

We are such social creatures, we’re more like bees or ants, that we’re externally programmed and driven, that we just don’t know how to play and win at these single-player games anymore.

We compete purely on multi-player games.__The reality is life is a single-player game. You’re born alone. You’re going to die alone. All of your interpretations are alone. All your memories are alone.__

You’re gone in three generations and nobody cares. Before you showed up, nobody cared. It’s all single-player.
