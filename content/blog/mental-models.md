+++
title = "A list of mental model resources"
description = ""
tags = ['mental-models']
categories = ['models']
date= 2019-01-02T21:19:54+05:30

+++

[Original post](https://www.facebook.com/notes/kent-beck/a-searching-and-fearless-intellectual-inventory/1179765038723025) by Kent Beck


## Models

* __Feedback loop__. To understand a situation, I need to look at what is coming in to the situation, but I also have to pay attention to history. If what has happened historically tends to intensify (rich get richer), it’s a positive feedback loop. Positive feedback loops can always be driven both ways. If what has happened historically tends to diminish (thermostat), it’s a negative feedback loop. [Thinking in Systems](https://www.amazon.com/dp/1603580557)[Quality Software Management: Volume 1, Systems Thinking](https://www.amazon.com/Quality-Software-Management-Systems-Thinking/dp/0932633722/).

* __Finite state machine__. The world responds differently to the same inputs at different times. Think about discrete “states”. Inputs move the the world to a new state. (Feedback Loop)

* Program. Sometimes I can make sense of the world as a recipe, a sequence of steps that always apply.

* Subroutine. I can break the recipe into parts and give the parts names. This lets me understand the whole while deferring details. (Succession, Model).


* Recursion. A particularly powerful way of breaking programs into parts is to have a part refer to itself. (Self-reference)
* Input/process/output. Another way to break up a program is first to think of getting the inputs ready, then processing the inputs, then preparing the outputs.
* Test. Another way to break up a program is first to think about what it looks like from the outside, its input/output behavior. This thinking takes the physical form of a test. Tests can also be written after the program already exists. (Succession, Model)
* Model. Another way to break up a problem is to emphasize parts for the moment and ignore other parts. “This is a just a model,” tells me it’s okay to ignore some details for the moment (but not too long). The powerful thing about models for me is that when I’m using one, I don’t have to think from scratch, I can say “what would this model say about the situation?” I’m actually thinking, but I stop being overwhelmed.
* Object. Rather than have a program being a sequence of steps that operates on external data, another way of dividing a program up is to have bits of data and steps live together.
* Pattern. Programs are mighty specific. Sometimes the world rhymes but it doesn’t repeat. A patterns lets me capture the rhyming but leave some details to be worked out later. (Model)
* Fractal. Some things that look infinitely complicated actually result from the same rules working at different scales. (Model) Fractal Geometry of Nature.
* Scarcity/sufficiency. The same people behave completely differently depending on whether they believe resources are scarce or sufficient. The Mountain People. The Forest People.
* Refactoring. From early in programming I was taught design and then implementation. Refactoring, adding design information to existing code, relieved my anxiety about making mistakes. Refactoring. (Succession)
* Scientific method. Make hypotheses (beliefs) explicit and deliberately try to falsify them. The New Organon.
* Net present value. A dollar today is worth more than a dollar tomorrow. Spend later and earn sooner. 
* Options. Owning the right to buy (sell) something provides more leverage than owning the thing itself.
* Hero’s journey. I avoid panic in tough situations by stepping back and seeing the situation as part of a story. The Writer’s Journey.
* Gaussian distribution. The tug-of-war between two negative feedback loops results in a clear spike in a histogram of samples with some values a little higher and a little lower.
* Power-law distribution. A pure positive feedback loop results in lots of tiny samples and a few really, really, really large samples. The more samples, the more “really”s (i.e. no Central Limit Theorem).
* Assembly line. Oh, this big job of making can be broken down into a sequence of relatively-autonomous steps. The steps can be parallelized and/or batched for economies of scale. (Succession)
* Just in time manufacturing. Each step in the assembly line produces both things and feedback about the making of the things. Reduce batch sizes to increase the proportion of information to things. (Optimization) Toyota Production System.
* Buffer. A buffer or inventory between steps reduces the variance communicated between steps. This both good if it keeps the second step going and bad if it prevents the first step from improving. The Goal.
* Reversibility. To make progress with large, complicated, unpredictable system, quit killing yourself trying to predict the unpredictable. Instead, make all decisions reversible (even if this seems expensive). (Optimization) Reversibility.
* Convex/concave. Distinguish between high-risk/high-reward situations with compounding payoffs and low-risk/low-reward situations with diminishing payoffs. (Optimization, Derivatives) Convexity for Programmers.
* Derivatives. Figure out where things stand now, how things are changing, and how fast that change is changing. Intervene at the appropriate level.
* Metric. From a desired change, derive a measurement that will change if the desired change happens. Beware of “grabbing the speedometer” or “teaching to the test”.
* Optimization. Get from a current situation towards a desired situation in small steps.
* Local maxima. During optimization, be aware of when further progress in small steps is impossible because of diminishing returns. Be prepared to reverse progress for a while to get on a slope with a higher maxima. (Diminishing returns).
* Pareto. Measure and rank potential improvements. One is likely to contain much more potential than all the others combined. Continue until nothing stands out. (Power law distribution, local maxima, optimization).
* Tradeoff. There are no solutions, only tradeoffs. Move from “this is how we do things” to “more of this means less of that, therefore in this situation we do things thusly”. Also known as “it depends, but on what?”. The Secrets of Consulting.
* Transcendence. If you don’t like the answers offered by today’s tradeoff, change the shape of the curves.
* Negotiation. The process of discovering what you really care about most while helping someone else discover what they care about most. At the end, if there’s an intersection, you have a deal. In any case, both parties learn. (Tradeoff, Transcendence)
* Succession. Don’t choke down the whole salami. Eat one slice at a time. Choose the next slice after finishing the previous slice.
* Indirect approach. When faced with opposition, move so that your goal is ambiguous. (Succession) Strategy.
* Accountability. Offer trustworthiness by accounting for your actions and consumption without blame 
* Manipulation. The attempt to induce someone else to make a decision without full freedom or information.
* Authenticity. To be unapologetically and fully yourself. Terrifying at first. Sometimes painful if someone doesn’t like your authentic self. Better than the alternatives. (see also Boundaries)
* Bids. Relationships are built out of a sequence of bids for attention, which can be responded to by turning towards, turning against, or turning away. The balance of towards versus against or away predicts the longevity of the relationship. The Relationship Cure.
* Detachment. Being in this moment, not the future or the past, not the antecedents or the consequences.
* Preferential attachment. The rich get richer. The complex get complexier. Makes no sense some times. Tough. Linked.
* Self-reference. The ultimate programmer trick. Referring to a thing inside itself, as this note does. Gödel, Escher, Bach: An Eternal Golden Braid.
* Operant conditioning. Skinner was a narrow-minded idiot. Incentives are far more nuanced than this. (Feedback Loop, not) Punished By Rewards.
* Maslow’s hierarchy. You have to take care of basic needs like security before belonging and aspiration matter. (Scarcity/sufficiency)
* Emergence. Big, complicated things aren’t designed whole. Optimize, escape local maxima, work at various scales. (Optimization, Local Maximum, Fractal)
* Affordance/mapping. Design interfaces that invite their available actions (affordances) and use consistent mappings from one space to another (left is left and right is right or up is loud and down is quiet). The Design of Everyday Things.
* Paradigm. What we see is colored by what we expect. Deliberately changing what we expect can change what we see. The Structure of Scientific Revolutions.
* Rewriting. Change the structure without changing the meaning. I learned this with trig identities. (Succession)
* Bayes. Rather than search for certainty, work to reduce uncertainty. I’m only ten years into trying to understand Bayes and I only really can apply it to poker. The Theory That Would Not Die.
* Invalidation. You can’t prove anything about the world true, you can only prove it false. The Logic of Scientific Discovery.
* Variance. You can play well and lose and play badly and win. Don’t keep score by winning and losing. Keep score by playing well and learning to play better. (Detachment)
* Butterfly effect. Small actions can have large effects. If I want to have large effects, look for or create positive feedback loops that amplify my actions.
* Diminishing returns. Be aware of when what were previously fruitful efforts no longer product the same payoff. Switch. (Convex/concave, Feedback loop).
* The Chasm. When communicating an innovation, you can’t get everyone on board at once. Start with the novelty vampires, then the rational risk takers, then the majority (I lose interest before the majority). Crossing the Chasm.
* Metaphor. Think about one topic by thinking about another. There’s a source topic, a destination topic, a mapping between the two, and the danger of carrying it too far. Saying “abstract thought is metaphorical” is metaphorical–thought is electro-chemical goo. (Model) Women, Fire, and Dangerous Things.
* Moral hazard. Think about what people would do if they were selfish bastards, even if they aren’t. If someone has an incentive, no matter how base, to act badly, either rejigger the incentives or keep your eyes open.
* Exponential depletion. When someone says, “There are 200 years of coal in the ground at current rates of consumption,” alarm bells should shriek. Depending on how fast consumption is growing, that might be 50 years or 30 years or even 10 years. Same goes for startup runway and consumption of any non-renewable resource. What’s in the bank versus compounded consumption rate.
* Graph. If I can translate a complicated problem into a graph, then there are all sorts of operations I can perform on the graph that translate back to the original problem (need to catalog these). (Model)
* Queueing. Divide the world into tasks, stations, and queues (Graph). Think of how the world is divided, and how it could be divided. Think about the distribution of sizes of tasks, arrival of tasks, changing queue size. Measure latency, throughput, and variance.
* Punctuated equilibrium. Change doesn’t happen steadily. Things will look stable then jump to a new “stable”. “It’s been stable” is not an argument that it won’t change. “It’s been growing” is not an argument that it will keep growing forever (Exponential depletion).
* Half life.
## Techniques
When I get stuck, these are specific things I do that can unstuck me.
* Implication. Hold your nose and do the math. Set aside “this is what I believe” or “this is what I would prefer”. Apply a model or analogy first, then analyze the results.
Boundaries. Be clear about what is my responsibility and what is not my responsibility. “I just got difficult feedback. How much is about me and how much is about the person giving the feedback?”
Waiting. Timing matters. If you are waiting for something specific, then waiting is an action.
Contrarian. You can’t create more impact than someone else by doing what they do. You either have to be better than they are or you have to do something different. I prefer to do something different. One simple heuristic is if someone says, “Obviously...” I always ask myself what would happen if the opposite were true. The greater the fury behind the “obviously”, the longer I’ll spend considering the counterfactual. Usually this leads to dead ends, but when it doesn’t, you won’t have any competition until it’s too late.
Isolation. When faced with a big, complicated, confusing situation (like tracking down a weird bug), first turn it into a small, simpler, still-confusing situation. Binary search is efficient but it’s hard not to try to understand first.
Phase shift. If you’re stuck doing A and then B, try doing half of A, the second half of A and the first half of B together, then the second half of B. (Implement-Test)(Implement-Test) becomes (Test-Implement)(Test-Implement). (Requires a cycle as a prereq).
Believe the Market. When one of my theories (see above) predicts that people will act in one way but a bunch of reasonable people act in a different way, I ask, “What theory would explain their behavior?” (I also use this on myself when I behave in ways I don’t like.)
Thompson Sampling. “A little nonsense/now and then/is relished by/the wisest man”. Put a little investment, occasionally, into even bad ideas, just in case.
Reverse causality. When someone says, “This person is senior so they write the meeting summary,” I immediately say, “Maybe they look senior because they write the meeting summaries.” Try writing the summary yourself and see if people treat your like you are senior.
What I Learned Writing This
I spent hundreds to thousands of hours learning each topic.
I use many of these ideas to avoid being overwhelmed in complex or ambiguous situations.
I use the ideas to generate more ideas. “Well, if this situation is convex, then that implies we should...”
There are more links between the ideas than I suspected. Succession, in particular, is absolutely fundamental.
There seems to be a distinction between models and techniques. 
The techniques are now habits. I don’t have to call them up consciously.
To Do
For each, could talk about definition, example, limitations, reference, application, personal history (both when first encountered and when useful), example-of, syllabus), connections between.
Have deep and shallow “learn more” links.
Have illustrations (pictures) as well as examples.
I’m missing a “how I use this to think” section, either a concrete example or something more pattern-y: patterns I look for, patterns I avoid, how I apply. I should write up one idea “completely”. What does “complete” mean? To what purpose? For whom? Wait for feedback.
Order and group. Wait for suggestions.
What is this good for? Wait for readers to tell me how they found it helpful.
