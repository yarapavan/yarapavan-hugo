+++
tags = []
description = "Busy, but not productive"
title = "Busy, but not productive"
date = "2017-03-21T20:36:53+05:30"
categories = []

+++
From collaborative post with the [title - Busy vs. Productive](http://www.collaborativefund.com/blog/busy-vs-productive/):

Despite the increase in leisure hours over the last several decades, the percentage of Americans who say they don’t have enough time in a day has been relatively constant, at about 50%, according to Gallup.

Why?

Dozens of nuanced things are going on. But two stick out to me as playing the biggest role.

1. Technology has pushed the cost of distraction to zero.

	Technology has freed up time, but it’s also reduced the commitment required for engaging in a task.

	Multitasking was a serious skill when the cost of switching tasks was high. It was rare, so people strived for it.

	But multitasking is now effortless. It’s so easy that it’s dangerous – an almost sure path to distraction.

2. Physical work has a clear start and stop point. Cognitive work never ends.

If your job requires you to think, but you spend your work day trying to look busy, then your job inevitably comes home with you, since the only quiet time you have to think about work is during what’s supposed to be your after-work leisure time.




