+++
title = "My HN Submission - A History of Transaction Histories"
description = "Notes from my HackerNews Submissions"
tags = ['hacker-news']
categories = []
date= 2018-04-01T09:50:56+05:30

+++
[HN Submission - A History of Transaction Histories](https://news.ycombinator.com/item?id=16722186])

> This is also finally the year when people start to wake up and realize they care about serializability, because Jeff Dean said its important. Michael Stonebraker, meanwhile, has been shouting for about 20 years and is very frustrated that people apparently only care when _The Very Important Googlers_say its important.

