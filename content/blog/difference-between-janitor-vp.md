+++
title = "Difference between the Janitor and the Vice President"
description = ""
tags = ["pavan-yara"]
categories = []
date= 2018-03-23T23:05:12+05:30

+++
via Adam Lashinsky's Fortune 2011 May Ed. article on Steve Jobs:

> "... and it's a sermon Jobs delivers every time an executive reaches the VP level.  Jobs imagines his garbage regularly not being emptied in his office, and when he asks the janitor why, he gets an excuse: The locks have been changed, and the janitor doesn't have a key.  This is an acceptable excuse coming from someone who empties trash bins for a living.  The janitor gets to explain why something went wrong.  Senior people do not.  "When you're the janitor," Jobs has repeatedly told incoming VPs, "reasons matter."  He continues, "Somewhere between the janitor and the CEO, reasons stop mattering."  That "Rubicon," he has said, "is crossed when you become a VP."  
