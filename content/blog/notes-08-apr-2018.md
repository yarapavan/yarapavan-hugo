+++
title = "Notes - April 08, 2018"
description = ""
tags = ['links']
categories = []
date= 2018-04-08T20:43:44+05:30

+++

#### CLI Tools

* [A list of command line tools for manipulating structured text data](https://github.com/dbohdan/structured-text-tools). A github compilation of cli tools manipulating text formats including CSV, XML, HTML, JSON, INI, etc. [This HN discussion](https://news.ycombinator.com/item?id=16784850) packs additional tools and HN-sytle comments. I've always been fascinated by unix cli tools and I can't count the number of times they've helped me build my career :)

Some tools I've used time and again include: cut, join, paste, sort, uniq, awk , csvkit, q, textql jq, 

Other noteworthy github collections are:

* [🖥 📊 🕹 🛠 A curated list of command line apps](https://github.com/agarrharr/awesome-cli-apps)

* [Awesome CLI Apps](https://github.com/herrbischoff/awesome-command-line-apps)

* [Awesome Shell](https://github.com/alebcay/awesome-shell)

* [The Art of Command Line](https://github.com/jlevy/the-art-of-command-line)

* [Awesome OSX Cli](https://github.com/herrbischoff/awesome-macos-command-line)

#### FaaS, because why not?

* [GO Functions as a Service](https://github.com/nzoschke/gofaas). Running a Go application on AWS Lambda is easier than ever, once you figure out how to configure Lambda, API Gateway and 10 or other "serverless" services to support the Go functions.  This is a boilerplate app with all the AWS pieces configured correctly and explained in depth. See the docs folder for detailed guides about functions, tracing, security, automation and more with AWS and Go.


