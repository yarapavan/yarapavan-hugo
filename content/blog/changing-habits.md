+++
title = "Resistance Is Futile. To Change Habits, Try Replacement Instead."
description = ""
tags = ['things-learnt']
categories = ['collections']
date= 2018-03-28T21:45:37+05:30

+++

This inconvenient little bit of neuroscience has bothered me ever since I came across a famous Carl Jung quote: __“What you resist not only persists but will grow in size.”__ If resisting a behavior I want to change is not only ineffective but harmful, then what should I do instead?

One trick is to pull a little bait and switch on your own brain. It goes like this: When the urge comes to do the counterproductive thing, don’t resist. Instead, replace.

via [@nytimes article](https://www.nytimes.com/2018/03/19/your-money/resistance-is-futile-to-change-habits-try-replacement-instead.html)
