+++
categories = []
tags = []
description = ""
date = "2017-03-08T19:51:15+05:30"
title = "Elastic Search Cheat Sheet"

+++

### General monitoring API endpoints

| Metric        	| Description							|
|-----------------------------	|------------------------------------------------	|
| stats from all nodes        	| `curl 'localhost:9200/_nodes/stats'`           	|
| Stats from specific nodes   	| `curl 'localhost:9200/_nodes/node1,node2/stats'` 	|
| Stats from a specific index 	| `curl 'localhost:9200/<INDEX_NAME>/_stats' `     	|
| Cluster-wide stats          	| `curl 'localhost:9200/_cluster/stats'`           	|


### Cluster status 

| Metric        		| Description							|
|-----------------------------	|------------------------------------------------	|
| Cluster status & unassigned shards | `curl 'localhost:9200/_cat/health?v'`           	|

### Search performance  

| Metric        		| Description							|
|-----------------------------	|------------------------------------------------	|
| Total number of queries       | `curl 'localhost:9200/_cat/nodes?v&h=name,searchQueryTotal'`          	|
