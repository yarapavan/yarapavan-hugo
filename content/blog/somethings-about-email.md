+++
date = "2017-04-03T21:05:46+05:30"
categories = []
tags = []
description = "Some things about Email"
title = "Some things about Email"

+++

From Albert Wenger's [post](http://continuations.com/post/158892863575/some-things-i-have-learned-about-email):

1. Two legitimate uses for BCC. First, moving some one to bcc who need not to be part of the ongoing exchange. Second, send an email to large group of people.

2. Avoid attachments when you can. Paste images, charts in the email, if less than two or three. Use text to summarize the key points of the charts. Be mobile friendly.

3. Try to answer obvious follow on questions in your initial email

4. Ask people to send an email that can be forwarded, for double opt-in intros.



