+++
title = "Whitney Tilson - How my success led to my fall"
description = ""
tags = ['lessons']
categories = ['life-lessons']
date= 2018-05-25T20:41:05+05:30

+++
Whitney Tilson founded and for nearly two decades managed hedge fund Kase Capital. He is now teaching the next generation of investors via his new business, Kase Learning.

In my new career teaching investing and investment fund entrepreneurship at Kase Learning, one of the most important things I teach is my story and the many, many lessons that can be learned from it.

I launched a tiny hedge fund with only $1 million under management in January 1999, running the entire business myself from my laptop on a rickety Ikea desk in a corner of my bedroom for the first five years. But I learned fast, worked around the clock, made some good decisions and, frankly, got lucky over the first dozen years, during which I nearly tripled my investors’ money in a flat market and grew to three hedge funds and two mutual funds with $200 million under management.

And then I screwed it up over the next seven years, seizing defeat from the jaws of victory. My funds chronically underperformed, which led to assets shrinking to $50 million and, eventually, the decision to close my funds last September.

You might think the single more important and valuable thing I teach during my Lessons from the Trenches investing bootcamp is how I achieved such success in my first dozen years.

But you’d be wrong. While there are of course many important lessons I teach about this early period in my investing career that will benefit those who seek to achieve similar success, I believe the most valuable thing I teach is what happened afterward.

## Studying mistakes and failures

“Huh?” you might ask. “Why should I study mistakes and failures?”

Here’s why: If you want to learn anything difficult (investing, surgery, flying a plane, etc.), don’t just study success: Spend an equal amount of time studying mistakes and failures. As Charlie Munger says, “Invert, always invert” and “All I want to know is where I’m going to die so I never go there.” (I discussed this way of thinking in my commencement address two years ago at my alma mater, Eaglebrook; video [here](https://www.youtube.com/watch?v=383qfAdYkbc&feature=youtu.be), text with appendices[here](http://www.tilsonfunds.com/TilsonEaglebrookaddress.pdf).)

During my two years at Harvard Business School, I read ~300 case studies and pretty much every one featured a heroic protagonist, facing a difficult issue, but almost every time reaching the right decision and achieving great success. I can’t recall a single one in which the protagonist made wrong decisions and screwed it all up. It’s absolute negligence on the part of HBS (and, to be fair, pretty much every other business school I suspect) to teach success 99% of the time. That’s not how the world works — everyone makes mistakes and suffers setbacks. In addition, if you want to make good decisions, it’s critical to be able to rule out bad ones, which means you need to study them!

## How to avoid a similar fate

After I finished teaching my personal tale of woe during the recent bootcamp, one of my students asked a wonderful, simple question: “What steps can I take to prevent this from happening to me?”

My quick answer was: “Don’t become successful.”

After the laughter subsided, I added: “And don’t go to Harvard … and graduate with high honors …twice!” (More laughter.)

But I was actually serious, as I explained:

I think being super successful educationally, and then being super successful in the first part of my investing career led to my downfall in many ways. Here are two:

First, an overabundance of hubris (going to Harvard tends to instill that) led me to launch my fund in January 1999 with almost no relevant experience — I had a good general business and entrepreneurial background, but hadn’t worked a day in the finance or investment industries.

I fancied myself a true value investor, following the principles of Warren Buffett and Munger, but I now see that I was little more than a late-’90s bull market genius. For example, I made six times my money in AOL stock in 1998, which led me to believe that I was God’s gift to investing. (I can’t tell you how many young investors today remind me exactly of myself back then!)

I shouldn’t have been managing my own money, much less anyone else’s, much less launching a hedge fund until I’d gotten some real experience in this apprenticeship-based business. But instead I took a shortcut.

While I was able to overcome my lack of experience for a number of years, I made some big mistakes, both as an investor and entrepreneur, which eventually caught up with me.

Second, nailing the internet bubble and then the housing bubble (which culminated in being featured in a “60 Minutes” segment in December 2008 that won an Emmy and CNBC calling me “The Prophet” filled me with hubris (which had become even worse after more than a decade of great success), which blinded me to risks and led to many bad decisions.

Perhaps the worst is that I thought I was a good macro prognosticator and market timer. Thus, instead of just focusing on finding a dozen or so cheap stocks, which is all I did in my early days, I instead formed the opinion that the market was ahead of itself and might plunge at any moment, as it had in 2008-09. Worse yet, I acted on this view, positioning my portfolio defensively, with lots of cash and a big short book, waiting for the market meltdown that never came. Needless to say, this was exactly the wrong positioning during this long, complacent bull market.

## To succeed, you have to be better than ever before

The reason I call Kase Learning’s core program Lessons from the Trenches is that it’s a battle. This long bull market combined with the rise of indexing and the increasing sophistication of supercomputers has made the job of investors and fund managers much harder. So, to succeed, you have to be better than ever before: do even more in-depth research, do better analysis, be more patient and disciplined, etc.

Most importantly, you have to constantly be moving rapidly up the experience curve, studying both successes and failures. There are only two ways to get experience: learning from veterans (like me) or stumbling around on your own, making mistakes and getting scars on your back. Which do you prefer?



Original Published at [Yahoo Finance](https://finance.yahoo.com/news/success-led-fall-174512177.html)

