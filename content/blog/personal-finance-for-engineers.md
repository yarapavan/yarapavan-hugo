+++
title = "Personal Finance for Engineers - Adam Nash's presentation"
description = ""
tags = []
categories = []
date= 2018-02-08T18:34:07+05:30

+++
Adam Nash (https://twitter.com/adamnash) did a guest lecture at Stanford University on Feb 7, 2018 for the class Pysch 102.

Slides are up at https://www.slideshare.net/adamnash/personal-finance-for-engineers-stanford-2018/adamnash/personal-finance-for-engineers-stanford-2018

