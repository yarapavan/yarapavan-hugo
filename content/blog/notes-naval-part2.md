+++
title = "Notes from Naval - Part 2"
description = "Periscope conversation - May 7, 2018"
tags = ['naval']
categories = ['notes']
date= 2018-06-26T15:48:26+05:30

+++

From [Scott Adams’ and Naval Ravikant’s Periscope Session – May 7, 2018](https://www.pscp.tv/ScottAdamsSays/1nAKERdZMkkGL) session:

### IT’S STATISTICALLY LIKELY WE’RE IN A SIMULATION
* Naval
	*  On a long enough period of time, its probable an advanced civilization will create a simulation…and since the universe has been around for such a long time, it’s probably already happened
	* There are similarities between Buddhism and the simulation hypothesis
	* The Buddhists say we’re all one thing, in the simulation hypothesis, we’re all made of the same code
	* Reaching enlightenment is equivalent to realizing we’re in a simulation
* Scott 
	* “The fact that we’re observed in this conversation is the only thing that makes it real”
	* “Reality doesn’t exist unless there’s a observer”
	* Wave particle duality doesn’t collapse unless there’s an observer

### DO WE HAVE FREE WILL?
* Scott – No. Our brain is a moist computer and it does what it has to do given the inputs
* Naval  – Also no, but it doesn’t mean things are predetermined. We have to move forward to figure out what’s going to happen

### STRATEGIES FOR SUCCESS
* Q: On a scale of 1-10, how does Naval rate the below in terms of being necessary for success?
* Hard work – 3 or 4 
 * “For general people, going through life, just trying to be successful, hard work doesn’t matter that much” 
 * “What you do, and how you do it, is so much more important than how hard you do it”
* Your network – 8/9 when you’re starting out
  * “Create work that can be seen and recognized, and the network emerges”
  * Intelligence – No answer
* “The number one thing you can have is general intelligence, because general intelligence allows you to get good at everything else”
 * “If you’re smart, and question everything, you’ll eventually figure everything else out at a good enough level” – this can apply to nutrition, fitness, your sales skills, really anything
* Intelligence and open mindedness are everything

### ENTREPRENEURSHIP
* You can tell with certain people, that they will be a “winner” – it’s just a question of when
* It’s hard to tell which businesses will work, lots of businesses fail
* Why are we betting on businesses instead of people with venture capital?
* Warren Buffet invests in natural monopolies
* One reason Coca Cola is a massive business – the sugar and caffeine is addictive
* It also has zero taste memory, so you don’t get sick of drinking it
* “He invests in natural monopolies that are basically addictive drugs”
* Amazon replaced cold packs for their grocery delivery with frozen water bottles
* This results in a small dopamine rise in the brain due to a free product

### DECENTRALIZATION
* Humans are cooperators- we build networks like language, Uber, and the US dollar
* We cooperate better than any other species
* Our networks need to be controlled, because there’s always cheaters
* Networks have historically been run by individuals, elites, or companies etc.
* Blockchains bring a new form of government
* How do they work? – A piece of code is run on the cloud, with a need for a resource, like storage, for example
* Anyone who contributes storage into the network, would get paid
* Anyone who needed storage out of the netwok, would have to pay
* All the transactions would be tracked in a ledger – that’s a blockchain – a piece of code in the cloud that nobody owns. It punishes cheaters (those who use too much storage/break the rules) and rewards people who bring lots of storage reliably
* Blockchains will build “permissionlessly programmable uncensorable networks” – Naval
* This is unlike our current money system where we have to have a third party’s permission for a transaction
* We are replacing the banks with code
* “The way Amazon treats you, imagine if your bank treated you that way”
* Uncensorable means where you don’t have a gatekeeper
* “At some level, all laws are contractual social self-censorship”
* Naval – “They still haven’t been able to shut down Bittorrent, how would they shut down Bitcoin”
* Could Bitcoin ever become illegal?
* Naval – Probably not. It’s just code, which is just speech. “You can’t make code illegal anymore than you can make speech illegal”
* Bitcoin is just insurance against politicians. “It only has value because it’s not controlled. The moment you have a cryptocurrency that is controlled by a democracy, a mob, an elite, whatever, it has no value.”

### ON HAVING KIDS
* Naval doesn’t think it gets enough credit
> “You get this amazing wholesome release when your genes are finally replicated”
* Scott views it in a different way, he only has step children
> His value is in the fact that his ideas are reproduced through books and other content, which is then passed on

### CONSCIOUSNESS
* Scott
 * “Consciousness is the ability to broadcast your expectations and judge how close you were to being right. It’s the friction between what you think, and what actually happens. That’s all you’re really experiencing.”
 * We’re most conscious while in pain or while we’re really happy
 * When things are going as we expect, we resort to our lowest level of consciousness
* Naval
 * The brain/mind is a machine that takes past memories and uses them to predict the future. Below that is just raw consciousness – awareness.

### WHAT WOULD A NAVAL/SCOTT UNIVERSITY TEACH?
* Naval
 * Naval would teach a class on persuasive writing
 * He recommends Scott’s article – The Day You Became a Better Writer
 * He would also have them check out the Dale Carnegie School of Persuasion
 * Naval would encourage students to learn a system of figuring out the optimal nutrition for themselves
 * There’s so much disagreement out there on what an optimal diet should look like, it’s almost like politics
 * Some sort of fun mathematics
  * Technology is applied science which is applied math
  * Fitness – find some sport that you love to do
  * He’d drop history
* Scott
 * He would teach good strategy – strategy for your own life and other things
 * There’s obvious “good strategies” for things like business and politics that can be taught
 * COOPERATION
  * “Cooperation is the human advantage. Anytime you can cooperate with more humans, you win.” – Naval
  * The more people you can work with, the richer you will be
* Naval – “Never declare anyone your opponent – lots of times the people we think are our enemies, are not, and it’s just a one way street. Just assume you don’t have enemies.”


