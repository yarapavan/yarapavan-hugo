+++
title = "Money Advice from Nassim Nicholas Taleb"
description = ""
tags = ['advice']
categories = ['money']
date= 2018-05-01T21:07:06+05:30

+++
From [Esquire's article](https://www.esquire.com/lifestyle/money/a19181300/nassim-nicholas-taleb-money-advice/):

* I have never, ever borrowed a penny. So I have zero credit record. No loans, no mortgage, nothing. Ever. When I had no money, I rented. I have an allergy to borrowing and a scorn for people who are in debt, and I don’t hide it. I follow the Romans’ attitude that debtors are not free people.

* Better to miss a zillion opportunities than blow up once. I learned this at my first job, from the veteran traders at a New York bank that no longer exists. Most people don’t understand how to handle uncertainty. They shy away from small risks, and without realizing it, they embrace the big, big risk. Businessmen who are consistently successful have the exact opposite attitude: Make all the mistakes you want, just make sure you’re going to be there tomorrow.

* Don’t invest any energy in bargaining except when the zeros become large. Lose the small games and save your efforts for the big ones.

* There’s nothing wrong with being wrong, so long as you pay the price. A used-car salesman speaks well, they’re convincing, but ultimately, they are benefiting even if someone else is harmed by their advice. A bullshitter is not someone who’s wrong, it’s someone who’s insulated from their mistakes.

* Money can’t buy happiness, but the absence of money can cause unhappiness. Money buys freedom: intellectual freedom, freedom to choose who you vote for, to choose what you want to do professionally. But having what I call “fuck you” money requires a huge amount of discipline. The minute you go a penny over, then you lose your freedom again. If money is the cause of your worry, then you have to restructure your life.

* The best money I’ve ever spent has been spent on books. The stupidest thing I’ve ever spent money on? Books. Also, I cannot understand why anyone would spend any amount to enhance their social status.

* If nobody’s paying my salary, I don’t have to define myself. I find it arrogant to call yourself a philosopher or an intellectual, so I call myself a flaneur and I refuse all honors. As Cato once said, it’s better to be asked why there is no statue in your name than why there is one.




