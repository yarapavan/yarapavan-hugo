+++
title = "Notes - May 07, 2018"
description = ""
tags = ['links']
categories = ['link']
date= 2018-05-07T23:03:26+05:30

+++

* [The “Warren Buffett Archive” CNBC Website](https://buffett.cnbc.com/annual-meetings/). This CNBC website features recordings of every Berkshire shareholder meeting since 1994 with synchronized transcripts.
* [The Ultimate Guide to Making Smart Decisions](https://www.fs.blog/smart-decisions/). “I don’t want to be a great problem solver. I want to avoid problems — prevent them from happening by doing it right from the beginning.” Starts with this Peter Bevelin's quote, Shane Parrish runs through a list of general thinking concepts - inversion, second order thinking, the map is not the territor; top articles, books and a compendium of resources.
* [12 “Manager READMEs” from Silicon Valley’s Top Tech Companies](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe). What does tech management at Slack, HubSpot, Netflix, Etsy, Shopify, InVision, and more have in common? A lot, apparently!

* The quote that Jeff Bezos has had [hanging on his fridge](https://twitter.com/JeffBezos/status/992765968182001665) for years:

> "To laugh often and much; to win the respect of the intelligent people and the affection of children; to earn the appreciation of honest critics and endure the betrayal of false friends; to appreciate beauty; to find the beauty in others; to leave the world a bit better whether by a healthy child, a garden patch, or a redeemed social condition; to know that one life has breathed easier because you lived here. This is to have succeeded." --Ralph Waldo Emerson


