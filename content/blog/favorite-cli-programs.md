+++
title = "A list of terminal focused tools"
description = ""
tags = ['links']
categories = ['collection']
date= 2018-05-08T23:03:56+05:30

+++
Listing down links from the recent [HN discussion](HN Discussion - https://news.ycombinator.com/item?id=17011227) on favorite terminal programs:

* Erstwhile [popular blog](https://inconsolation.wordpress.com/), not updated in last couple of years. 
* Twitter CLI client, https://github.com/orakaro/rainbowstream
* Git Standup, https://github.com/kamranahmedse/git-standup
* Rq, Record Query - A tool for doing record analysis and transformation,  https://github.com/dflemstr/rq 
* AsciiCinema, https://github.com/asciinema
* A cat clone with wings, https://github.com/sharkdp/bat
* Visidata, with super powers, https://github.com/saulpw/visidata
* Curl-size, https://github.com/egoist/curl-size
* RipGrep, https://github.com/BurntSushi/ripgrep
* jq, https://github.com/stedolan/jq
* autojump, https://github.com/rupa/z
* notif tool, https://github.com/variadico/noti
* tmux, https://github.com/tmux/tmux/wiki
* glances, https://nicolargo.github.io/glances/
* rtv, https://github.com/michael-lazar/rtv
* discurses, https://github.com/topisani/Discurses
* tg, telegram client, https://github.com/vysheng/tg
* PDFtk, https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/
* gdb+peda, https://github.com/longld/peda
* CLI diassembler, https://github.com/radare/radare2
