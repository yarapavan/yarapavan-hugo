+++
categories = []
tags = []
description = ""
title = "Phones and Drones"
date = "2017-03-14T18:36:31+05:30"

+++

Chris Anderson, currently the CEO of 3D Robotics and founder of DIY Dones, says in an [edge.org conversation](https://www.edge.org/conversation/chris_anderson-closing-the-loop) - 

>Most of the devices in our life, from our cars to our homes, are “entropic,” which is to say they get worse over time. Every day they become more outmoded. But phones and drones are “negentropic” devices. Because they are connected, they get better, because the value comes from the software, not hardware

