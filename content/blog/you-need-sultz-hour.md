+++
title = "You're Too Busy"
description = "George Shultz on solitude"
tags = []
categories = []
date = 2017-08-06T14:34:31+05:30

+++

> When George Shultz was secretary of state in the 1980s, he liked to carve out one hour each week for quiet reflection. He sat down in his office with a pad of paper and pen, closed the door and told his secretary to interrupt him only if one of two people called:

>“My wife or the president,” Shultz recalled.

>Shultz, who’s now 96, told me that his hour of solitude was the only way he could find time to think about the strategic aspects of his job. Otherwise, he would be constantly pulled into moment-to-moment tactical issues, never able to focus on larger questions of the national interest. And the only way to do great work, in any field, is to find time to consider the larger questions.



