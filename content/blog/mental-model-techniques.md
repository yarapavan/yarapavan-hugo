+++
title = "Techniques - Mental Models"
description = "Techniques and Mental Models by Kent Buck"
tags = []
categories = []
date = 2018-01-09T14:34:31+05:30
+++

From Kent Buck's 2016 [FB post](https://www.facebook.com/notes/kent-beck/a-searching-and-fearless-intellectual-inventory/1179765038723025) :

When I get stuck, these are specific things I do that can unstuck me.

1. **Implication**. Hold your nose and do the math. Set aside “this is what I believe” or “this is what I would prefer”. Apply a model or analogy first, then analyze the results.

2. **Boundaries**. Be clear about what is my responsibility and what is not my responsibility. “I just got difficult feedback. How much is about me and how much is about the person giving the feedback?”
Waiting. Timing matters. If you are waiting for something specific, then waiting is an action.

3. **Contrarian**. You can’t create more impact than someone else by doing what they do. You either have to be better than they are or you have to do something different. I prefer to do something different. One simple heuristic is if someone says, “Obviously...” I always ask myself what would happen if the opposite were true. The greater the fury behind the “obviously”, the longer I’ll spend considering the counterfactual. Usually this leads to dead ends, but when it doesn’t, you won’t have any competition until it’s too late.

4. **Isolation**. When faced with a big, complicated, confusing situation (like tracking down a weird bug), first turn it into a small, simpler, still-confusing situation. Binary search is efficient but it’s hard not to try to understand first.

5. **Phase shift**. If you’re stuck doing A and then B, try doing half of A, the second half of A and the first half of B together, then the second half of B. (Implement-Test)(Implement-Test) becomes (Test-Implement)(Test-Implement). (Requires a cycle as a prereq).

6. **Believe the Market**. When one of my theories (see above) predicts that people will act in one way but a bunch of reasonable people act in a different way, I ask, “What theory would explain their behavior?” (I also use this on myself when I behave in ways I don’t like.)

7. **Thompson Sampling**. “A little nonsense/now and then/is relished by/the wisest man”. Put a little investment, occasionally, into even bad ideas, just in case.

8. **Reverse causality**. When someone says, “This person is senior so they write the meeting summary,” I immediately say, “Maybe they look senior because they write the meeting summaries.” Try writing the summary yourself and see if people treat your like you are senior.

