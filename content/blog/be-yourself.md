+++
title = "Be Yourself"
description = ""
tags = ['wisdom']
categories = []
date= 2018-08-07T22:48:12+05:30

+++
via [Warren Buffett](https://twitter.com/itswarenbuffett/status/1026218444914606082)

Be humble. You may be wrong.

Be hard working. You may be luckier.

Be kind. You may be remembered. 

Be generous. Others may reciprocate.

Be curious. You may stay teachable.

Be trustworthy. You may see further.

Be forgiving. You may feel lighter.

Be yourself. You will be happier.


