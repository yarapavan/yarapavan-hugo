+++
title = "The Most Important Question we Never Ask"
description = ""
tags = ['notes']
categories = []
date= 2018-06-04T17:56:03+05:30

+++

## The Most Important Question We Never Ask

> The key thing in economics, whenever someone makes an assertion to you, is to always ask, “And then what?” Actually, it’s not such a bad idea to ask it about everything. But you should always ask, “And then what?” 
> ~ Warren Buffett

---

> Two things. First, in 1703 the mathematician Gottfried von Leibniz told the scientist Jacob Bernoulli that nature does work in patterns, but “only for the most part.” The other part—the unpredictable part—tends to be where things matter the most. That’s where the action often is.

> Second, Pascal’s Wager. In making decisions under conditions of uncertainty, the consequences of being wrong must carry more weight than the probabilities of being right. You begin with something that’s obvious. But because it’s hard to accept, you have to keep reminding yourself: We don’t know what’s going to happen with anything, ever. And so it’s inevitable that a certain percentage of our decisions will be wrong. There’s just no way we can always make the right decision.

> That doesn’t mean you’re an idiot. But it does mean you must focus on how serious the consequences could be if you turn out to be wrong: Suppose this doesn’t do what I expect it to do. What’s going to be the impact on me? If it goes wrong, how wrong could it go and how much will it matter?

> Pascal’s Wager doesn’t mean that you have to be convinced beyond doubt that you are right. But you have to think about the consequences of what you’re doing and establish that you can survive them if you’re wrong. Consequences are more important than probabilities.

> …Risk-taking is an inevitable ingredient in investing, and in life, but never take a risk you do not have to take.

> -Peter Bernstein
