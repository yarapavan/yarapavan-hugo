+++
title = "Notes - 28th March 2018"
description = "Links"
tags = ["things-learnt"]
categories = ['links']
date= 2018-03-28T22:08:43+05:30

+++

* [Idea Sandbox](http://idea-sandbox.com/what-we-do/) I see this linked from Derek Sivers article and bookmarking it. Seems to be a repository of problem-solving articles and creative tools. Here is an example of PMI decision making method, from one of the articles:

    >  __PMI Method__

	> A third way to examine choices is the PMI Method, invented by Edward de Bono. PMI is an acronym for Plus, Minus, Interesting. It takes the Scored Pro & Con a step further by forcing us to think about “what is interesting” about the choice.

	> __Plus__ are the pros. What’s good about the idea.

	> __Minus__ are the cons, the bad points of the idea. And finally,

	> __Interesting__. What is interesting? What are the possibilities?

	> This chart is especially handy when brainstorming and you have ideas that are not a pro or a con. Rather, ideas interesting to think about. To calculate your PMI score add up your (Plus) + (Minus) + (Interesting) scores. Items in the “interesting” column can score as a plus or a minus depending on the implication of the thought.

* [Derek Sivers Blog Post - Detailed dreams blind you to new means](https://sivers.org/details). 

    > New technologies make old things easier, and new things possible. That’s why you need to re-evaluate your old dreams to see if new means have come along. Some actors move to Hollywood hoping to get noticed. Others use every new outlet to make themselves unavoidable.

    > Some authors are just waiting for a publisher to sign them. Others are getting rich just self-publishing.

    > You need to distinguish between what is your real goal, and what are the unnecessary details. Don’t let the details distract you from your goal.

    > For each of your dreams, occasionally ask yourself what was the real point. Then look for a better way to get to that point.

    > Let go of out-dated dreams that keep you from noticing what’s here now.




