+++
title = "Always and Never"
description = "Always and Never"
tags = []
categories = []
date= 2018-02-20T19:16:53+05:30

+++
via [dollarsanddatda](https://ofdollarsanddata.com/the-abnormal-environment-f4ce2794d17c):

**Always/Never**

The problem with designing a nuclear bomb is the inevitable tradeoff between reliability and safety. Schlosser explains in Command and Control:

```
A safety mechanism that made a bomb less likely to explode during an accident could also, during wartime, render it more likely to be a dud. The contradiction between these two design goals was succinctly expressed by the words “always/never.” Ideally, a nuclear weapon would always detonate when it was supposed to — and never detonate when it wasn’t supposed to.
```
