+++
title = "How to fail as a new engineering manager"
description = ""
tags = ['career']
categories = ['advice']
date= 2018-09-18T08:59:09+05:30

+++

From [@hashbrown1](https://twitter.com/hashbrown1) blog post - [How to fail as a new engineering manager](https://blog.usejournal.com/how-to-fail-as-a-new-engineering-manager-30b5fb617a):

1. Keep coding.
2. Focusing only on the work, not the people.
3. Measuring your value by your output
4. Expecting without expressing.
5. Leaving the team out of commitments
6. Mistaking directing for leading
7. Avoiding hard conversations
8. Stop learning your craft

