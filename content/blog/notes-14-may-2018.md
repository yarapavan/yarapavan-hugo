+++
title = "Notes - May 14, 2018"
description = "Links"
tags = ['cli']
categories = []
date= 2018-05-14T23:50:37+05:30

+++
* [Most Useful Command Line Tools: 50 Cool Tools to Improve Your Workflow, Boost Productivity, and More](https://stackify.com/top-command-line-tools/). A list of useful CLI tools in the categories - Web Development, Productivity, Utility, and Visual Entertainment. Interesting to explore - [surge.sh](https://surge.sh/), [Diff2html](https://github.com/rtfpessoa/diff2html-cli), [HTTPie](https://httpie.org/), [Pandoc](http://pandoc.org/), [icdiff](https://github.com/jeffkaufman/icdiff), [taskwarrior](https://taskwarrior.org/), [doing](http://brettterpstra.com/projects/doing/), [Simple command line timetracker](http://rubygems.org/gems/timetrap), [uber-cli](https://github.com/jaebradley/uber-cli), [jq](https://stedolan.github.io/jq/), [autojump](https://github.com/wting/autojump), [ranger](http://ranger.nongnu.org/), [GoTTY](https://github.com/yudai/gotty), [Aria2](https://github.com/aria2/aria2), [ttygif](https://github.com/icholy/ttygif), [Pockyt](https://github.com/arvindch/pockyt), [cmus](https://github.com/cmus/cmus), [youtube-dl](https://rg3.github.io/youtube-dl/), [medium-cli](https://github.com/djadmin/medium-cli), and [hget](https://github.com/bevacqua/hget),[timetrap](https://github.com/samg/timetrap).

* [setapp osx app collection](setapp.com) seems to be interesting.
* [AskMeEvery](https://www.askmeevery.com/). You sign up with a question you want to track. Every day, at the time you choose, you'll receive that question as an email or text. Reply with your answer, and then login to see an intelligent graph of your responses over time.

