+++
title = "Google Drive Clients"
categories = []
date = "2017-03-14T19:23:28+05:30"
tags = []
description = "Linux clients for Google Drive (personal) or Gsuite"

+++
Do you know that there is no official Google Drive client for Linux? And, that you have to rely on third-party tools and SaaS serivces to backup/transfer your Google drive data?

Recently, I've been involved in one such migration exercise, where we have to resort to using a number of third-party and open source tools to backup/transfer. 

Here is the list of tools we evaluted:

1. [Gdrive](https://github.com/prasmussen/gdrive). The most popular and verstaile CLI client for Google Drive. A tutorial [here](https://www.howtoforge.com/tutorial/how-to-access-google-drive-from-linux-gdrive/), video [here](https://www.youtube.com/watch?v=st590nXaKQ0). OpenSource.

2. [Overgrive](https://www.thefanclub.co.za/overgrive). A good desktop client solution for Linux. Supports a lot of distributions and desktop. Paid.

3. [Skicka](https://github.com/google/skicka). Skicka is swedish for "to send" which vaguely alludes to what the tool does. An unofficial google product with 'alpha' quality, as termed by its developers. Promising. Supports encryption.  

4. [Drive](https://github.com/odeke-em/drive). Originally developed by a Googler, this project is very active again. Supports all popular Linux distributions. Opensource.  

5. [rclone](https://github.com/ncw/rclone). The swiss army knife program to sync files and directories to and from Google Drive, S3, Dropbox, OneDrive or local file systems. Has a ton of features. Opensource. 

6. [Grive2](http://yourcmc.ru/wiki/Grive2). It is the fork of abandoned [Grive](https://github.com/Grive/grive) with added support for new Drive REST API and some new features. Can do two-side synchronization. There are few limitations like not supporting downloading of Google documents, undefined behavior with multiple folders/files with same name or with multiple parents. Opensource.

7. [CloudBerry Backup](https://www.cloudberrylab.com/backup/linux.aspx). From the popular CloudBerry Lab folks. Has clients for all major platforms. Supports Amazon S3, Azure, GCP etc. Freeware version available. Not opensource.


I'm tuned towards [1] and [5] for now having used them with some reasonable success in the past.

What is your recommendation?

