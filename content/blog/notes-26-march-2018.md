+++
title = "Notes - 26th March 2018"
description = "Things I learnt today"
tags = ['things-learnt']
categories = []
date= 2018-03-26T14:10:16+05:30

+++

* [High-precision indoor positioning framework, version 3](https://github.com/schollz/find3). Simply put, FIND will allow you to replace tons of motion sensors with a single smartphone! Looks an interesting project. Follow HN discussion at https://news.ycombinator.com/item?id=16672320 #reference #readlater

* [Awesome list of Important Podcasts for software engineers](https://github.com/rShetty/awesome-podcasts)
* [The Key to Good Luck Is an Open Mind](http://nautil.us/blog/-the-key-to-good-luck-is-an-open-mind). Lucky people “appear to have an uncanny ability to be in the right place at the right time and enjoy more than their fair share of lucky breaks.”

What do these people have that the rest of us don’t? It turns out “ability” is the key word here. Beyond their level of privilege or the circumstances they were born into, the luckiest people may have a specific set of skills that bring chance opportunities their way. Somehow, they’ve learned ways to turn life’s odds in their favor. 

[Wiseman] He turned these findings into a “luck school” where people could learn luck-inducing techniques based on four main principles of luck: maximizing chance opportunities, listening to your intuition, expecting good fortune, and turning bad luck to good. The strategies included using meditation to enhance intuition, relaxation, visualizing good fortune, and talking to at least one new person every week. A month later, he followed up with participants. Eighty percent said they were happier, luckier people.
*
