+++
title = "Notes - 14th April 2018"
description = ""
tags = ['links']
categories = []
date= 2018-04-14T11:56:02+05:30

+++

* [The dots do matter: How to scam a Gmail user](https://jameshfisher.com/2018/04/07/the-dots-do-matter-how-to-scam-a-gmail-user.html). In Gmail, dots do not matter but is not the same case with other online accounts including paid serviceslike netflix, dropbox, airbnb and others.
* [An Art Leveraging a Science](http://www.collaborativefund.com/blog/an-art-leveraging-a-science/). Albert Einstein’s favorite idea was called gedankenerfahrung.It’s when he’d close his eyes and imagine how physics worked in the real world, instead of formulas drawn on a chalkboard. Many quotes are misattributed to Einstein. But one he actually said was, “Imagination is more important than knowledge.” For him, it was an art leveraging a science.  Communication is the same to investing.
* [Labnol's list of best android apps for 2018](https://www.labnol.org/internet/best-android-apps/28644/). I use (or used) these apps - Authy, Newpiple, Files Go,Timbre, Sesame Shortcuts, APK updater, Keep, zoho notebook, Photoscan, Reddit Sync, Recent Notifications, Samsung Voice Recorder, Trello, Firefox Focus, Hermit, Twilight, Vidoder, SnapSeed, SMS organizer, Push Bullet, Airdroid, Pocket, Office Lens, IFTTT, Fing, Fake GPS etc.

