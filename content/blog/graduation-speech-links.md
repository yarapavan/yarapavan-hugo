+++
title = "Graduation Speech Links from Evernote"
description = ""
tags = ['links']
categories = []
date= 2018-05-19T22:49:19+05:30

+++

Link: https://blog.evernote.com/blog/2018/05/18/the-graduation-speech-excellent-advice-not-just-for-students/

Highlights:

1. Love what you do or stop doing it. The price of hating your life is too high.

> “I’m convinced that the only thing that kept me going was that I loved what I did. You’ve got to find what you love. And that is as true for your work as it is for your lovers. Your work is going to fill a large part of your life, and the only way to be truly satisfied is to do what you believe is great work. And the only way to do great work is to love what you do. If you haven’t found it yet, keep looking. Don’t settle.”
> — [Steve Jobs](https://www.youtube.com/watch?v=UF8uR6Z6KLc), Stanford, 2005

 

> “I stopped pretending to myself that I was anything other than what I was, and began to direct all my energy into finishing the only work that mattered to me. Had I really succeeded at anything else, I might never have found the determination to succeed in the one arena I believed I truly belonged.”
> — [J. K. Rowling](https://www.youtube.com/watch?v=wHGqp8lz36c): Harvard University, 2008

 

> “I implore you to not fear what you are drawn to, for rejecting that is to stymie the very patterns that make you, well, you. Imagine the torture of putting yourself through that. No matter how much money you make, you will not be happy spending your time imagining the path you wished you’d chosen.”
> — [Max Londberg](https://uoregon.uloop.com/news/view.php/86149/a-graduation-speech-on-how-to-fail): University of Oregon, 2013

2. Success and failure. Decide for yourself what these ideas mean to you.

> “If everybody followed their first dreams in life, the world would be ruled by cowboys and princesses.” — Stephen Colbert: Northwestern University, 2011

 
> “David Letterman wanted to be Johnny Carson, and was not, and as a result, my generation of comedians wanted to be David Letterman. And none of us are. My peers and I have all missed that mark in a thousand different ways. But the point is this: It is our failure to become our perceived ideal that ultimately defines us and makes us unique. It’s not easy, but if you accept your misfortune and handle it right, your perceived failure can become a catalyst for profound reinvention.” — Conan O’Brien: Dartmouth College, 2011

 
> “If you don’t fail, you’re not even trying. My wife told me this great expression, ‘To get something you never had, you have to do something you never did.” — Denzel Washington: University of Pennsylvania, 2011


3. Open Up. Explore. Question habits. Listen up. Change.

> “Because as my grandmother used to tell me, every time a fool speaks, they are just advertising their own ignorance. Let them talk. If you don’t, you just make them a victim, and then they can avoid accountability.” — President Barack Obama: Howard University, 2016

 

> “As you start your journey, the first thing you should do is throw away that store-bought map and begin to draw your own.” — Michael Dell: University of Texas at Austin, 2003

4. Explore.  The good stuff is worth looking for

> “One of the greatest gifts you can give yourself, right here, right now, in this single, solitary, monumental moment in your life, is to decide, without apology, to commit to the journey, and not to the outcome.” — Joyce DiDonato: Juilliard School, 2014

 
> “Just because something doesn’t confirm your existing beliefs does not mean it’s a hoax. The smartest and most successful people I know are the people who are constantly evolving, always learning. And some of them, believe it or not, are smart enough to occasionally change their minds.” — Stephanie Ruhle: Lehigh University, 2017

> “If you love only yourself, you will serve only yourself. And you will have only yourself. So no more winning. Instead, try to love others and serve others, and hopefully find those who love and serve you in return.” — Stephen Colbert: Northwestern University, 2011

> “So how do you know what is the right path to choose to get the result that you desire? And the honest answer is this: You won’t.” — Jon Stewart: College of William and Mary 2004

5. Forget about it. Sometimes a short memory is your very best friend. 

> “Fall forward. Reggie Jackson struck out 2,600 times in his career, the most in the history of baseball. But you don’t hear about the strikeouts. People remember the home runs. Fall forward. Thomas Edison conducted 1,000 failed experiments. Did you know that? I didn’t know that because the 1,001st was the light bulb. Fall forward. Every failed experiment is one step closer to success.” — Denzel Washington: University of Pennsylvania, 2011

 

> “I wish someone had said to me that it’s normal to feel lost for a little while.” — Ira Glass: Goucher College, 2012

> “Rehearsal’s over. You’re going out there now, you’re going to do this thing. How you live matters. You’re going to fall down, but the world doesn’t care how many times you fall down, as long as it’s one fewer than the number of times you get back up.” — Aaron Sorkin: Syracuse University 2012

#### Polonius Advice

```
Give thy thoughts no tongue,
Nor any unproportioned thought his act.
Be thou familiar but by no means vulgar.
Those friends thou hast, and their adoption tried,
Grapple them unto thy soul with hoops of steel,
But do not dull thy palm with entertainment
Of each new-hatch’d, unfledged comrade.
Beware of entrance to a quarrel, but being in,
Bear ’t that th’ opposèd may beware of thee.
Give every man thy ear but few thy voice.
Take each man’s censure but reserve thy judgment.
Costly thy habit as thy purse can buy,
But not expressed in fancy—rich, not gaudy,

…Neither a borrower nor a lender be,
For loan oft loses both itself and friend,
And borrowing dulls the edge of husbandry.
This above all: to thine own self be true,
And it must follow, as the night the day,
Thou canst not then be false to any man.

— William Shakespeare: Hamlet (Act I, Scene iii)

```




