+++
title = "Pmarca Guide to Personal Productivity"
description = ""
tags = ['things-learnt']
categories = ['links']
date= 2018-03-30T21:39:36+05:30

+++


[Pmarca Guide to Personal Productivity](https://pmarchive.com/guide_to_personal_productivity.html) was written by Marc Andreessen and originally published on his blog, blog.pmarca.com. These articles are probably some of the best writings on business and startups anywhere but they were taken down years ago.

__Techniques__

* __Don't keep a schedule__. By not keeping a schedule, you can always __work on whatever is most important or most interesting__, at any time. The idea came from __A Perfect Mess__ book.

  > If you have at any point in your life lived a relatively structured existence -- probably due to some kind of job with regular office hours, meetings, and the like -- you will know that there is nothing more liberating than looking at your calendar and seeing nothing but free time for weeks ahead to work on the most important things in whatever order you want.

* Keep three and only three lists: a _Todo List, a Watch List, and a Later List_
 * Into the __Todo List__ goes all the stuff you "must" do -- commitments, obligations, things that have to be done. A single list, possibly subcategorized by timeframe (today, this week, next week, next month).
 * Into the __Watch List__ goes all the stuff going on in your life that you have to follow up on, wait for someone else to get back to you on, remind yourself of in the future, or otherwise remember. 
 * Into the __Later List__ goes everything else -- everything you might want to do or will do when you have time or wish you could do.
 * _If it doesn't go on one of those three lists, it goes away._

* Each night before you go to bed, __prepare a 3x5 index card with a short list of 3 to 5 things that you will do the next day__.
 * And then, the next day, do those things.
 * People who have tried lots of productivity porn techniques will tell you that this is one of the most successful techniques they have ever tried.
    Once you get into the habit, you start to realize how many days you used to have when you wouldn't get 3 to 5 important/significant/meaningful things done during a day.

* Then, throughout the rest of the day, use the back of the 3x5 card as your __Anti-Todo List__.
* And then at the end of the day, before you prepare tomorrow's 3x5 card, take a look at today's card and its Anti-Todo list and marvel at all the things you actually got done that day.  Then tear it up and throw it away.  Another day well spent, and productive.

* __Structured Procrastination__.

  The gist of Structured Procrastination is that you should never fight the tendency to procrastinate -- instead, you should use it to your advantage in order to get other things done.  Generally in the course of a day, there is something you have to do that you are not doing because you are procrastinating.  While you're procrastinating, just do lots of other stuff instead.

 All procrastinators put off things they have to do. Structured procrastination is the art of making this bad trait work for you. The key idea is that procrastinating does not mean doing absolutely nothing.

 > Structured procrastination means shaping the structure of the tasks one has to do in a way that exploits this fact. The list of tasks one has in mind will be ordered by importance. Tasks that seem most urgent and important are on top. But there are also worthwhile tasks to perform lower down on the list. Doing these tasks becomes a way of not doing the things higher up on the list. With this sort of appropriate task structure, the procrastinator becomes a useful citizen. Indeed, the procrastinator can even acquire, as I have, a reputation for getting a lot done.

* __Strategic Incompetence__.

 > The best way to to make sure that you are never asked to do something again is to royally screw it up the first time you are asked to do it. Or, better yet, just say you know you will royally screw it up -- maintain a strong voice and a clear gaze, and you'll probably get off the hook.

* __Do email exactly twice a day__ --  once first thing in the morning, and once at the end of the workday. Allocated half an hour or whatever it takes, but otherwise, keep your email client shut and your email notifications turned off.  Only doing email twice a day will make you far more productive for the rest of the day. The problem with email is that getting an email triggers that same endorphin hit I mentioned above -- the one that a mouse gets when he bonks on the button in the cage and gets a food pellet.  Responding to an email triggers that same hit.

* When you do process email, do it like this: 
 * First, always finish each of your two daily email sessions with a completely empty inbox.
 * Second, when doing email, either answer or file every single message until you get to that empty inbox state of grace.
 * Third, emails relating to topics that are current working projects or pressing issues go into temporary subfolders of a folder called Action.
 * Fourth, aside from those temporary Action subfolders, only keep three standing email folders: __Pending, Review, and Vault__.
 
* __Don't answer the phone__. Let it go to voicemail, and then every few hours, screen your voicemails and batch the return calls. Say, twice a day. Have two cell phones with different numbers -- one for key family members, your closest friends, and your boss and a few coworkers, and the other for everyone else.  Answer the first one when it rings, but never answer the second one.

* __Hide in an IPod__. People, for some reason, feel much worse interrupting you if you are wearing headphones than if you're not.

* __Start the day with a real, sit-down breakfast__.

* Only agree to new commitments when __both your head and your heart say yes__. If you get that little adrenaline spike (in a good way) when you think about it, then your heart is saying yes.  The corollary, of course, is that when your head says no and your heart says yes, your mouth should generally say yes as well :-).  But not when your head says yes and your heart says no
 
* __Do something you love__.



 










