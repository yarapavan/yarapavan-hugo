+++
title = "Notes - 24th March 2018"
description = ""
tags = ['things-learnt']
categories = []
date= 2018-03-24T21:07:43+05:30

+++

### Links
* [17 productivity and focus apps/extensions from RadReadings](https://rad.family/perfect-your-craft-productivity-and-focus/). Looks interesting - [StayFocusd, which is now redirecting to freedom](http://www.stayfocusd.com/); [batchedinbox](http://try.batchedinbox.com/); [Bookcision](https://www.norbauer.com/bookcision/). Of the tools mentioned, I use Google Auto Advance, Evernote, keyboard expander (similar to textexpander but less powerful), onetab, instapaper (occasionally) and quitter.
* [How I get work done by Marie Kondo](https://www.thecut.com/2018/03/marie-kondo-lifechanging-magic-tidying-up-interview.html). On the significance of things Ask yourself this, often: Is what I’m doing connected to the kind of life that will spark joy for me? By thinking through this, the steps required to have that kind of life become clearer. I really do recommend tidying. Make sure all the things in your home spark joy and that all the things have their proper place. Just getting your home in order can really make your life clearer. It can make your head clearer.


