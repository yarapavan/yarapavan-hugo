+++
title = "Trading Time"
description = "The Most Important Asset is TIME"
tags = ['time']
categories = ['wisdom']
date= 2018-07-17T18:18:06+05:30

+++
Via []@dollarsanddata](https://ofdollarsanddata.com/the-most-important-asset/)

> Think of it this way:  Each week you get 10,000 minutes to live your life (it’s actually 10,080 minutes, but let’s round for simplicity).  If these 10,000 minutes represent 100% of your week, then each minute is 1 basis point (i.e. one hundredth of a percent).  If you sleep 7 hours a night that’s 2,940 minutes or ~30% of your week.  If you work 40 hours, that’s another 2,400 minutes or 24% gone.  Therefore, if you spend 5 minutes reading one of my posts, that’s 5 basis points of your week that you are allocating to me.  I know that doesn’t sound like much, but when you consider the endless amount of content competing for your attention, it’s amazing.
## Knowing When To Trade

1. The key to changing your worldview on time is knowing when to trade it for money.  Obviously, if you don’t have much money, you don’t have many options for trading, but as you gain wealth, this tradeoff will become more relevant.  The traditional way to think about when to trade your money for your time has been to pay for anything that is below your hourly wage.  This explains how one could justify paying for a cleaning or laundry service.  I generally agree with this, but it can be expanded.
2. You should also trade your money for anything that you would regret missing on your deathbed.  If you can imagine yourself regretting skipping an event in your final days of life, then you should trade money for it today.  This worldview is something I only adopted recently, but it has made my decision-making process so much easier when it comes to tradeoffs like these.  For example, I would probably spend money to visit a good friend, but I wouldn’t spend money to have a nice car.  I will (hopefully) remember seeing my friend on my deathbed, but the car?  No chance.
``
