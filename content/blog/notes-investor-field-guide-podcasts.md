+++
title = "2nd Anniversary Lessons from Investor Field Guide Podcasts"
description = ""
tags = ['lessons']
categories = ['notes']
date= 2018-09-17T11:17:12+05:30

+++

via [Patrick O'Shaughnessy](http://investorfieldguide.com/trailmagic/) of [Invest Like the Best](http://investorfieldguide.com/podcast/) podcast fame:

> Summed up, what I’ve learned from these people is to follow your own way, always. Figure out the right units of exploration, embrace strange intersections, and carefully consider what could go wrong. Rest when you need it, be dogged and aggressive when the situation calls for it, but just keep going. Do it all with respect for others and as much trail magic as you can muster.
