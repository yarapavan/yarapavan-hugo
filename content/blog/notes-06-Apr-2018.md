+++
title = "Notes - April 6, 2018"
description = ""
tags = ['links']
categories = []
date= 2018-04-06T11:55:39+05:30

+++

* [Interactive terminal for tracking crypto currencies](https://github.com/miguelmota/cointop). Go Project (ensure that your go path is set properly). Data gets polled every minute from [coin market](https://coinmarketcap.com/)
* [Free, Secured and JSON based cloud data store for small projects](https://github.com/bluzi/jsonstore). Just enter https://www.jsonstore.io/, copy the URL and start sending HTTP requests to communicate with your datastore. POST requests will save data, PUT requests modify data, DELETE requests delete data and GET requests retrieves data.
* [One dependency to rule them all](https://github.com/miedzinski/import-pypi). Copy pypi.py into your site-packages directory or straight into your project. Don't bother using pip, requirements.txt and all that crap. ```import pypi ``` is what all you need.

* [Extract API endpoints from APK](https://github.com/UltimateHackers/Diggy). Diggy can extract endpoints/URLs from apk files. It saves the result into a txt file for further processing.

* [Convert text documents to high fidelity audio(books)](https://github.com/danthelion/doc2audiobook). Extract text from a document (textract) and convert it into a natural sounding synthesised speech (Cloud Text-To-Speech), which is able to leverage Deepminds Wavenet models.

* [Learn Block Chain in 2 months](https://github.com/llSourcell/Learn_Blockchain_in_2_months). This is the code for "Learn Blockchain in 2 Months" by Siraj Raval on Youtube

* [A tool to scrape LinkedIn without API restrictions for data reconnaissance](https://github.com/dchrastil/ScrapedIn). Tool to scrape LinkedIn. This tool assists in performing reconnaissance using the LinkedIn.com website/API. Provide a search string just as you would on the original website and let ScrapedIn do all the dirty work. Output is stored as an XLSX file, however it is intended to be used with Google Spreadsheets. After importing the XLSX into Google Spreadsheets there will be a "dataset" worksheet and a "report" worksheet.  

