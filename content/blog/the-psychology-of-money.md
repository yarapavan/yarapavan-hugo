+++
title = "Notes from The Psychology of Money"
description = ""
tags = ['classics']
categories = ['long-reads']
date= 2018-06-07T06:55:25+05:30

+++

Morgan Housel, of the The Collaborative Fund, wrote a classic note on biases, flaws and other human behavior, we exhibit when we deal with money. The full post is [here](http://www.collaborativefund.com/blog/the-psychology-of-money/) and a PDF copy is [here](http://www.collaborativefund.com/uploads/The%20Psychology%20of%20Money-9dbc86.pdf).

Quoting from the article:

> …investing is not the study of finance. It’s the study of how people behave with money. And behavior is hard to teach, even to really smart people. You can’t sum up behavior with formulas to memorize or spreadsheet models to follow. Behavior is inborn, varies by person, is hard to measure, changes over time, and people are prone to deny its existence, especially when describing themselves.

> …The finance industry talks too much about what to do, and not enough about what happens in your head when you try to do it.

> …This report describes 20 flaws, biases, and causes of bad behavior I’ve seen pop up often when people deal with money

### Biases:

*  Earned success and deserved failure fallacy: A tendency to underestimate the role of luck and risk, and a failure to recognize that luck and risk are different sides of the same coin.

> I like to ask people, “What do you want to know about investing that we can’t know?”
> Years ago I asked economist Robert Shiller the question. He answered, “The exact role of luck in successful outcomes.”
>  If I say, “There are a billion investors in the world. By sheer chance, would you expect 100 of them to become billionaires predominately off luck?” You would reply, “Of course.” But then if I ask you to name those investors – to their face – you will back down. That’s the problem.

> The same goes for failure. Did failed businesses not try hard enough? Were bad investments not thought through well enough? Are wayward careers the product of laziness?

> Some people are born into families that encourage education; others are against it. Some are born into flourishing economies encouraging of entrepreneurship; others are born into war and destitution. I want you to be successful, and I want you to earn it. But realize that not all success is due to hard work, and not all poverty is due to laziness. Keep this in mind when judging people, including yourself.

*  Cost avoidance syndrome: A failure to identify the true costs of a situation, with too much emphasis on financial costs while ignoring the emotional price that must be paid to win a reward.

> Every money reward has a price beyond the financial fee you can see and count. Accepting that is critical. Scott Adams once wrote: “One of the best pieces of advice I’ve ever heard goes something like this: If you want success, figure out the price, then pay it. It sounds trivial and obvious, but if you unpack the idea it has extraordinary power.” Wonderful money advice.

*  Rich man in the car paradox.

> The paradox of wealth is that people tend to want it to signal to others that they should be liked and admired. But in reality those other people bypass admiring you, not because they don’t think wealth is admirable, but because they use your wealth solely as a benchmark for their own desire to be liked and admired.

* A tendency to adjust to current circumstances in a way that makes forecasting your future desires and actions difficult, resulting in the inability to capture long-term compounding rewards that come from current decisions.

> Things change. And it’s hard to make long-term decisions when your view of what you’ll want in the future is so liable to shift.

> This gets back to the first rule of compounding: Never interrupt it unnecessarily. But how do you not interrupt a money plan – careers, investments, spending, budgeting, whatever – when your life plans change? It’s hard.

> There is no solution to this. But one thing I’ve learned that may help is coming back to balance and room for error. Too much devotion to one goal, one path, one outcome, is asking for regret when you’re so susceptible to change.

* Anchored-to-your-own-history bias: Your personal experiences make up maybe 0.00000001% of what’s happened in the world but maybe 80% of how you think the world works.

> And that’s a problem. When everyone has experienced a fraction of what’s out there but uses those experiences to explain everything they expect to happen, a lot of people eventually become disappointed, confused, or dumbfounded at others’ decisions.

> A team of economists once crunched the data on a century’s worth of people’s investing habits and concluded: “Current [investment] beliefs depend on the realizations experienced in the past.”

> Keep that quote in mind when debating people’s investing views. Or when you’re confused about their desire to hoard or blow money, their fear or greed in certain situations, or whenever else you can’t understand why people do what they do with money. Things will make more sense.


* Historians are Prophets fallacy: Not seeing the irony that history is the study of surprises and changes while using it as a guide to the future. An overreliance on past data as a signal to future conditions in a field where innovation and change is the lifeblood of progress.

> The cornerstone of economics is that things change over time, because the invisible hand hates anything staying too good or too bad indefinitely. Bill Bonner once described how Mr. Market works: “He’s got a ‘Capitalism at Work’ T-shirt on and a sledgehammer in his hand.” Few things stay the same for very long, which makes historians something far less useful than prophets.

> The most important driver of anything tied to money is the stories people tell themselves and the preferences they have for goods and services. Those things don’t tend to sit still. They change with culture and generation. And they’ll keep changing.

> The further back in history you look, the more general your takeaways should be. General things like people’s relationship to greed and fear, how they behave under stress, and how they respond to incentives tends to be stable in time. The history of money is useful for that kind of stuff. But specific trends, specific trades, specific sectors, and specific causal relationships are always a showcase of evolution in progress.

* The seduction of pessimism in a world where optimism is the most reasonable stance.

> Most promotions of optimism, by the way, are rational. Not all, of course. But we need to understand what optimism is. Real optimists don’t believe that everything will be great. That’s complacency. Optimism is a belief that the odds of a good outcome are in your favor over time, even when there will be setbacks along the way. The simple idea that most people wake up in the morning trying to make things a little better and more productive than wake up looking to cause trouble is the foundation of optimism. It’s not complicated. It’s not guaranteed, either. It’s just the most reasonable bet for most people. The late statistician Hans Rosling put it differently: “I am not an optimist. I am a very serious possibilist.”

* Underappreciating the power of compounding, driven by the tendency to intuitively think about exponential growth in linear terms.

> The danger here is that when compounding isn’t intuitive, we often ignore its potential and focus on solving problems through other means. Not because we’re overthinking, but because we rarely stop to consider compounding potential.

> Physicist Albert Bartlett put it: “The greatest shortcoming of the human race is our inability to understand the exponential function.”

> The counterintuitiveness of compounding is responsible for the majority of disappointing trades, bad strategies, and successful investing attempts. Good investing isn’t necessarily about earning the highest returns, because the highest returns tend to be one-off hits that kill your confidence when they end. It’s about earning pretty good returns that you can stick with for a long period of time. That’s when compounding runs wild.

* Attachment to social proof in a field that demands contrarian thinking to achieve above-average results.


> Here’s the thing: Most attempts at contrarianism is just irrational cynicism in disguise – and cynicism can be popular and draw crowds. Real contrarianism is when your views are so uncomfortable and belittled that they cause you to second guess whether they’re right. Very few people can do that. But of course that’s the case. Most people can’t be contrarian, by definition. Embrace with both hands that, statistically, you are one of those people.

* An appeal to academia in a field that is governed not by clean rules but loose and unpredictable trends.

>  A hard reality is that what often matters most in finance will never win a Nobel Prize: Humility and room for error.

* The social utility of money coming at the direct expense of growing money; wealth is what you don’t see.

> Wealth, in fact, is what you don’t see. It’s the cars not purchased. The diamonds not bought. The renovations postponed, the clothes forgone and the first-class upgrade declined. It’s assets in the bank that haven’t yet been converted into the stuff you see.

> But that’s not how we think about wealth, because you can’t contextualize what you can’t see.

> You can laugh. But the truth is, yes, people need to be told that. When most people say they want to be a millionaire, what they really mean is “I want to spend a million dollars,” which is literally the opposite of being a millionaire. This is especially true for young people.

> A key use of wealth is using it to control your time and providing you with options. Financial assets on a balance sheet offer that. But they come at the direct expense of showing people how much wealth you have with material stuff.

* A tendency toward action in a field where the first rule of compounding is to never interrupt it unnecessarily.

> When volatility is guaranteed and normal, but is often treated as something that needs to be fixed, people take actions that ultimately just interrupts the execution of a good plan. “Don’t do anything,” are the most powerful words in finance. But they are both hard for individuals to accept and hard for professionals to charge a fee for. So, we fiddle. Far too much.

* Underestimating the need for room for error, not just financially but mentally and physically.

> Ben Graham once said, “The purpose of the margin of safety is to render the forecast unnecessary.”

> People underestimate the need for room for error in almost everything they do that involves money. Two things cause this: One is the idea that your view of the future is right, driven by the uncomfortable feeling that comes from admitting the opposite. The second is that you’re therefore doing yourself economic harm by not taking actions that exploit your view of the future coming true.

> But room for error is underappreciated and misunderstood. It’s often viewed as a conservative hedge, used by those who don’t want to take much risk or aren’t confident in their views. But when used appropriately it’s the opposite. Room for error lets you endure, and endurance lets you stick around long enough to let the odds of benefiting from a low-probability outcome fall in your favor. The biggest gains occur infrequently, either because they don’t happen often or because they take time to compound. So the person with enough room for error in part of their strategy to let them endure hardship in the other part of their strategy has an edge over the person who gets wiped out, game over, insert more tokens, when they’re wrong.

* A tendency to be influenced by the actions of other people who are playing a different financial game than you are.

> Few things matter more with money than understanding your own time horizon and not being persuaded by the actions and behaviors of people playing different games.

> This goes beyond investing. How you save, how you spend, what your business strategy is, how you think about money, when you retire, and how you think about risk may all be influenced by the actions and behaviors of people who are playing different games than you are.

> Personal finance is deeply personal, and one of the hardest parts is learning from others while realizing that their goals and actions might be miles removed from what’s relevant to your own life.

* An attachment to financial entertainment due to the fact that money is emotional, and emotions are revved up by argument, extreme views, flashing lights, and threats to your wellbeing.

> It helps, I’ve found, when making money decisions to constantly remind yourself that the purpose of investing is to maximize returns, not minimize boredom. Boring is perfectly fine. Boring is good. If you want to frame this as a strategy, remind yourself: opportunity lives where others aren’t, and others tend to stay away from what’s boring.

* Optimism bias in risk-taking, or “Russian Roulette should statistically work” syndrome: An over attachment to favorable odds when the downside is unacceptable in any circumstance.

> Nassim Taleb says, “You can be risk loving and yet completely averse to ruin.”

> The idea is that you have to take risk to get ahead, but no risk that could wipe you out is ever worth taking. The odds are in your favor when playing Russian Roulette. But the downside is never worth the potential upside.

> A key point here is that few things in money are as valuable as options. The ability to do what you want, when you want, with who you want, and why you want, has infinite ROI.

* A preference for skills in a field where skills don’t matter if they aren’t matched with the right behavior.

* Denial of inconsistencies between how you think the world should work and how the world actually works, driven by a desire to form a clean narrative of cause and effect despite the inherent complexities of everything involving money.

> Accepting that everything involving money is driven by illogical emotions and has more moving parts than anyone can grasp is a good start to remembering that history is the study of things happening that people didn’t think would or could happen. This is especially true with money.

* Political beliefs driving financial decisions, influenced by economics being a misbehaved cousin of politics.

> The point is not that politics don’t influence the economy. But the reason this is such a sensitive topic is because the data often surprises the heck out of people, which itself is a reason to realize that the correlation between politics and economics isn’t as clear as you’d like to think it is.

* The three-month bubble: Extrapolating the recent past into the near future, and then overestimating the extent to which whatever you anticipate will happen in the near future will impact your future.

> Most of the time, something big happening doesn’t increase the odds of it happening again. It’s the opposite, as mean reversion is a merciless law of finance. But even when something does happen again, most of the time it doesn’t – or shouldn’t – impact your actions in the way you’re tempted to think, because most extrapolations are short term while most goals are long term. A stable strategy designed to endure change is almost always superior to one that attempts to guard against whatever just happened happening again.

-----
If there’s a common denominator in these, it’s a preference for humility, adaptability, long time horizons, and skepticism of popularity around anything involving money. Which can be summed up as: Be prepared to roll with the punches.

Jiddu Krishnamurti spent years giving spiritual talks. He became more candid as he got older. In one famous talk, he asked the audience if they’d like to know his secret.

He whispered, “You see, I don’t mind what happens.”

That might be the best trick when dealing with the psychology of money.


