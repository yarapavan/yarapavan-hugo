+++
title = "Rule of 50 for dropping a bad book"
description = ""
tags = ['lifehacks']
categories = ['books']
date= 2018-07-19T17:31:42+05:30

+++

The woman on the other end of the line said, "But how many pages should I read before I can guiltlessly stop reading a book?"

On the spur of the moment, with no particular psychological or literary theory in mind to justify it, I developed my Rule of 50:

Give a book 50 pages. When you get to the bottom of Page 50, ask yourself if you're really liking the book. If you are, of course, then great, keep on reading. But if you're not, then put it down and look for another. (Always keep in mind that there's nothing to stop you from going back to it later, whether that might be in six days or six years. Or 60 years. There is many a book that I couldn't get into the first time, or even two, that I tried to read it, and then, giving it one more chance, totally fell under its spell. The book obviously hadn't changed - but I had.)

When you are 51 years of age or older, subtract your age from 100, and the resulting number (which, of course, gets smaller every year) is the number of pages you should read before you can guiltlessly give up on a book. As the saying goes, "Age has its privileges."

And the ultimate privilege of age, of course, is that when you turn 100, you are authorized (by the Rule of 50) to judge a book by its cover.


source: https://www.theglobeandmail.com/arts/books-and-media/nancy-pearls-rule-of-50-for-dropping-a-bad-book/article565170/
