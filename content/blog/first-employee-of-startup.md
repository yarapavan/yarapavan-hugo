+++
title = "First employee of Startup? You are probably getting Screwed"
description = ""
tags = ['startups']
categories = []
date= 2018-04-17T21:26:40+05:30

+++
via [patio11](https://twitter.com/patio11) top comment on the [HN post](https://news.ycombinator.com/item?id=2949323) with the article title as this post:

> Imagine three twenty-something guys working on a startup that has more lines of code than dollars in the bank. They're working out of an apartment and spend most evenings eating ramen noodles from the same MSG-laden box. They work approximately equal hours (too many). They suffer approximately equal stress (more than they ever expected). They bear approximately equal responsibility for not tanking the company through poor performance. They each accept dramatic pay-cuts relative to easier, better jobs which they could sleepwalk their way into.
> Next door, there are another three guys, eating ramen, etc etc.

> Now, it seems to me like the three guys behind Door #1 are very similar to the three guys behind Door #2. However, in one case they're all co-founders, and in one case they are two co-founders and a first employee. Those are very, very different statuses for the third guy. The third co-founder gets mentioned in press hits about the company. The third co-founder can call himself a co-founder, a status of value in an industry (and society) which is sometimes obsessed with status. The third co-founder cannot get excised from the cap table without that being mentioned as a subplot in the eventual movie.

> The first employee will not usually get mentioned. The first employee gets no social status of particular esteem. The first employee will not have a seat at the table -- literally or figuratively -- when the eventual disposition of the first employee's equity is decided. The first employee's equity stake is approximately 1/6 to 1/40th (or less!) of what the third co-founder's was. Well, theoretically. 0.5% is 1/40th of 20% in engineering math, not in investor math, because investors can change the laws of mathematics retroactively. 0.5% of millions of dollars is sometimes nothing at all. (This is one of the least obvious and most important things I have learned from HN.)

> If you're good enough to be a first employee, you're probably epsilon away from being good enough to be a third co-founder. There may be good reasons to prefer being an employee... but think darn hard before you make that decision.




