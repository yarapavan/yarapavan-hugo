+++
date = "2017-03-13T19:38:20+05:30"
title = "httpie"
categories = []
tags = []
description = "HTTPie - A useful command line HTTP client"

+++

[HTTPie](http://httpie.org) is a useful HTTP CLI client with sensible defaults, built-in JSON support via an expressive and intuitive command syntax.

On OSX, you can install using HomeBrew  - ``` brew install httpie ``` and invoked using `http`.

### Favorite oneliners

Upload a file using redirected input

``` http example.com < file.json```

Download a file 

``` http example.com/file > file```

``` http --download example.com/file```

Send API data from file

```http POST api.example.com/person/1 < person.json ```

Basic Auth

```http -a username:password example.com```

Digest Auth

```http --auth-type=digest -a username:password example.org```

With Password Prompt

```http -a username example.com```

Custom HTTP method HTTP headers and JSON data:

```http PUT example.com X-API-Token:12345 name=Pavan```

Submitting forms:

``` http -f POST example.com hello=World```
