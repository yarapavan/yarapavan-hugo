+++
title = "That Cocky Voice in Your Head is Wrong"
description = "Jason Zweig's essay on how behavioral economics affects Investors. The findings from the field of behavioral economics apply to everyone. Especially you."
tags = ['wisdom']
categories = ['wisdom']
date= 2018-08-29T20:34:45+05:30

+++

Jason Zweig is one of my favorite finance writers. His recent [WSJ column - The Intelligent Investor](https://www.wsj.com/articles/dear-investor-that-cocky-voice-in-your-head-is-wrong-1535108459) distills 20 years of studying __Behavioral Economics__ into a set of axioms.

*  Behavioral economics teaches that people are [overconfident](http://faculty.haas.berkeley.edu/odean/Papers%20current%20versions/BoysWillBeBoys.pdf?mod=article_inline): They believe they know more than they do, or they assume their knowledge is more precise than it is.


* Behavioral economists say that [confirmation bias](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.93.4839&rep=rep1&type=pdf&mod=article_inline) leads most people to seek out evidence supporting what they already believe or to ignore data that might disprove their beliefs.


* Behavioral economics says investors are [myopic](http://faculty.chicagobooth.edu/richard.thaler/research/pdf/MyopicLossAversion.pdf?mod=article_inline): Short-term losses or costs can blind them to the pursuit of longer-term rewards.


* Behavioral economists say you should inform your decisions with the [base rate](http://www.psyfitec.com/2014/06/b-is-for-base-rate-neglect.html?mod=article_inline), or the best available historical evidence of how likely an outcome is.

* Extensive research documents [unconscious biases](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2992812&mod=article_inline), or factors that shape our behavior below the level of awareness.

* Most people tend to be [unrealistically optimistic](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.535.9244&rep=rep1&type=pdf&mod=article_inline), overestimating how likely they are to have good fortune and underestimating how many bad things will happen to them.

* The [disposition effect](https://whatinvestorswant.files.wordpress.com/2010/08/statman-what-is-bf-cfa.pdf?mod=article_inline) leads investors to sell their winning stocks too soon and hold onto their money-losing positions too long.

* The [sunk-cost fallacy](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.452.2318&rep=rep1&type=pdf&mod=article_inline) leads many people to keep trying to justify a past decision even after it’s become obvious that it was a mistake.

* Research in dozens of countries around the world shows that investors almost everywhere keep most of their stock portfolios in shares of local companies instead of spreading their bets worldwide. This [“home bias”](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1025806&mod=article_inline) leaves them underexposed to the benefits of global diversification.

* Research shows that many people are prone to “[status-quo bias](https://sites.hks.harvard.edu/fs/rzeckhau/status%20quo%20bias.pdf?mod=article_inline)” or investing inertia, preferring to leave their current portfolio in place even when they might be better off switching to other choices.


* Behavioral economics shows that people are predictably bad at [estimating probabilities](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.471.5243&rep=rep1&type=pdf&mod=article_inline): They tend to overestimate the likelihood of rare events and underestimate the frequency of common events.


* Many people exhibit what’s called the [bias blind spot](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.910.3329&rep=rep1&type=pdf&mod=article_inline), or the tendency to see clearly that other people’s behavior isn’t optimal while remaining oblivious to our own shortcomings.


* Experiments in behavioral economics show that most people are prone to [anchoring](https://cpb-us-e1.wpmucdn.com/blogs.cornell.edu/dist/b/6819/files/2017/04/EpleyGilovich.2010-23r847v.pdf?mod=article_inline). People who compare prices to the last digits of their Social Security number, for instance, are willing to pay more for something if their final digits are high.


* Experiments have shown for decades that people tend to draw sweeping conclusions from [extremely small samples of data](http://www.stats.org.uk/statistical-inference/TverskyKahneman1971.pdf?mod=article_inline).


* Decades of data show that investors may [overreact](http://faculty.chicagobooth.edu/richard.thaler/research/pdf/DoesStockMarketOverreact.pdf?mod=article_inline) to relatively minor fluctuations in the stock market.



