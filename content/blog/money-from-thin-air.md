+++
title = "Notes: Money from Thin Air"
description = ""
tags = ['notes']
categories = []
date= 2018-06-22T12:31:05+05:30

+++

> “I have a principle I live by. Never go through the front door unless you’ve got a back door, and the hardest thing to get people to do is to not commit themselves to one course of action but to think about what you’re going to do next. Playing chess with my father, I give him credit for that. I learned, if you haven’t thought three moves ahead and what if he does this and what if that happens, in today’s world you can’t predict what’s going to happen. You can take chances, but you never, ever play the game without an out. Maybe that’s from being a history major, studying everybody in history who has failed to have a back door, whether it’s Hitler, Napoleon and down the list. If you take a chance, always have a back door. That’s the fun of it.”


> “I make it a practice never to read what people are saying about me, because if you read what they say and care, they’ve won. It’s necessary that you insulate yourself from what others think. The greatest ideas you will ever have are the ones that other people don’t understand. And if you’re in that position, and you care too much about what they think, you will not do the right thing. And therefore, I purposefully have long ago decided that if I live by the moral code that I want to live by, then what people think of me is not so important, because I’m doing what I believe is right and I’m not trying to hurt other people. So long as my success, such as it is, does not come at the expense of other people, then I’m happy and I don’t mind if they don’t agree with me.”

> “McCaw’s approach to making money is indirect. It’s how you create and build value. If you do that, there are lots of ways to make money. You’re not going to make money very long if you’re not creating value.”

what are the keys to Craig McCaw’s impressive and numerous business successes?

- He always retained business flexibility and had a “back door” which could be used if things didn’t quite go to plan. Time and again, Craig McCaw showed a rare willingness to change business plans on a dime – which is an asset in “the highly volatile and evolving world of telecommunications.

- Trust the people hired to run the business. McCaw delegated meaningful amounts of authority to them, and then get out of their way. In his later companies, Craig McCaw has almost managed to turn this skill into an art form, giving his managers objectives and then leaving them to figure out the best way to achieve that. He also fully supported any decisions they made, and insisted they go ahead and make decisions without getting his approval.

- McCaw always retained control in every business enterprise he became involved in. That allowed him to move the companies in the best direction without interference.

- Take risks, and don’t be afraid of building a company which will require significant amounts of capital to grow in the future. Time and again, Craig McCaw has taken risks analysts discouraged or others thought foolish. Yet, his vision of the future has proven to be unerringly accurate. That has delivered a significant competitive advantage to him.

- Maintain a low public profile. Craig McCaw tried to stay “under the radar screens” for as long as possible. That allowed him to develop projects more fully before subjecting them to public scrutinyy – which vastly enhanced the quality of his ideas.

- Embrace impossible ideas and take unconventional paths. Craig McCaw has been termed a visionary simply because he’s spotted business opportunities other people assumed were worth little. He achieves that by refusing to get too caught up in details, and looking at things from the customers perspective rather than the businessman’s perspective.

- Focus first and foremost on creating value for the customer – even if that requires huge expense. Craig McCaw always steadfastly refused to cut corners whenever anything involving the quality of the product delivered to customers was concerned.

- Go for huge payoffs in substantial markets. Craig McCaw has always focused on large multi-billion dollar markets.”

- “Keep it simple. Invariably, McCaw found simple solutions for complex problems while everyone else looked for complex solutions that were unworkable.

- Pursue excellence. Craig McCaw defines that as the one thing that gives real meaning to life.

> “What lies ahead for Craig McCaw? Expect the unexpected. He could spend years staying on course, expanding his companies and enjoying the result. But just as his face begins to appear again on the covers of magazines that celebrate his triumphs, look for a change. He hates routine. He gets restless when a venture begins to succeed and his presence is less essential. Having built and sold two national companies, McCaw could wake up one morning and decide to sell one or all of his present companies. He could pull in new partners, raise his bets and propose something even more exciting, built around something bigger than the parts of his disparate enterprises. No one can yet say what McCaw’s final legacy will be. It could be his known companies or some embryonic venture kept off the radar. Whichever, it will bear the influence of a great and wholly American entrepreneur. Putting the stamp of his unique personality on business, keeping private his secret places, resisting the powerful pull to be conventional, playing the wizard to a dumbfounded audience – Craig McCaw’s greatest achievement may simply be staying true to himself."


Excerpt From: BusinessNews Publishing. “Summary: Money from Thin Air: Review and Analysis of Corr's Book.” 
