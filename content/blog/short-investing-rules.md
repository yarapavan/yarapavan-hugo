+++
categories = []
date = "2017-04-14T19:05:34+05:30"
description = ""
tags = []
title = "Short Investing Rules from Collaborative Fund"

+++

1. Define what you’re incapable of and stay away from it.

2. You’re not proven until you’ve survived a calamity.

3. Plan on every plan not going according to plan.

4. Every product that changed the world was once belittled by the crowd.

5. The crowd is usually right.

6. Work for companies you would invest in and invest in companies you would work for.

7. Nothing’s free, so figure out the cost of investment returns – emotional, analytical, whatever – and be prepared to pay every cent of it.

8. Most great companies focus on the intersection of customer empathy and competitive paranoia.

9. Most great investors focus on the intersection of patience and contrarianism.

10. Most contrarianism is irrational cynicism.

11. Three types of businesses: Solve a customer’s problem, scratch a customer’s itch, exploit a customer’s vulnerability.

12. Solving a customer’s problem is the most lucrative and enduring, especially as access to information proliferates.

13. The biggest risks are things that aren’t in the news, as people aren’t preparing for them because they’re not in the news.

14. Reducing your desires has the same effect as leveraging your assets, but with no downside risk.

15. Spreadsheets cannot model trust and honesty, so due diligence always has to have a soft, subjective side.

16. Read fewer forecasts and more history.

17. Study more failures and fewer successes.

18. Reject existing beliefs as easily as you are persuaded by new ones.

19. No amount of intelligence can counteract the influence of extremely strong political beliefs.

20. Absorbing manageable damage is more realistic than avoiding risk.

21. Everything is ten times more complicated than it looks.

22. Solutions should usually be ten times simpler than they are.

23. The cure to overconfidence is constantly reminding yourself that you’ve experienced maybe 0.00001% of the world.

24. Highly overrated: Forecasts, goals, and multitasking.

25. Highly underrated: Options, systems, and getting along with people you disagree with.
