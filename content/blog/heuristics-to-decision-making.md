+++
title = "Heuristics to Decision Making"
description = ""
tags = ['wisdom']
categories = ['life']
date= 2018-08-06T15:07:49+05:30

+++
Source: https://twitter.com/farnamstreet/status/1026105498372845571

Some heuristics to make decisions. Please add your own and challenge these.

1. Schedule time to think.

2. When considering options, watch your heart rate.

3. If all things are equal take the road less travelled (i.e., hard decisions easy life, easy decisions, hard life.)

4. No decision is sometimes the best path. If you can’t get comfortable deciding, the answer is to explore the tension not force a decision.
5. Instead of fight or flight, gather info. Think about what you can do to get the information you need to make a better decision.

6. Saying no is more important than saying yes.

7. Use time to filter people and ideas. The majority of the time you don’t need to be an early adopter.

8. Look for win-win decisions. If someone absolutely has to lose, you’re likely not thinking hard enough or you need to make structural/environmental changes.

9. When stuck work back to first principles and build up.

10. The rule of 5. Think about what the decision looks like 5 days, 5 weeks, 5 months, 5 years, 5 decades.

11. Let other people’s hindsight become your foresight.

12. Avoid things the best version of yourself will regret.

13. Ask what information would cause you to change your mind. If you don’t have that information, find it. If you do, track is religiously.

14. Focus on collecting feedback to calibrate your ability to make this decision.

15. If you’re outside your circle of competence and still have to make a decision, ask experts HOW they would make the same decision not WHAT they would decide.

16. Lean into (not away from) what’s making you uncomfortable.

17. Put things on a reversibility/consequence grid —irreversible and high consequence decisions likely require more time. The rest of the time you can usually go fast.

18. Identify the 2-3 variables that really matter and break them down to unearth assumptions and get the team on the same page.

19. Fast decisions should never be rushed.

20. Too much information increases confidence not accuracy.

21. Avoiding stupidity is easier than seeking brilliance.

22. Walk around the decision from the perspective of everyone implicated (shareholders, employees, regulators, customers, partners, etc.)

23. Some warnings signs that increase the likelihood of stupidity are (environment, the pace of change, rushing, physically tired, hyper-focus, authority, consensus-seeking behavior).

24. Own the decision. (If you make decisions by comittiee have one person sign their name to the decision - someone needs to own it.)

25. Decisions are nothing without execution.

26. Ask yourself “and then what?”
