+++
title = "Notes from Jim O'Shaughnessy Tweetstorm"
description = ""
tags = ['wisdom']
categories = []
date= 2018-05-17T11:36:18+05:30

+++

Jim O'Shaughnessy is Chairman, Chief Investment Officer, and Portfolio Manager at O’Shaughnessy Asset Management.

A professional investor for over three decades, he recently shared his wisdom on Twitter over a thread of 26 tweets. In this dispassionate and non-judgmental delivery, he imparts some amazing insights and wisdom for investors. They have all been reproduced below.

I don’t know how the market will perform this year. Or the next year. I don’t know if stocks will be higher or lower in 5 years. Indeed, even though the probabilities favour a positive outcome, I don’t know if stocks will be higher in 10 years.

On the other hand...

I DO know that, according to Forbes, “since 1945…there have been 77 market drops between 5% and 10%, and 27 corrections between 10% and 20%”.
I know that market corrections are a feature, not a bug, required to get good long-term performance.
I know that during these corrections, there will be a host of “experts” on business television, blogs, magazines, podcasts and radio warning investors that THIS is the big one. That stocks are heading dramatically lower, and that they should get out now, while they still can.
I know that given the way we are constructed, many investors will react emotionally and heed these warnings and sell their holdings, saying they will “wait until the smoke clears” before they return to the market.
I know that over time, most of these investors will not return to the market until well after the bottom, usually when stocks have already dramatically increased in value.
I think I know that, at least for U.S. investors, no matter how much stocks drop by, they will always come back and make new highs. That’s been the story in America since the late 1700s.
I think I know that this cycle will repeat itself, with variations, for the rest of my life, and probably for my children’s and grandchildren’s lives as well.
Massive amounts of data have documented that while the world is very chaotic, the way humans respond to things is fairly predictable. I don’t know if some incredible jump in evolution or intervention based upon new discoveries will change human nature but would gladly make a long-term bet that such a thing will not happen.
I don’t know what exciting new industries and companies will capture investor’s attention over the next 20 years.

But…..

I think I know that investors will get very excited by them and price them to perfection.
I do know that perfection is a very high hurdle that most of these innovative companies will be unable to achieve.
I think I know that they will suffer the same fate as the most exciting and innovative companies of the past and that most will crash and burn. I infer this because “about 3,000 automobile companies have existed in the United States” and that of the remaining 3, one was bailed out, one was bought out and only one is still chugging along on its own.
I know that, as a professional investor, if my goal is to do better than the market, my investment portfolio must look very different than the market. I know that, in the short-term, the odds are against me but I think I know that in the long-term, they are in my favour.
I do know that by staking my claim on portfolios that are very different than the market, I have, and will continue to have, far higher career risk than other professionals, especially those with a low tracking error target.
I know that I cannot tell you which individual stocks I’m buying today will be responsible for my portfolio’s overall performance. I also know that trying to guess which ones will be the best performers almost always results in guessing the wrong way.
I know that as a systematic, rules-based quantitative investor, I can negate my entire track record by just once emotionally overriding my investment models, as many sadly did during the financial crisis.
I think I know that no matter how many times you “prove” that we are saddled with a host of behavioral biases that make successful long-term investing an odds-against bet, may people will say they understand but continue to exhibit the biases.
I think the reason for the persistence of these “cognitive mirages” is that up to 45% of our investment choices are determined by genetics and cannot be educated against. Read: Why do individuals exhibit investment biases?
I think I know that if I didn’t adhere to an entirely quantitative investment mythology, I would be as likely—maybe MORE likely—to giving into all these behavioral biases.
I don’t know exactly how much of my success is due to luck and how much is due to skill.

But I do know that luck definitely played, and will continue to play, a fairly substantial role.

I don’t know how the majority of investors who are indexing their portfolios will react to a bear market.

I think I know that they will react badly and sell out of their indexed portfolio near a market bottom.
I think I know that the majority of active stock market investors—both professional and aficionado—will secretly believe that while these human foibles that make investing hard apply to others, they don’t apply to them.
I know they apply to me and to everyone who works for me.

Finally, while I think I know that everything I’ve just said is correct, the fact is I can’t know that with certainty and that if history has taught us anything, it’s that the majority of things we currently believe are wrong.
