+++
categories = []
date = "2017-05-19T20:33:22+05:30"
description = "The Engineer/Manager Pendulum"
tags = []
title = "The Engineer/Manager Pendulum"

+++
Source: https://charity.wtf/2017/05/11/the-engineer-manager-pendulum/

The best frontline eng managers in the world are the ones that are never more than 2-3 years removed from hands-on work, full time down in the trenches. The best individual contributors are the ones who have done time in management.

And the best technical leaders in the world are often the ones who do both. Back and forth. Like a pendulum.

Being a manager teaches you how the business works. It also teaches you how people work. You will learn to have uncomfortable conversations. You will learn how to still get good work out of people who are irritated, or resentful, or who hate your guts. You will learn how to resolve conflicts, dear god will you ever learn to resolve conflicts. (Actually you’ll learn to YEARN for conflicts because straightforward conflict is usually better than all the other options.) You’ll go home exhausted every day and unable to articulate anything you actually did. But you did stuff.

You’ll miss the dopamine hit of fixing something or solving something. You’ll miss it desperately.

One last thing about management. There’s a myth that makes it really hard for people to stop managing, even when it makes them and everyone around them miserable. And that’s the idea that management is a promotion.

Management is not a promotion, management is a change of profession. And you will be bad at it for a long time after you start doing it. If you don’t think you’re bad at it, you aren’t doing your job.


