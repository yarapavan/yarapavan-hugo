+++
title = "Notes - May 15, 2018"
description = ""
tags = ['links']
categories = []
date= 2018-05-15T18:18:46+05:30

+++

* [CloudEvents Spec](https://cloudevents.io/).  Events are everywhere. However, event publishers tend to describe events differently.  The lack of a common way of describing events means developers must constantly re-learn how to receive events. This also limits the potential for libraries, tooling and infrastructure to aide the delivery of event data across environments, like SDKs, event routers or tracing systems. The portability and productivity we can achieve from event data is hindered overall.  Enter CloudEvents, a specification for describing event data in a common way. CloudEvents seeks to ease event declaration and delivery across services, platforms and beyond.

* [💻 Wonderful world of OSX apps](https://github.com/nikitavoloboev/my-mac-os/) List of applications, alfred workflows and various tools that make my macOS experience even more amazing

* [Scaling Event Sourcing at Jet](https://medium.com/@eulerfx/scaling-event-sourcing-at-jet-9c873cac33b8). Event sourcing is a paradigm where changes to application state are recorded as a series of events.

* [nuage with support for org-mode](https://github.com/mickael-kerjean/nuage). A Dropbox-like interface for your existing data (with support for many backends). Need to explore more.
