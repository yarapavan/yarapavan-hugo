+++
title = "The Thrill of Uncertainity"
description = "From Morgan Housel's blog post - the thrill of uncertainity"
tags = []
categories = []
date= 2018-01-12T16:55:53+05:30

+++

From Morgan Housel's [blog post](http://www.collaborativefund.com/blog/the-thrill-of-uncertainty/):

> You are probably more interested in the hunting process of investing than you need to be. This varies by how you invest, but it’s true at all levels. There’s too much evidence it all styles of investing that, in aggregate, we put in more effort and intelligence than is necessary. Why we do this make sense when you understand Skinner’s research on variable interval rewards. The buzz of this game is strong enough to keep us searching for the next reward, often obsessively, sometimes to our own detriment. Some rats in these experiments obsessed themselves into exhaustion, frantically waiting for food pellets nonstop for days on end. They wanted food to keep them alive, but they nearly killed themselves trying to get it. Everyone knows an investor like that. A combination of letting time do its work, and humility in how much you can and can’t control, is vital in almost all investing styles.


> This is not Excel and charts. **It’s dopamine and cortisol. So much of investing is not what you know, but how you behave. And behavior is hard to teach, hard to control, and hard to even come to terms with.** There’s uncomfortable truth here: My best guess is that 10% of people are born natural investors, and 10% of people can’t ever be taught how to invest successfully no matter their education. The other 80% of us could improve by spending more energy on how we respond to risk and reward vs. the active chase of searching. **Barry Ritholtz was once asked the greatest financial lesson he’s ever learned. “You’re a monkey. It all comes down to that. You are a slightly clever, pants-wearing primate.”**

What is the greatest financial lesson you’ve ever learned?

> You’re a monkey. It all comes down to that. You are a slightly clever, pants-wearing primate. If you forget that you’re nothing more than a monkey who has been fashioned by eons on the plains, being chased by tigers, you shouldn’t invest. You have to be aware of how your own psychology effects what you do. This is why we as investors sell at the bottom, get panicked. All the other lessons I’ve learned have come out of that. As has the field of behavioral economics.

> Wall Street clichés, like “cut your losses and let your winners run” come back to prevent the monkey part of your brain from doing what it does. There’s a banana–I want it. That’s how chimps behave. Us humans react to greed and fear in predictable ways. We are predictably irrational. If you understand that you can take steps to prevent that–we don’t own anything in the office that doesn’t have a stop-loss on it. In 2008, we watched the market go down 40%. We figured out we’re chimps, and don’t let the chimp inside us make those chimp-like decisions.

> Every good financial decision I’ve made comes from, “Wait a second, monkey boy, step back, don’t do that.” Once you realize how your own brain chemistry works against you, it gives you a chance to not panic at the bottom.
