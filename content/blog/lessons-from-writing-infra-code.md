+++
title = "Lessons Learned From Writing Over 300,000 Lines of Infrastructure Code "
description = "From Gruntwork folks - A concise masterclass on how to write infrastructure code"
tags = ['infrastructure']
categories = []
date= 2018-12-19T21:49:09+05:30

+++

Original post up at [Gruntwork blog](https://blog.gruntwork.io/5-lessons-learned-from-writing-over-300-000-lines-of-infrastructure-code-36ba7fadeac1)

1. Go through the [Production-Grade Infrastructure Checklist](https://cdn-images-1.medium.com/max/2000/1*-nYI19LZZKDQjdAx0qhlFw.png) to make sure you’re building the right thing.
2. Define your infrastructure as code using tools such as Terraform, Packer, and Docker. Make sure your team has the time to master these tools (see DevOps Resources).
3. Build your code out of small, standalone, composable modules (or use the off-the-shelf modules in the Infrastructure as Code Library).
4. Write automated tests for your modules using Terratest.
5. Submit a pull request to get your code reviewed.
6. Release a new version of your code.
7. Promote that new version of your code from environment to environment.

