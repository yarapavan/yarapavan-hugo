+++
title = "The Three Important Words - I Don't Know"
description = "according to Howard Marks"
tags = ['notes']
categories = ['notes']
date= 2018-12-28T17:05:52+05:30

+++

"There are three words which are among the most important words in our business: I don't know. And if you don't know something...you should admit it—to yourself and to everybody around you. And I think personally that it's very freeing to say 'I don't know.' I think it's very, almost depressing, to feel that you have to have an opinion on every subject, even the ones where by definition you can't have superior knowledge."

--Howard Marks [Hong Kong, October 26, 2018](https://youtu.be/ARQOG8MnyO4?t=890)
