+++
title = "Life is Short"
description = "Notes on Life is Short Philosophy"
tags = ['life']
categories = ['advice']
date= 2018-04-02T21:49:30+05:30

+++

Three articles influenced current blog post. Each of us have an opinion and view point on the concept of life - its meaning, modus operandi and the beginnning, mid and end results. When you cross 30 years of age, you start beginning to realize that life is indeed short, sweet and often difficult. You start and raise your own family, keep up with your peers at work and past life, beginning to see futility of things that you fancied so much in your teens and 20s among other things. 

Suddenly you are closer to the 2nd half of your life than than ever. You hair starts turning gray and you are less confident of things around you - career,friends, family and society at general. And, you get to think deeply about people and things that you experience at times. You no longer tend to take things for granted - health, family, friends, career, relations, hobbies.

In this context, chanced up on these three articles today:

## Life is Short 

Source: [PG's popular essay on Life](http://www.paulgraham.com/vb.html)

 > Relentlessly prune bullshit, don't wait to do things that matter, and savor the time you have. That's what you do when life is short.
 
 > If you ask yourself what you spend your time on that's bullshit, you probably already know the answer. __Unnecessary meetings, pointless disputes, bureaucracy, posturing, dealing with other people's mistakes, traffic jams, addictive but unrewarding pastimes__.

 > As I've written before, one byproduct of technical progress is that things we like tend to become more addictive. Which means we will increasingly have to make a conscious effort to avoid addictions—to stand outside ourselves and ask "is this how I want to be spending my time?" 
 
 > One heuristic for distinguishing stuff that matters is to ask yourself whether you'll care about it in the future. Fake stuff that matters usually has a sharp peak of seeming to matter. That's how it tricks you. The area under the curve is small, but its shape jabs into your consciousness like a pin. The things that matter aren't necessarily the ones people would call "important." Having coffee with a friend matters. You won't feel later like that was a waste of time.

 > If life is short, we should expect its shortness to take us by surprise. And that is just what tends to happen. You take things for granted, and then they're gone. You think you can always write that book, or climb that mountain, or whatever, and then you realize the window has closed. The saddest windows close when other people die. Their lives are short too.

 > The usual way to avoid being taken by surprise by something is to be consciously aware of it. Cultivate a habit of impatience about the things you most want to do. Don't wait before climbing that mountain or writing that book or visiting your mother. You don't need to be constantly reminding yourself why you shouldn't wait. Just don't wait.

 > I can think of two more things one does when one doesn't have much of something: try to get more of it, and savor what one has. Both make sense here.  How you live affects how long you live. Most people could do better. Me among them.

## The Tail End 

Source: [Tim Urban's Essay - The Tail End ](https://waitbutwhy.com/2015/12/the-tail-end.html)

<p align="center">
  <img width="460" height="300" src="https://28oa9i1t08037ue3m1l0i861-wpengine.netdna-ssl.com/wp-content/uploads/2015/12/Days.jpg">
  </p>

 Instead of measuring your life in units of time, you can measure it in activities or events. If I'm 35, then assuming that the average Indian life is about 70 years old, then I've about another 30-35 years left to live - which means:
 
 * in sports parlance, about 9 cricket world cups, 9 football world cups, 9 olympics left

 * if I travel a week every year, then I'd have been to 35 different places in rest of the life time. 

 * if I spend 5 days a week working for another 10 years, then I have about 2500 working days left. 

 * if I spend 2 days a month for another 10 years with parents, then I have about 240 days to spend time with them

 	> When you look at that reality, you realize that despite not being at the end of your life, you may very well be nearing the end of your time with some of the most important people in your life.
 
 The three takeaways are:

 1. Living in the same place as the people you love matters. 

 2. Priorities matter. Your remaining face time with any person depends largely on where that person falls on your list of life priorities. Make sure this list is set by you—not by unconscious inertia.

 3. Quality time matters. If you’re in your last 10% of time with someone you love, keep that fact in the front of your mind when you’re with them and treat that time as what it actually is: precious.


## The days are long but the decades are short 

Source: [Sam Altman's reflections on turning 30](https://blog.samaltman.com/the-days-are-long-but-the-decades-are-short)

About 36 life nuggets from Sama covering life, family, friends, career, health, money, hiring and more...

  1. Never put your family, friends, or significant other low on your priority list.  Prefer a handful of truly close friends to a hundred acquaintances.  Don’t lose touch with old friends. 

  2. Life is not a dress rehearsal—this is probably it.  Make it count.  Time is extremely limited and goes by fast.  Do what makes you happy and fulfilled. Don’t spend time trying to maintain relationships with people you don’t like, and cut negative people out of your life.  

  3. How to succeed: pick the right thing to do (this is critical and usually ignored), focus, believe in yourself (especially when others tell you it’s not going to work), develop personal connections with people that will help you, learn to identify talented people, and work hard.  It’s hard to identify what to work on because original thought is hard.

  4. On work: it’s difficult to do a great job on work you don’t care about.  And it’s hard to be totally happy/fulfilled in life if you don’t like what you do for your work.  Work very hard—a surprising number of people will be offended that you choose to work hard—but not so hard that the rest of your life passes you by.  Aim to be the best in the world at whatever you do professionally.  Even if you miss, you’ll probably end up in a pretty good place.  Figure out your own productivity system—don’t waste time being unorganized, working at suboptimal times, etc.  Don’t be afraid to take some career risks, especially early on.  Most people pick their career fairly randomly—really think hard about what you like, what fields are going to be successful, and try to talk to people in those fields.

  5. On money: Whether or not money can buy happiness, it can buy freedom, and that’s a big deal.  Also, lack of money is very stressful.  In almost all ways, having enough money so that you don’t stress about paying rent does more to change your wellbeing than having enough money to buy your own jet. 

  6. Talk to people more.  Read more long content and less tweets.  Watch less TV.  Spend less time on the Internet.

  7. Don’t waste time.  Most people waste most of their time, especially in business.

  8. Have clear goals for yourself every day, every year, and every decade. 

  9. However, as valuable as planning is, if a great opportunity comes along you should take it.  Don’t be afraid to do something slightly reckless.  One of the benefits of working hard is that good opportunities will come along, but it’s still up to you to jump on them when they do.

  10. Go out of your way to be around smart, interesting, ambitious people.  Work for them and hire them (in fact, one of the most satisfying parts of work is forging deep relationships with really good people).  Try to spend time with people who are either among the best in the world at what they do or extremely promising but totally unknown.  It really is true that you become an average of the people you spend the most time with

  11. Minimize your own cognitive load from distracting things that don’t really matter.  It’s hard to overstate how important this is, and how bad most people are at it.  Get rid of distractions in your life.  Develop very strong ways to avoid letting crap you don’t like doing pile up and take your mental cycles, especially in your work life.

  12.  Keep your personal burn rate low.  This alone will give you a lot of opportunities in life.

  13.  If you think you’re going to regret not doing something, you should probably do it.  Regret is the worst, and most people regret far more things they didn’t do than things they did do. 

  14. Exercise.  Eat well.  Sleep.  Get out into nature with some regularity.

  15.  Go out of your way to help people.  Few things in life are as satisfying.  Be nice to strangers.  Be nice even when it doesn’t matter.

  16.  Tell your parents you love them more often.  Go home and visit as often as you can.

  17. This too shall pass.

  18.  Learn voraciously. 

  19. Do new things often.  This seems to be really important.  Not only does doing new things seem to slow down the perception of time, increase happiness, and keep life interesting, but it seems to prevent people from calcifying in the ways that they think.  Aim to do something big, new, and risky every year in your personal and professional life.

  20. Don’t screw people and don’t burn bridges.  Pick your battles carefully.

  21. Forgive people. 

  22. Don’t chase status.  Status without substance doesn’t work for long and is unfulfilling.

  23. Be grateful and keep problems in perspective.  Don’t complain too much.  Don’t hate other people’s success (but remember that some people will hate your success, and you have to learn to ignore it). 

  24. Be a doer, not a talker.

  25.  Given enough time, it is possible to adjust to almost anything, good or bad.  Humans are remarkable at this.

  26. Think for a few seconds before you act.  Think for a few minutes if you’re angry.

  27. Don’t judge other people too quickly.  You never know their whole story and why they did or didn’t do something.  Be empathetic.

  28. The days are long but the decades are short.


