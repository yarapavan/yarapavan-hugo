---
date: 2017-03-08
linktitle: Creating a new hugo website
title: Creating yarapavan.in 
highlight: "true"
---

## Relaunching website after a long hiatus
[Hugo](https://gohugo.io) provided an opportunity to relaunch my long-forgotten website! Been away from active [blogging](https://yarapavan.blogspot.com) since my Cognizant days and been occasionally writing on [LinkedIn](http://www.linkedin.com/in/yarapavan). [Twitter](http://twitter.com/yarapavan) hasbecome my goto tool for information consumption and dissemintation these days, in tune with the lowering attention span phenomena we're used to :)


## Hugo 

Being an avid [hacker news](http://news.ycombinator.com) follower, Hugo was easily one of the popular choices for static website generators out there. The fact that it is written in ``Go`` without requiring any dependencies or expensive runtimes sealed the deal for me.

### Hosting

The obvious choices are [Firebase Hosting](https://firebase.google.com/docs/hosting/), [Amazon S3](http://aws.amazon.com/s3/), [GitHub Pages](https://pages.github.com/) and [Netlify](https://www.netlify.com/).

Will be trying Firebase Hosting and GitHub Pages for hosting this new website for a week.

