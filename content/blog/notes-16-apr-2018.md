+++
title = "Notes - 16th April 2018"
description = ""
tags = ['links']
categories = []
date= 2018-04-16T12:35:09+05:30

+++

* [🚀 Awesome list of open source applications for macOS](https://github.com/serhii-londar/open-source-mac-os-apps). List of awesome open source applications for macOS. This list contains a lot of native and cross-platform apps. The main goal of this repository is to find open source and free apps and start contributing.

* [Hackerdaily - Daily Podcast for HackerNews](https://hackerdaily.io). Try at https://hackerdaily.simplecast.fm, if this is to your taste.

* [AskHN: What is your favorite tech talk?](https://news.ycombinator.com/item?id=16838460). Found a lot of useful resources from this link. Listened to Martin Kleppmann's StrangeLoop'14 talk - [Turning the Database Inside out](https://youtu.be/fU9hR3kiOK0) as a first step in this direction. Looking forward to share my notes from these talks as part of my blog series.

