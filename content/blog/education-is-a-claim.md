+++
title = "Education is something that you claim"
description = "an education is not something that you get but something that you claim. "
tags = ['notes']
categories = ['notes']
date= 2019-02-06T19:04:03+05:30

+++
via Maria Popova's [conversation](https://onbeing.org/programs/maria-popova-cartographer-of-meaning-in-a-digital-age-jan2017/) with Krista Tippett:

 As a culture — you’re right. We seem somehow bored with thinking. We want to instantly know. And there’s this epidemic of listicles. Why think about what constitutes a great work of art when you can skim the “20 Most Expensive Paintings in History?” And I’m very guided by this desire to counter that in myself because I am, like everybody else, a product of my time and my culture. And I remember, there’s a really beautiful commencement address that Adrienne Rich gave in 1977 in which she said that __an education is not something that you get but something that you claim__.

 And I think that’s very much true of knowledge itself. The reason we’re so increasingly intolerant of long articles and why we skim them, why we skip forward even in a short video that reduces a 300-page book into a three-minute animation — even in that we skip forward — is that we’ve been infected with this kind of pathological impatience that __makes us want to have the knowledge but not do the work of claiming it__. I mean, __the true material of knowledge is meaning__. And the meaningful is the opposite of the trivial. And the only thing that we should have gleaned by skimming and skipping forward is really trivia. __And the only way to glean knowledge is contemplation. And the road to that is time. There’s nothing else. It’s just time. There is no shortcut for the conquest of meaning. And ultimately, it is meaning that we seek to give to our lives__.


