+++
title = "Notes - June 22, 2018"
description = ""
tags = ['links']
categories = []
date= 2018-06-22T14:55:03+05:30

+++

> “I think the tech world has this problem where we go out and say ‘We think we’re really smart and the rest of the world is really dumb, and so why don’t we build apps to make their lives more like ours.’ What you end up doing is building products with judgement at their core. But when you find founders that are building products for themselves and their friends you find products with empathy at their core. That empathy translates to features and functions that understand the seemly non-pragmatic nuances of a given industry.”  - Ali Hamed

