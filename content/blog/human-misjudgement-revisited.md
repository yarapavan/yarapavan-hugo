+++
title = "Human Misjudgement Revisited"
description = "Charlie Munger's human misjudgement talk revisited"
tags = []
categories = []
date= 2017-06-28T19:50:22+05:30

+++

Phil Ordway gave a talk, revisiting Charlier Munger's famous talk on "The Psychology of Human Misjudgement", at John Mihaljevic's Zurich conference in June 2017.

To quote him:

> Here is a copy of a talk I gave at John Mihaljevic's excellent conference in Zurich last month. The goal was to "update" Charlie Munger's famous talk "The Psychology of Human Misjudment" to include more recent examples and the work of Kahneman and Tversky. The best part is probably the contribution of Jason Zweig -- collaborator with Kahneman on his book and leading expert on all things behavioral and investing -- who was kind enough to share his thoughts. A big thank you to him for that and to John for hosting an excellent event.

Read the document here: https://www.dropbox.com/s/209xq1dk50gb5ju/Human%20Misjudgment%20Revisited%20--%20Philip%20C.%20Ordway.pdf?dl=0
