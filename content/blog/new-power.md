+++
date = "2017-03-24T14:23:30+05:30"
categories = []
tags = []
description = ""
title = "Power to ignore"

+++

>"The world is changing faster than ever before, and we are flooded by impossible amounts of data, of ideas, of promises and of threats. Humans relinquish authority to the free market, to crowd wisdom and to external algorithms partly because they cannot deal with the deluge of data. In the past, censorship worked by blocking the flow of information. In the twenty-first century, censorship works by flooding people with irrelevant information. People just don’t know what to pay attention to, and they often spend their time investigating and debating side issues. In ancient times having power meant having access to data. Today having power means knowing what to ignore." -Yuval Noah Harari (Homo Deus)

