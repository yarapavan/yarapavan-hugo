+++
title = "awesome-selfhosted: A list of selfhosted apps"
description = "list of self hosted apps - mostly free and/or open source"
tags = ['opensource']
categories = ['lists']
date= 2019-01-29T19:32:52+05:30


+++
Selfhosting is the process of locally hosting and managing applications instead of renting from SaaS providers. There is an [active community in reddit](https://reddit.com/r/selfhosted) around selfhosting. Another community listing, [awesome-selfhosted](https://github.com/Kickball/awesome-selfhosted), also maintains a list of Free Software network services and web applications which can be hosted locally.

