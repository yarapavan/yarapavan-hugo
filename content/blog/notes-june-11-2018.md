+++
title = "Notes - June 11, 2018"
description = ""
tags = ['links']
categories = []
date= 2018-06-11T12:07:35+05:30

+++

* [Awesome design patterns](https://github.com/DovAmir/awesome-design-patterns). A software design pattern is a general, reusable solution to a commonly occurring problem within a given context in software design. It is a description or template for how to solve a problem that can be used in many different situations.
* [Data Models in 50 categories](http://www.databaseanswers.org/data_models/), This page shows a list of our Industry-specific Data Models in 50 categories that cover Subject Areas and are used to create Enterprise Data Models. There is a [Short downloadable Tutorial](http://www.databaseanswers.org/downloads/Short_Tutorial_on_DWH_by_Example.pdf) on creating a Data Warehouse using any of the Models on this page.
* [Project Based Learning](https://github.com/tuvtran/project-based-learning). A list of programming tutorials in which learners build an application from scratch. These tutorials are divided into different primary programming languages. Some have intermix technologies and languages.


