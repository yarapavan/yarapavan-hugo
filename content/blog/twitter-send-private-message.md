+++
title = "Sending a Private Message in Twitter"
description = ""
tags = ['hacks']
categories = []
date= 2018-06-28T15:47:32+05:30

+++

Freelancing Pro Tip: add a pinned “Send a private message” button to your 🐤Twitter profile

👉 find your Twitter user ID at tweeterid .com
👉 post a tweet with https://twitter .com/messages/compose?recipient_id=user_id
👉 remove space before .com and replace user_id
👉 pin it
