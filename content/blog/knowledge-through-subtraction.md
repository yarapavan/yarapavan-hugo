+++
categories = []
date = "2017-04-14T20:15:34+05:30"
description = ""
tags = []
title = "Knowledge grows by subtraction "

+++

In life, understanding what to avoid is more important than constantly searching for positive advice to do something new. This is expressed well in the following brief excerpt from Taleb's Antifragile book:

> “So the central tenet of the epistemology I advocate is as follows: we know a lot more what is wrong than what is right, or, phrased according to the fragile/robust classification, negative knowledge (what is wrong, what does not work) is more robust to error than positive knowledge (what is right, what works).  So knowledge grows by subtraction much more than by addition — given that what we know today might turn out to be wrong but what we know to be wrong cannot turn out to be right, at least not easily.  If I spot a black swan (not capitalized), I can be quite certain that the statement “all swans are white” is wrong.  But even if I have never seen a black swan, I can never hold such a statement to be true.”




