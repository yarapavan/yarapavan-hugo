+++
title = "Using Custom Domains with AWS API Gateway"
description = ""
tags = ['aws']
categories = []
date= 2018-06-13T14:43:45+05:30

+++

From a Stackoverflow post:

1. Generate an origin certificate from the crypto tab of the Cloudflare dashboard.
2. Import the certificate to AWS Certificate manager in the us-east-1 region, even if your API is located in a different region. If you are prompted for the certificate chain you can copy it from here.
3. Add your custom domain in the API Gateway console and select the certificate you just added. Check the AWS support article for more information on how to do this.
It usually takes about 45 minutes for the custom domain to finish initializing. Once it's done it will give you a new Cloudfront URL. Go ahead and make sure your API still works through this new URL.
4. Go to the Cloudflare DNS tab and setup a CNAME record pointing to Cloudfront URL you just created.
5. Switch to the crypto tab and set your SSL mode to "Full (Strict)". If you skip this step you'll get a redirect loop.
