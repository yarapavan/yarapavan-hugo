+++
title = "Notes - 31st March 2018"
description = ""
tags = ['things-learnt']
categories = ['links']
date= 2018-03-31T08:47:27+05:30

+++
#### Links
* [This is how Cambridge Analytica's Facebook targeting model really worked](http://www.niemanlab.org/2018/03/this-is-how-cambridge-analyticas-facebook-targeting-model-really-worked-according-to-the-person-who-built-it/). The whole point of a dimension reduction model is to mathematically represent the data in simpler form. It’s as if Cambridge Analytica took a very high-resolution photograph, resized it to be smaller, and then deleted the original. The photo still exists — and as long as Cambridge Analytica’s models exist, the data effectively does too.

* [Lessons for building AI-Driven Products](https://blog.insightdatascience.com/moving-towards-managing-ai-products-5268c5e9ecf2). 1. Track how the market is using AI technology 2.Follow the trends in deep learning research 3. Cut through the AI hype — focus on practical use cases 4. Be obsessed with customer-centric data 5. Build a usable product with a simple model before exploring complicated AI models 6. Iteratively build use cases where AI directly impacts a metric 7. Build Breadth-first (Data/Pipeline/Model) instead of Depth-first (AI model) 8. Ensure your product fails gracefully 9. Insist on AI model explainability 10. Establish clear communication with your teams — know the fundamentals and language of data and ML. ![AI-Driven Product Management](https://cdn-images-1.medium.com/max/1600/1*FCoHl9-TeFV5ZJj8xEhtzQ.png)

 The top HN comment, on this topic, summarizes as follows: As someone who is a product manager in the commercialized AI SaaS space, the most important pieces of feedback I would give a new PM here:

 1. Don't let your -brilliant- colleagues try to force their -brilliantly complex- solution of a problem - clearly define market problems, and don't let the team try to go the route of trying to force fit a solution to a market problem. Market problems come first.

 2. Frame the market problems appropriately for your ML/AI teams, and practice trying to frame the problem from a variety of angles. Framing from different angles promotes the 'Ah-ha' moment in terms of the right way to solve the problem from the ML side.

 3. Don't commit serious time to a model before having a naive solution to benchmark against. Always have a naive solution to compare against the AI solution. 'Naive' here may be a simple linear regression, RMSE, or multi armed bandit/Thompson sampling.


