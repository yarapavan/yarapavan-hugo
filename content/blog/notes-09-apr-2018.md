+++
title = "Notes - April 9, 2018"
description = ""
tags = ['things-learnt']
categories = ['links']
date= 2018-04-09T21:10:28+05:30

+++

* For Einstein, the five ascending levels of intellect were, ‘Smart, Intelligent, Brilliant, Genius, Simple’
> Simplicity is a very powerful construct. Henry Thoreau recognized this when he said, “Our life is frittered away by detail . . . simplify, simplify.” Einstein also recognized the power of simplicity, and it was the key to his breakthroughs in physics. He noted that the five ascending levels of intellect were, “Smart, Intelligent, Brilliant, Genius, Simple.” For Einstein, simplicity was simply the highest level of intellect.”


* [Why you stink at fact-checking](https://theconversation.com/why-you-stink-at-fact-checking-93997). The Moses Illusion demonstrates what psychologists call knowledge neglect – people have relevant knowledge, but they fail to use it.
*
o

> Daniel Kahneman told his financial advisor he didn’t want to get rich. He just wanted to continue living like he had.

> “She told me, ‘I can’t work with you,’” Kahneman wrote.

> I asked Kahneman about the incident a few years ago. “She was very puzzled in the context of somebody coming to get financial advice and not trying to get richer,” he said. “I’m not sure that I’m all that unusual. Many people retired on pensions and are perfectly satisfied with it and they are not desperate to have more.”

* [How Hedge Funds Get Rich](https://ofdollarsanddata.com/how-hedge-funds-get-rich-hint-its-not-their-returns-4630db9a9f6e). Nick, shows with his R program, that no matter what initial capital you give the hedge fund to start with, the hedge fund will become richer than you since its real talent is transferring your wealth into its coffers. And, one of the better ways to get rich is to pay lower fees.
> The moral of the story is that fees matter, a lot. When it comes to investing, a little more in fees can go a long way (for those charging them). So though 2% (or 1%) might sound small now, within a short time frame it can add up very quickly. I recommend keeping your average fees below 0.1% across your portfolio. Thank you for reading!
