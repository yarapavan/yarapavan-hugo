+++
title = "Notes from Naval - Part 1"
description = ""
tags = ['naval']
categories = ['notes']
date= 2018-06-26T13:24:53+05:30

+++

Notes from Naval's [Periscope session on June 6, 2018](https://twitter.com/naval/status/1004583433929048065):

### INTRO

Check out Naval’s [latest tweet storm](https://twitter.com/naval/status/1002103360646823936) on how to create wealth.

* Each tweet is written, so it can stand alone – this was deliberate, it makes the entire tweet storm randomly accessible 
* This Periscope will be discussing that
* “Everyone pretends like they don’t want to make money, but in reality, everyone does want to make money”
* Money buys you freedom in the material world
* It won’t make you happy, but it will solve a lot of your external problems
* There are many people who are attached to not being happy/making money, so sometimes it’s difficult to talk about these topics
* These people don’t want to feel bad about their choices, so they tend to shut down these conversations
* “Everyone in the world can be happy, and everyone in the word can be wealthy”
* __It’s just a matter of education, know-how, and effort__

### OWNERSHIP VS. WAGE WORK

* If you’re getting paid for renting out your time, you can make good money, but you won’t make the kind of money that can really give you freedom – the passive income that can make you money while you’re on vacation
* __You want to own equity – “If you don’t own equity in a business, your odds of making money are slim” – Ownership is important__

### PRODUCTIZE YOURSELF
* If you can only remember two words from the tweet storm – __PRODUCTIZE YOURSELF__
* How you package yourself in a product
> “Create a product out of whatever it is that you naturally and uniquely do really well”
* If you compete at being someone else, you won’t be the best in the world at it, and in turn you won’t get rewarded properly for it

#### COMPOUND INTEREST
* Compound interest – keeps adding on itself, so that you end up with thousands of times your original investment
* Compound business relationships – people trust you when you have shown yourself in a visible, and accountable way, to be a high integrity person. This can apply to your reputation, as it’s compounded over time
* If you’ve worked with the same person for a long enough time, the good relationship is compounded to a place where trust tends to dominate
> “In the intellectual domain, compound interest rules”
* __“When you find the right thing to do, and people to work with, invest deeply into that, and stick with it for decades” – THAT’S how you make the big bucks__

### HOW DO YOU ESCAPE THE 9-5?
* It’s difficult
* You get suck into being busy, and the job chews into all your time – it makes it hard to work on anything else
* Find a career/job/education where you will end up in a business where the inputs and outputs are disconnected
* 2000 years ago, the inputs and outputs were connected – effort=result
* Nowadays – we have knowledge worker jobs
* A good developer can write a good piece of code that will generate millions of dollars over the next few years
>  “Navigate towards a career where you’re tracked on the output”
* Sales is a good example, or product building etc.
* NOT a customer service role

### CAN 7 BILLION PEOPLE ALL PRODUCTIZE THEMSELVES?
* Yes
* Assume everyone had maximum education and practical knowledge
* Naval thinks if this were the case, within 5 years everyone would be independently wealthy, robots would be doing the manual work, and we’d all be doing creative work
* Wealth is not a zero sum game – it’s a positive sum game
* 7 billion people can have 7 billion products

### SKIN IN THE GAME: HIDDEN ASYMMETRIES IN DAILY LIFE
* Written by Nassim Taleb. The best book Naval has read in 2018

### SALES SKILLS
* Sales skills are a form of specific knowledge
* You can be trained, but there’s such a thing as a natural, and it’s easy to identify one
* How to improve? – read Influence: The Psychology of Persuasion by Robert Cialdini
* The book talks about strategies we can use to persuade people
* __CLASSR__
* __Consistency__ – people want to be consistent with their past actions
* __Liking__ – if someone likes you, they’re more likely to be persuaded by you, so be nice to people
* __Authority__ – you’re more likely to listen to a doctor for medical advice than a non-doctor
* __Scarcity__ – we want what’s scarce 
* __Social Proof__ – monkey see, monkey do
* __Reciprocity__ – people always want to repay you
* __“Persuasion is one of foundational skills that everyone should learn”__

### ADDITIONAL FOUNDATIONAL SKILLS PEOPLE SHOULD HAVE IN ADDITION TO PERSUASION
* Basic mathematics – probability and statistics are important for personal finance
* Basic accounting
* Calculus – understand rates of change, be able to measure the change in small discrete/continuous events

### SPECIFIC KNOWLEDGE
* Specific Knowledge – A combination of unique traits from your DNA, combined with your unique upbringing and your response to it
* Figure out what it was you were doing as a kid, almost effortlessly, that you didn’t even really consider a skill, but people around you would notice

### PROGRAMMING
* Is not a commodity skill
* In the future, computers will not write code – coding is a general AI task
* It is not likely to be automated in our lifetimes
* The top coders in the world can make billions of dollars of value, while a not so good coder can well, create no value
* The spread is so large

### WHAT WOULD NAVAL MAJOR IN TODAY IF HE WERE TO GO TO COLLEGE?
* Learn things in college that you can’t learn by yourself
* Mathematics, programing, physics, medicine etc.
* If you can learn STEM disciplines on your own, you may not need to go to school
* One of the advantages of a college education is an alumni network

### DESIRES
* We all have many desires in life
* When you’re desires are split up, you create anxiety, and this forces you to lose focus
* Pick the one thing you care about more than anything, and ignore everything else
	* “If you’re going to make money, it has to be your number one desire”
	* “It’s very, very hard to be successful in business while trying to live a well rounded life” 

### DECISION MAKING
* Naval’s 3 decision making heuristics
* When faced with a difficult choice, if you can’t decide, the answer is no
* Modern society is filled with options – “We are biologically built to not realize how many choices there are, because we evolved in tribes of ~150 people, where if you pass up one choice, the second one never comes along”
* When you choose something, you often get locked in for a long time
* Only say yes when you’re pretty certain
* If you have to create a pro/con list – the answers no
* If you have 2 choices to make, and they’re relatively equal (it’s 50/50), take the path that’s more difficult and more painful in the short term
* Short term pain, means long term gain
* Our brain tends to overvalue the choice with short term happiness, and is trying to avoid the one with short term pain
* Lean into the pain
* “Most of the gains in life come from suffering in the short term, so you can get paid in the long term”
* “Hard choices, easy life. Easy choices, hard life.”
* In times of interpersonal conflict, make the choice that will leave you more equanimous (internally calm) 
* Peace of mind is a precursor to happiness
* Happiness is one of those things that can’t be chased directly – if you do chase it, you’re chasing pleasure
* Pleasure is just a high, which you eventually come crashing down from
* “If you actually want to be happy and content, that comes from peace”

### BE IMPATIENT AND WILLFUL
* Naval hates wasting time – he’ll  often leave a dinner/party at a moments notice if he feels it’s a waste of time
* “The moment I figure out something is a waste of my time, I leave immediately”
* After looking back now, Naval realizes most of his past business trips were a waste of time
* “Business trips are almost never worth it”
* “Value your time, it’s all you have”

### FAILURE
* “If you didn’t have failure, you’d never improve”
* Failure causes us to change, and through that, we get growth
* If you always got what you wanted, you’d probably be married to your high school sweetheart and be a manager at your first job

### 99% OF EFFORT IS WASTED
* Most of what you’ve learned in school, doesn’t really apply to your life now – sure you learned from it, but according to goal oriented life, only about 1% of the effort you put in pays off
* In most aspects of life, try to find the thing you can go all in on with compound interest
* In dating – the minute you think the relationship won’t lead to marriage, you should probably end it
* Be more conscious of what’s a waste of your time
* “When you find that 1%, go all in, and forget about the rest”

### THE LAST TWEET IN NAVAL’S TWEET STORM
* “When you’re finally wealthy, you’ll realize that it wasn’t what you were seeking in the first place. But that’s for another day”
* Making money won’t make you happy
* Making money will solve your money problems – it removes a set of things that are in the way of making you happy
* Happiness comes from peace
* To be happy – “You truly have to believe that happiness is a choice you make and a skill you can develop” – It is NOT an intrinsic quality
* You can improve your happiness baseline, just like you can improve your fitness
* “The first step to being happier, is to believe you can be happier”
