+++
categories = []
date = "2017-04-17T19:25:58+05:30"
description = ""
tags = []
title = "A list of opensource backup tools`"

+++
via cronweekly:

1. [Backup](https://github.com/backup/backup) is a system utility for Linux and Mac OS X, distributed as a RubyGem, that allows you to easily perform backup operations. It provides an elegant DSL in Ruby for modeling your backups.

2. [Duplicati](https://github.com/duplicati/duplicati)  is a free, open source, backup client that securely stores encrypted, incremental, compressed backups on cloud storage services and remote file servers.

3. [Obnam](http://obnam.org/) is an easy, secure backup program. Backups can be stored on local hard disks, or online via the SSH SFTP protocol. The backup server, if used, does not require any special software, on top of SSH.

4. [Bareos](https://www.bareos.org/en/) is a 100% open source fork of the backup project from bacula.org. The fork is in development since late 2010, it has a lot of new features.

5. [Duply](http://duply.net/) is a frontend for duplicity. duplicity is a python based shell application that makes encrypted incremental backups to remote storages. Different backends like ftp, sftp, imap, s3 and others are supported.

6. [Restic](https://github.com/restic/restic) is a backup program that is fast, efficient and secure.



